import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Toast from "../components/Toast"


const Header = ({ }) => (
  <header
    style={{
      background: `#15A0F5`,
      marginBottom: `0`,
    }}
  >

    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
        height: `40px`,
        display: `flex`,
        alignItems: `center`

      }}
    >
      <span style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
        </Link>
      </span>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
