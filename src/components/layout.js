import React, { useState, useEffect } from 'react';
import { useI18next } from "gatsby-plugin-react-i18next"

/** Hooks */
import { useStaticQuery, graphql, navigate } from 'gatsby';
import { useSelector, useDispatch } from 'react-redux';
import toastHook from '../hooks/toastHook';

/** Components */
import { SnackbarProvider } from 'notistack';
import { Container } from '@material-ui/core/';
import SideBarNavigation, { SIDEBAR_WIDTH } from './SideBar';
import Toast from '../components/Toast';
import SEO from "../components/seo"
import Header from "./LandingHeader"


/** Utils */
import PropTypes from 'prop-types';
import cx from 'classnames';
import { createTheme, ThemeProvider, makeStyles } from '@material-ui/core/styles';
import authTokenObserver from '../services/authTokenObserver';
import { ClientService } from 'src/services/zed-api'
import { PayoutPartnerService } from 'src/services/PayoutPartnerService';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { getHttpErrorMessage, concatArrays } from "src/services/helper"
import { RecipientService } from "src/services/RecipientService"
import { TransferService } from "src/services/TransferService"

/** Styles */
import './layout.scss'


const PPS = new PayoutPartnerService();

const useStyles = makeStyles((theme) => ({
  loadingWrapper: {
    minWidth: '100vw',
    minHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down("sm")]: {
      flexDirection: 'column'
    },
  },
  container: {
    minWidth: '100vw',
    minHeight: '100vh',
    display: 'flex',
    [theme.breakpoints.down("sm")]: {
      flexDirection: 'column'
    },
  },
  guestContent: {
    width: '100%',
  },
  content: {
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    padding: `1rem 8rem`,
    position: 'relative',
    marginLeft: SIDEBAR_WIDTH,
    marginTop: 66,
    [theme.breakpoints.down("md")]: {
      padding: `1rem`,
    },
    [theme.breakpoints.down("sm")]: {
      padding: `1rem 0 0 0`,
      marginLeft: 0,
    },
  }
}));

const LoadingScreen = ({children}) => {
  const classes = useStyles();

  return (
    <Container
      classes={{
        root: classes.loadingWrapper
      }}
    >
      <div>ZED Webportal Initializing...</div>

      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.45rem`,
          overflowY: "hidden",
        }}
      >
        <main>{children}</main>
        <footer
          style={{
            marginTop: `2rem`,
          }}
        ></footer>
      </div>
    </Container>
  )
}

export const GuestLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <SEO title="Test Progress" />

      <Toast />
      <div className={classes.guestContent}>
        {children}
      </div>
    </div>
  )
}

export const AuthLayout = ({ children, ...props }) => {
  
  const dispatch = useDispatch()
  const [onToggleIcon, setOnToggleIcon] = useState(false)
  const isMatches = useMediaQuery(theme => theme.breakpoints.up('md'), { noSsr: true })
  dispatch({ type: "MEDIA_QUERY", payload: { matches: isMatches } })
  const showSideBar = useSelector(state => state.sidebar.showSideBar)
  const childrenWithProps = React.Children.map(children, child => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, props);
    }
    return child;
  })

  const classes = useStyles()
  return (
    <div className={classes.container}>
      <Header/>
      
      <main className={cx({ [classes.content]: true })} >
        {isMatches && <SideBarNavigation onToggleIcon={onToggleIcon} setOnToggleIcon={setOnToggleIcon}/>}
        {!isMatches && showSideBar && <SideBarNavigation onToggleIcon={onToggleIcon} setOnToggleIcon={setOnToggleIcon}/>}
        {childrenWithProps}
      </main>
    </div>
  )
}

const Layout = ({ children, location }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)
 
  const toastDispatcher = toastHook()
  const dispatch = useDispatch()
  const onTokenNotFound = () => {
    navigate("/")
  }
  
  useEffect(() => {
    authTokenObserver.attachObserver(onTokenNotFound)
    return () => {
      authTokenObserver.detachObserver(onTokenNotFound)
    }
  }, [])
  
  const regex = /client\/{1}/
  const matches = location?.pathname.match(regex)
  const hasMatch = matches?.length > 0

  const clientService = new ClientService()
  const initializeAccountsBalance = async () => {
    try {
      const { data: result, status } = await PPS.getPayoutPartner();
      if (status !== 200) {
        toastDispatcher('Failed to fetch payout partner integrations', 'danger')
        return
      }
      if (result.length === 0) {
        toastDispatcher('No payout integration found. Please integrate with a payout partner before testing.', 'danger')
        return
      }
      dispatch({ type: "INTEGRATIONS", payload: result})

    } catch (error) {
    }
  }

  const initializeUser = async () => {
    try {
      const response = await clientService.getDetails()
      if (response.status === 403) return 
      if (response.status === 400) {
        toastDispatcher('Integration credentials failed: please double check API key and password', 'error')
        navigate("/client/service-gateway-selection-page")
      }
      if (response.status === 404) {
        toastDispatcher('Welcome to Zed Client Portal', 'success')
        navigate("/client/onboarding")
        return 
      }
      if (response.status !== 200 ) {
        const errorMessage =
          getHttpErrorMessage(response) ||
          'Something went wrong while trying fetch your information'
      
        toastDispatcher(errorMessage, 'danger')
        return
      }
      dispatch({ type: "USER", payload: response.data})
    } catch (error) {
      toastDispatcher("Something went wrong while trying fetch your information", 'danger')
      navigate("/")
    }
  }
  
  
  const user = useSelector(state => state.user.userInfo)
  // const recipients = useSelector(state => state.clientBankAccounts.recipients)
  // const transfers = useSelector(state => state.clientBankAccounts.transfers)
  const reInitialize = location?.pathname !== "/client/onboarding" && !user
  useEffect(() => {
    reInitialize && hasMatch && initializeUser()
  },[hasMatch, reInitialize])

  useEffect(() => {
    hasMatch && user && fetchIndividuals("recipient") 
    hasMatch && user && fetchIndividuals("senders") 
    hasMatch && user && fetchTransfers()
    hasMatch && initializeAccountsBalance() 
  }, [user])

  const fetchIndividualsByPageIndex = async (invidiualRole, pageIndex) => {
    try {
      const recipietService = new RecipientService()
      if(pageIndex === 1) {
        const response = invidiualRole === "recipient" ? await recipietService.getRecipients(pageIndex) : await recipietService.getSenders(pageIndex)

        if (response.status !== 200) {
          console.error(`Something went wrong while loading ${recipietService.getRecipients(pageIndex)}`)
          return
        }
        const { total_pages, recipient, senders } = response.data
        
        const fetchedIndividuals = (recipient || senders).filter(recip => !recip.status_code)
        return  { total_pages, fetchedIndividuals } 
      }
      const response = await recipietService.getRecipients(pageIndex)
      if (response.status !== 200) {
        console.error(`Something went wrong while loading ${recipietService.getRecipients(pageIndex)}`)
        return
      }
      const { total_pages, recipient } = response.data
      const fetchedIndividuals = recipient.filter(recip => !recip.status_code) 
      return { total_pages, fetchedIndividuals }
    } catch(error) {
      console.error(error)
    }
  }

  const fetchIndividuals = async (invidiualRole) => {
    try {
      const pageIndex = 1
      const {total_pages, fetchedIndividuals} = await fetchIndividualsByPageIndex(invidiualRole, pageIndex)
      let individual = fetchedIndividuals || []
      if(total_pages > pageIndex) {
        for (let page = 2; page < total_pages; page++) {
          const { total_pages, fetchedIndividuals } = await fetchIndividualsByPageIndex(invidiualRole, page)
          individual = concatArrays(individual, fetchedIndividuals)
        }
      }
      invidiualRole === "recipient" ? dispatch({ type: "RECIPIENTS", payload: { recipients: individual } }) : dispatch({ type: "SENDERS", payload: { senders: individual } })
      dispatch({ type: "RECIPIENTS", payload: { recipients: individual } })
    } catch {
      console.error("Something went wrong while loading recipients")
    } 
  }
  const fetchTransfersByPageIndex = async (pageIndex) => {
    try {
      const transferService = new TransferService()
      if(pageIndex === 1) {
        const response = await transferService.getTransfers(pageIndex)
        if (response.status !== 200) {
          console.error("Something went wrong while loading transfers")
          return
        }
        const { total_pages, transfers } = response.data
        return  { total_pages, transfers } 
      }
      const response = await transferService.getTransfers(pageIndex)
      if (response.status !== 200) {
        console.error("Something went wrong while loading transfers")
        return
      }
      const { total_pages, transfers } = response.data
      return { total_pages, transfers }

    } catch(error) {
      console.error(error)
    }
  }

  const fetchTransfers = async () => {
    try {
      let pageIndex = 1
      
      const {total_pages, transfers} = await fetchTransfersByPageIndex(pageIndex)
      let transfer = transfers || []
      if(total_pages > pageIndex) {
        for (let page = 2; page < total_pages; page++) {
          const { total_pages, transfers } = await fetchTransfersByPageIndex(page)
          transfer = concatArrays(transfer, transfers)
        }
      }
      dispatch({ type: "TRANSFERS", payload: { transfers: transfer } })
    } catch {
      console.error("Something went wrong while loading transfers")
    } 
  }

  const isInitialize = useSelector(state => state.app.isInitialize)
  if (!isInitialize) {
    return <LoadingScreen />
  }
  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <GuestLayout children={children} data={data} />
      </SnackbarProvider>
    </ThemeProvider>
  )
}

const theme = createTheme({
  typography: {
    fontFamily:"Lexend",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightLarge: 600,
    h6: {
      fontWeight: 700,
      fontSize: ".9rem",
      letterSpacing: "1px"
    }
  },
  palette: {
    text: {
      darkBlue: "#4F6382",
      primary: '#333333',
      lightGray: '#707C8D',
      lightGray2: '#868686',
      darkGray: '#333333',
    },
    primary: {
      main: "#007bff",
    },
    icon: {
      grey: '#475B79',
      blue: '#0569FF',
      purpleIcon:'#5F72FF',
      purpleIconBg: '#EFF1FE',
      blueIcon: '#0569FF',
      redIcon: '#F96C6C',
      redIconBg:'#FDEAEA',
      disabledLightGray: '#e0e0e0',
      disabledDarkGray: '#999999'
    },
    button: {
      primaryBlue: '#3273AD',
    },
    primaryGreen: "#00B368",
    primaryGreen1: "#16BF78",
    primaryLightGreen: "#b6fcdf",
    primaryBlue: "#227AFF",
    primaryBlue1: "#3485FF",
    primaryOrange: "#EFA333",
    primaryOrange1: '#FF715A',
    primaryPink: "#D04EAC",
    primaryViolet: "#845EC2",
    primaryDarkBlue: '#0F1E34',
    primaryBlue1: "#0569FF",
    primaryWhite: '#FFFFFF',
    primaryGrey: '#B6B6B6',
    primaryDarkGrey: '#5A6677',
    primaryLightGray: '#FBFBFB',
    primaryLightGray2: '#E6E6FC',
    primaryLightGray3: '#F3F3F3',
    primaryLightGray4: '#FAFAFF',
    primaryDarkBlue: '#1F3351',
    dashedButtonBorder: '#8397B5',
    primaryDarkBlue: '#1F3351',
    dashedButtonBorder: '#8397B5',
    dashedButtonBorder: '#8397B5',
    shadow: '#D0D3E499',
    primaryRed: '#C73734'
  },
  overrides: {
    MuiPaper: {
      rounded: {
        borderRadius: "0.7rem",
        border: "none",
      },
    },
    container: {
      borderRadiusLarge: '15px'
    },
    button: {
      borderRadiusLarge: '8px'
    },
    greyBorder:'1px solid #BBBBBB',
    errBorder: '1px solid #C73734'
  },
  breakpoints: {
    xs: '375px',
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
  }
})

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout;
