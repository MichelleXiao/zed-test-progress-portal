import React, { useEffect, useMemo } from "react"
import PropTypes from "prop-types"

import { makeStyles } from '@material-ui/core/styles'
import MaUTable from "@material-ui/core/Table"
import {
  TableContainer,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from "@material-ui/core"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCaretDown, faCaretUp  } from "@fortawesome/free-solid-svg-icons"

import { useTable, useSortBy, usePagination, useRowSelect } from "react-table"

import TableSkeletonList from "../views/RecipientForms/RecipientListSkeleton"
const useStyles = makeStyles((theme) => ({
  tableContainer: {
    maxHeight: '600px',
  },
  table: {
    marginBottom: '0 !important',
    '& tbody tr td:first-child':{
      paddingLeft: '45px !important',
      [theme.breakpoints.down("sm")]: {
        paddingLeft: '2rem !important',
      },
      [theme.breakpoints.down("xs")]: {
        paddingLeft: '1rem !important',
      }
    }
  },
  tableHeader: {
    height: 42,
    backgroundColor: theme.palette.bgLightGray,
    '& .MuiTableRow-root.MuiTableRow-head': {
      '& th:first-child': {
        paddingLeft: 45,
        [theme.breakpoints.down("sm")]: {
          paddingLeft: '2rem',
        },
        [theme.breakpoints.down("xs")]: {
          paddingLeft: '1rem',
        }
      },
    }
  },
  headerCell: {
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 13,
    [theme.breakpoints.down("sm")]: {
      padding: '16px',
    }
  },
  paginationContainer: {
    paddingRight: 30,
    height: 42,
  },
  changePageButton: {
    width: '40px !important',
    minWidth: 0,
    '&:focus': {
      outline: 0
    }
  },
  pageNumber: {
    fontSize:12,
    fontWeight: theme.typography.fontWeightLarge,
  },
  sortIcon: {
    cursor: 'pointer',
    marginLeft: 10
  },
  checkBox:{
    '&:hover': {
      backgroundColor: 'transparent !important'
    }
  },
  checkBoxIcon: {
    borderRadius: '3px',
    width: 12,
    height: 12,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#EFEFF4',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  },
  checkedIcon: {
    backgroundColor: theme.palette.primaryGreen,
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 12,
      height: 12,
      backgroundImage:
        "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
        " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
        "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: theme.palette.primaryGreen,
    },
  },
  tableRow: {
    '& td:first-child': {
      paddingLeft: 45
    },
    '& .MuiTableCell-root.MuiTableCell-body': {
      padding: '10px 16px',
    },
    '&:hover': {
      backgroundColor: '#F4F4F9'
    }
  },
  boldText: {
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 12,
  },
  lightText: {
    color: '#868686',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: 10,
  },
  emptyMessage: {
    height: 300,
    textAlign: 'center',
    padding: `0px !important`
  },
  message: {
    fontSize: 15,
  },
}))
const Table = ({
  columns,
  data,
  tableRows: TableRows,
  onChangePage,
  isLoading,
  emptyState,
  currentPageIndex,
  checkBox,
  style = {},
  enabledPagination = true,
  maxHeight = '530px',
  onClickRow,
  rowNum,
  ...useTableProps
}) => {
  const tableData = useMemo(() => data, [data])
  const tableColumns = useMemo(() => columns)
  const classes = useStyles()
  const {
    getTableProps,
    rows,
    headers,
    page,
    prepareRow,
    nextPage,
    canNextPage,
    canPreviousPage,
    previousPage,
    hideHeader,
    pageCount,
    gotoPage,
    state: { pageIndex, pageSize, selectedRowIds },
  } = useTable(
    {
      columns: tableColumns,
      data: tableData,
      initialState: enabledPagination ? { pageIndex: 0, pageSize: pageSize ? pageSize : 10 } : {},
      autoResetPage: false,
       ...useTableProps
    },
    useSortBy,
    usePagination,
    useRowSelect
  )
  const tableRows = enabledPagination ? page : rows
  useEffect(() => {
    if (onChangePage) {
      onChangePage(pageIndex)
    }
  }, [pageIndex])

  useEffect(() => {
    if (!enabledPagination) return;
    gotoPage(currentPageIndex)
  }, [currentPageIndex])

  const renderLoading = () => {
    return (
      <TableSkeletonList
        coloumnNum={checkBox ? columns.length + 1 : columns.length}
        rowNum={rowNum || 16}
        className={classes.name}
      >
      {
        !hideHeader && (
          <TableHeaders 
            headers={headers} 
          />
        )
      }
      </TableSkeletonList>
    )
  }

  const handleScroll = (e) => {
    const bottom = e.target.scrollHeight - e.target.scrollTop <= e.target.clientHeight
    bottom && canNextPage && nextPage()
  }

  return (
    <>
      {isLoading && renderLoading()}
      {!isLoading && (
        <TableContainer className={classes.tableContainer} style={{ maxHeight:maxHeight, ...style }} onScroll={handleScroll}>
          <MaUTable
            {...getTableProps()}
            stickyHeader={true}
            className={classes.table}
          >
            {
              !hideHeader && (
                <TableHeaders 
                  headers={headers} 
                />
              )
            }
            <TableBody>
              {
                tableRows.length > 0
                ? TableRows ? (
                    <TableRows
                      page={tableRows}
                      prepareRow={prepareRow}
                      isLoading={isLoading}
                    />
                  ) :(
                    <DefaultTableRows
                      page={tableRows}
                      prepareRow={prepareRow}
                      isLoading={isLoading}
                      onClickRow={onClickRow}
                    />
                  )
                : (emptyState || <EmptyTable span={headers.length} />)
              }
              
            </TableBody>
          </MaUTable>
        </TableContainer>

      )}
    </>
  )
}
const TableHeaders = ({ headers }) => {
  const classes = useStyles()
  return (
    <TableHead className={classes.tableHeader}>
      <TableRow>
        {headers.map(column => (
          <TableCell
            className={classes.headerCell}
            {...column.getHeaderProps(column.sortType && {
              ...column.getSortByToggleProps(),
              style: { minWidth: column.minWidth, width: column.width },
            })}
          >
            {column.render("Header")}
            <SortIcon column={column} />
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

const DefaultTableRows = ({ page, prepareRow, isLoading, onClickRow }) => {
  const classes = useStyles();

  return page.map((row, i) => {
    prepareRow(row)
    return (
      <TableRow 
        {...row.getRowProps()}
        className={classes.tableRow}
        onClick={()=> onClickRow && onClickRow(row)}
      >
        {row.cells.map(cell => {
          const cellProps = cell.getCellProps({
            style: {
              minWidth: cell.column.minWidth,
              width: cell.column.width,
            },
          })
          if (isLoading) return <TableCell {...cellProps} />
          return (
            <TableCell {...cellProps}>
              <Typography
                className={classes.boldText}
                component={'span'}
              >
                {cell.render("Cell")}
              </Typography>
            </TableCell>
          )
        })}
      </TableRow>
    )
  })
}

const EmptyTable = ({ span }) => {
  const classes = useStyles()

  return (
    <TableRow> 
      <TableCell className={classes.emptyMessage} colSpan={span}>
        <Typography className={classes.message}>There is no data available</Typography>
      </TableCell>
    </TableRow>
  )
}

const SortIcon = ({ column }) => {
  const classes = useStyles()
  if (!column.sortType) return null
  return column.isSortedDesc 
    ? <FontAwesomeIcon icon={ faCaretUp } className={classes.sortIcon} />
    : <FontAwesomeIcon icon={ faCaretDown } className={classes.sortIcon} />
}

Table.propTypes = {
  //react-use Table columns https://react-table.tanstack.com/docs/quick-start#define-columns
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  // TableRow component
  tableRows: PropTypes.func,
   // Callback on page change
  onChangePage: PropTypes.func,
  // Indicate loading state
  isLoading: PropTypes.bool,
  // Handle empty tbody, accepts following props:
  //  desc: description of empty state. eg. You don't have any recipient yet.
  //  buttonText: description of a button to solve empty state. eg. Add new recipient
  //  clickableFunc: hanle click on button. 
  //  colSpan: number of columns.
  emptyState:PropTypes.node,
  // Page number of current page
  currentPageIndex: PropTypes.number,
  style: PropTypes.object,
}

export default Table
