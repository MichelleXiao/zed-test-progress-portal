import React, { useState } from 'react'

/** Components */
import {
  Button,
  Collapse,
  Drawer,
  ListSubheader,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Paper,
  SvgIcon,
  Typography,
  Grid
} from '@material-ui/core';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

/** Utils */
import { navigate } from 'gatsby';
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'gatsby-plugin-react-i18next'

/** Assets */
import RecipientIcon from '@material-ui/icons/GroupAdd'
import SenderIcon from '@material-ui/icons/Group'
import TransferIcon from '@material-ui/icons/Send'
import WalletIcon from '@material-ui/icons/Payment'
import { RECIPIENT, SENDER, WALLET, THUNES, CORPAY } from '../constants/testCases'


import {capitalizeFirstLetter} from 'src/services/helper'

export const SIDEBAR_WIDTH = 260

const GENERAL_LIST = ["Recipient", "Sender", "Wallet"]
const useStyles = makeStyles((theme) => ({
  drawer: {
    zIndex: 99,
    width: props => !props.isMatches && props.showSideBar ? '100%' : SIDEBAR_WIDTH,
    flexShrink: 0,
    transition: theme.transitions.create('transform', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    '&:hover .toggleDrawer': {
      opacity: 1
    },
  },
  drawerPaper: {
    width: props => !props.isMatches && props.showSideBar ? '100%' : SIDEBAR_WIDTH,
    backgroundColor: theme.palette.button.primaryBlue,
    height: 'calc(100% - 66px)',
    marginTop: 66
  },
  drawerContent: {
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    justifyContent: 'space-between'
  },

  sidebarHeader: {
    display: 'flex',
    alignItems: 'center',
    gap: 12,
    marginTop: 2,
    color: theme.palette.primaryWhite,
    cursor: 'pointer'
  },
  sidebarSubtitle: {
    fontSize: '0.67rem',
    display: 'flex',
    alignItems: 'center',
    gap: 4,
  },
  menuSection: {
    width: '100%',
    color: theme.palette.primaryWhite,
    
  },
  menuSectionTitle: {
    color: theme.palette.primaryWhite,
    fontSize: '0.95rem',
    fontWeight: '600',
    position: 'unset'
  },
  menuItemText: {
    fontSize: 15,
    lineHeight: 1.1,
    color: theme.palette.primaryWhite,
    fontWeight: '500'
  }, 
  menuIcon: {
    minWidth: 40,
    color: theme.palette.primaryWhite,
    marginTop: 5,
    '& svg': {
      '& path': {
        fill: theme.palette.primaryWhite
      }
    }
  },

  companyMenuItem: {
    '&:hover': {
      color: theme.palette.icon.blue,
      '& svg path:not([fill="white"])': {
        fill: theme.palette.icon.blue
      }
    }
  },
  paper: {
    padding: 15,
    marginTop: 20,
    backgroundColor: theme.palette.primaryWhite,
  },
  paperTitle: {
    display: 'flex',
    gap: 8,
    marginBottom: 12
  },
  paperTitleText: {
    fontSize: '0.75rem',
    color: theme.palette.primaryDarkBlue
  },
  paperCaptionText: {    
    fontSize: '0.68rem'
  },
  paperIcon: {
    color: theme.palette.primaryGreen
  },
  listItem: {
    padding: '8px 0'
  },
  APIlistItem: {
    padding: '8px 0 8px 40px',
  }
}))

const generalSidebarMapping = [
  {
    title: 'General',
    type: 'section',
    children: [
      {
        title: 'Recipient',
        icon: <SvgIcon component={RecipientIcon} viewBox='0 0 29 29'  />, 
        type: 'link',
        path: '/client/recipient',
        apis: RECIPIENT
      },
      {
        title: 'Sender',
        icon: <SvgIcon component={SenderIcon} viewBox='0 0 29 29'  />, 
        type: 'link',
        path: '/client/sender',
        apis: SENDER
      },
      {
        title: 'Wallet',
        icon: <SvgIcon component={WalletIcon} viewBox='0 0 29 29'  />,
        type: 'link',
        path: '/client/wallet',
        apis: WALLET
      }
    ]
  }
]


const Sidebar = () => {
  const [dropdownOpen, setDropdownOpen] = useState(false)

  const dispatch = useDispatch()
  const open = useSelector(state => state.app.open)
  const user = useSelector(state => state.user.userInfo)
  const matches = useSelector(state => state.app.matches)
  const showSideBar = useSelector(state => state.sidebar.showSideBar)
  const integrations = useSelector(state => state.clientBankAccounts.integrations)
  const showApis = useSelector(state => state.sidebar.showApis)

  const { t } = useTranslation()
  const classes = useStyles({ 
    showSideBar: showSideBar, 
    isMatches: matches,
    dropdownOpen
  })
  const isValid = user && user.status === "Approved"
  const isPendingUser = user && user.status === "PENDING"
  
  const generateAPISection = (apiObj) => {
    return ({
      key: apiObj.key,
      name: apiObj.name,
      status: "COMPLETED"
    })
  }
  const generateMainSection = (sectionName) => {
    let apiArr = []
    switch(sectionName) {
      case 'THUNES': 
        apiArr = THUNES.map(api => generateAPISection(api))
        break
      case 'CORPAY': 
        apiArr = CORPAY.map(api => generateAPISection(api))
      default: 
        break
    }
    return apiArr
  }
  const generateSideMapping = () => {
    if(!integrations) return 
    const payoutPartnersSection = integrations.map(integration => {
      return ({
        title: capitalizeFirstLetter(integration.payout_partner),
        type: 'section',
        children: [
          {
            title: `${capitalizeFirstLetter(integration.payout_partner)} Transfer`,
            icon: <SvgIcon component={TransferIcon} viewBox='0 0 29 29'  />, 
            type: 'link',
            path: `/client/${integration.payout_partner.toLowerCase()}`,
            apis: generateMainSection(integration.payout_partner)
          }
        ]
      })
    })
    return generalSidebarMapping.concat(payoutPartnersSection)
  }

  return (
    <Drawer
      className={classes.drawer}
      anchor="left"
      open={open}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerContent}>
        <SidebarContent 
          showApis={showApis}
          dispatch={dispatch}
          setDropdownOpen={setDropdownOpen} 
          matches={matches} 
          isValid={isValid} 
          isPendingUser={isPendingUser}
          translate={t}
          generateSideMapping={generateSideMapping}
        />
      </div>
    </Drawer>
  )
}

const SidebarContent = ({ showApis, dispatch, setDropdownOpen, matches, isValid, isPendingUser, translate, generateSideMapping }) => {
  const classes = useStyles();
  return (
    <div>
      {generateItem(showApis, dispatch, isPendingUser, isValid, generateSideMapping())}
    </div>
  )
}


const SidebarSection = ({ showApis,dispatch, title, children, depth, isValid, isPendingUser }) => {
  const classes = useStyles();
  const {t} = useTranslation()
  return (
    <List
      component="nav"
      subheader={
        <ListSubheader component="div" className={classes.menuSectionTitle}>
          {t(title)}
        </ListSubheader>
      }
      className={classes.menuSection}
    >
      {generateItem(showApis, dispatch,isPendingUser, isValid, children, depth + 1 )}
    </List>
  )
}

const SidebarLink = ({ showApis, dispatch, title, icon, path, isValid, isPendingUser, item }) => {
  const classes = useStyles({disabled: !isValid});
  
  const { t } = useTranslation();

  const generateAPIs = (api) => {
    const navigateAPI = (apiObj) => {
      isValid && navigate(
        path,
        {
          state: { 
            expandedApi: apiObj.key
          },
        }
      )
    }
    if(!api) return
    return api.apis.map(apiObj => {
      return <ListItem  className={classes.APIlistItem} button onClick={() => {}}>
        <ListItemText 
          primary={t(apiObj.name)} 
          classes={{ primary: classes.menuItemText }} 
          onClick={()=>navigateAPI(apiObj)}
        />
      </ListItem>
    })
  }

  const handleNavigation = () => {
    dispatch({ type: "SIDEBAR", payload: title})
    isValid && navigate(path)
  }

  return (
    <>
      <ListItem className={classes.listItem} button onClick={() => handleNavigation()}>
        {
          icon && (
            <ListItemIcon className={classes.menuIcon}>
              {icon}
            </ListItemIcon>
          )
        }
        <ListItemText primary={t(title)} classes={{ primary: classes.menuItemText }} />
        <ListItemIcon className={classes.menuIcon}>
          {showApis === title ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
        </ListItemIcon>
      </ListItem>
      {
        showApis===title
        && generateAPIs(item)
      }
    </>
    
    
  )
}

const generateItem = (showApis, dispatch, isPendingUser, isValid, items, depth = 0 ) => {
  if(!items) return
  return items.map((item) => {
    switch (item.type) {
      case 'section':
        return <SidebarSection showApis={showApis} dispatch={dispatch} key={item.title} {...item} depth={depth} isValid={isValid} isPendingUser={isPendingUser}/>
      case 'link':
        return <SidebarLink showApis={showApis} dispatch={dispatch} key={item.path} {...item} depth={depth} isValid={isValid} isPendingUser={isPendingUser} item={item}/>
      default:
        return null;
    }
  })
}



export default Sidebar;
