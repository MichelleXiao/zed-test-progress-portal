import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

let displayed = [];

const Toast = () => {
    const dispatch = useDispatch();
    const notifications = useSelector(store => store.app.toast || []);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const storeDisplayed = (id) => {
      displayed = [...displayed, id];
    };

    const removeDisplayed = (id) => {
      displayed = [...displayed.filter(key => id !== key)];
    };

    const removeSnackbar = (key) => {
      dispatch({
        type: 'DELETE_TOAST',
        payload: {
          key,
        }
      })
    }

    React.useEffect(() => {
      notifications.forEach(({ key, message, type, options = {}, dismissed = false }) => {
        if (dismissed) {
          closeSnackbar(key);
          return;
        }

        if (displayed.includes(key)) return;
        closeSnackbar(key);
        enqueueSnackbar(message, {
          key,
          variant: type || 'info',
          ...options,
          autoHideDuration: type === "error" ? 60000 : 3000,
          onClose: (event, reason, myKey) => {
            if (options.onClose) {
              options.onClose(event, reason, myKey);
            }
          },
          onExited: (event, myKey) => {
            removeSnackbar(myKey);
            removeDisplayed(myKey);
          },
          onClick: () => {
            closeSnackbar(key);
          }
          
        });

        storeDisplayed(key);
      });
    }, [notifications, closeSnackbar, enqueueSnackbar, dispatch]);

    return null;
};

export default Toast;
