import React, { useState, useEffect } from "react"
import { useSelector, useDispatch } from "react-redux"
import { navigate } from "gatsby"
import classnames from "classnames"

import { getCookie, deleteCookie } from 'src/services/Cookies'

import { makeStyles } from '@material-ui/core/styles'
import { Grid, SvgIcon } from "@material-ui/core"
import Button from "src/components/Button"
import { getUserId } from "src/components/PrivateRoutes"
import ViewHeadline from '@material-ui/icons/ViewHeadline'
import ZedLogo from "src/images/imgs/zed_logo.png"

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor:theme.palette.primaryWhite,
    height: '66px',
    boxShadow: '0px 0px 20px #0000001A',
    width: '100%',
    padding: '14px 40px',
    position: 'fixed',
    top: 0,
    zIndex: 9,
    [theme.breakpoints.down("lg")]: {
      padding: '13px 40px',
    },
    [theme.breakpoints.down("xs")]: {
      padding: '16px',
    },
    '& svg': {
      marginLeft: '0.5rem',
      height: 30
    }
  },
  logo: {
    height: 34,
    width: 76,
    margin: 0,
    cursor: 'pointer'
  },
  signupBtn: {
    minWidth: '75px !important',
    '&:hover': {
      backgroundColor: theme.palette.button.primaryBlue,
    },
  },
  button: {
    borderRadius: '20px',
    width: '100px',
    height: '40px',
    marginLeft: '20px',
    [theme.breakpoints.down("md")]: {
      width: '90px',
    },
    [theme.breakpoints.down("xs")]: {
      width: '70px',
      height: '30px',
      fontSize: 12,
      marginLeft: '10px',
    }
  },
  profileImg: {
    padding:0,
    marginRight: props => props.open ? '5px' : 0
  },
  menuPaper: {
    borderRadius: 5,
    marginTop: 45,
    '& ul': {
      paddingBottom: 0
    }
  },
  menuItem: {
    '& p': {
      fontSize: 14
    },
  },
  logout: {
    display: 'flex',
    justifyContent: 'center',
    paddingBottom: 8,
    background: theme.palette.button.primaryBlue,
    color: theme.palette.primaryWhite,
    '&:hover': {
      background: theme.palette.primaryBlue,
    }
  },
  viewIcon: {
    display: "none",
    [theme.breakpoints.down("xs")]: {
      display: "unset",
    }
  }
}))

const Header = () => {
  const classes = useStyles()
  const showSideBar = useSelector(state => state.sidebar.showSideBar)
  const [authToken, setAuthToken] = useState(null)
  useEffect(() => {
    const token = getCookie('Authorization')
    setAuthToken(token);
  }, [])
  const userId = authToken && getUserId(authToken)
  
  return (
    <Grid 
      container
      direction='row'
      justifyContent='space-between'
      alignItems='center'
      className={classes.root}
    >
      <img
        src={ZedLogo}
        alt="Zed-Logo.png"
        className={classes.logo}
        onClick={() => navigate('/')}
      />
      <Grid
        container
        direction='row'
        justifyContent='flex-end'
        item
        md={9}
        xs={8}
      >
       <AuthenticationPath
          userId={userId}
          showSideBar={showSideBar}
        />
        
      </Grid>
    </Grid>
  )
}

const AuthenticationPath = ({userId, showSideBar}) => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const handleLogout = () => {
    navigate('/')
    dispatch({ type: "USER", payload: null})
    deleteCookie()
  }
  const setShowSideBar = () => {
    dispatch({ type: "SHOW_SIDEBAR", payload: !showSideBar})
  }
  
  return (
    <>
      <Button 
        className={classnames(classes.button, classes.signupBtn)} 
        onClick={() => navigate('https://zed-api-dev.zed.network/redoc/')}
        stylingProp='transparent'
      >
        API DOC
      </Button>
      {
        userId
        && <Button 
          className={classes.button} 
          onClick={() => handleLogout()}
        >
          LOGOUT
        </Button>
      }
      <SvgIcon component={ViewHeadline} className={classes.viewIcon}viewBox='0 0 24 24' onClick={()=>setShowSideBar(!showSideBar)} />
    </>
  )
}


export default Header