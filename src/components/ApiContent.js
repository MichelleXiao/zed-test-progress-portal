import React, { useState } from "react"
import { useLocation } from '@reach/router'

/** Utils **/
import { makeStyles } from '@material-ui/core/styles'

/** Components **/
import { Grid, Typography, SvgIcon, Divider, Collapse } from '@material-ui/core'
import ExpandedTestCase from "./ExpandedTestCase"
import CheckCircle from '@material-ui/icons/CheckCircleOutlineSharp'
import Attention from '@material-ui/icons/ErrorOutline'
import Star from '@material-ui/icons/StarOutlineOutlined'
import TestNumberTable from "src/components/TestNumberTable"

import { TEST_BANK_MOBILE_NUMBERS_DATA, TEST_CASH_PICKUP_NUMBERS_DATA } from '../constants/testCases'

const useStyles = makeStyles((theme) => ({
  testCaseContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '1rem'
  },
  testCaseTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    '& svg': {
      height: 35,
      width: 35,
      marginRight: '1rem'
    }
  },
  moreDetails: {
    color: theme.palette.primaryBlue,
    fontSize: 15,
    cursor: 'pointer',
    textAlign: 'end',
    '&:hover': {
      color: theme.palette.text.darkBlue,
      textDecoration: 'underline'
    }
  },
  caseName: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  completed: {
    fontSize: 16,
    color: theme.palette.primaryGreen
  },
  incomplete: {
    color: theme.palette.icon.redIcon
  },
  star: {
    color: theme.palette.primaryOrange
  },
  divider: {
    margin: '1rem -1rem'
  },
  firstDivider: {
    margin: '0 -1rem 1rem -1rem'
  }
}))

const ApiContent = ({api}) => {
  const classes = useStyles()
  const local = useLocation()
  const pathArr = local.pathname.split("/")
  const showTestNumberTable = pathArr[pathArr.length - 1]

  const renderTestNumberTable = () => {
    if(showTestNumberTable === "thunes" && api.key.includes('create') && api.key.includes('mobile')) return <TestNumberTable testNumbers={TEST_BANK_MOBILE_NUMBERS_DATA}/>
    if(showTestNumberTable === "thunes" && api.key.includes('create') && api.key.includes('cash')) return <TestNumberTable testNumbers={TEST_CASH_PICKUP_NUMBERS_DATA}/>
  }
  
  return (
    <Grid xs={12}>
      {renderTestNumberTable()}
      <Divider className={classes.firstDivider}/>
      {
        api.test_cases.map((testCase, index) => <TestCase testCase={testCase} isLastTestCase={index !== api.test_cases.length - 1}/>)
      }
    </Grid>

    
  )
}

const TestCase = ({testCase, isLastTestCase}) => {
  const [expanded, setExpanded] = useState(false)
  const classes = useStyles({expanded})
  return <>
    <Grid key={testCase.case_id} className={classes.testCaseContainer}>
      <Grid className={classes.testCaseTitle}>
        <Grid 
          container
          direction='row'
          alignItems='center'
          item
          sm={10}
        >
          <StatusIcon testCase={testCase}/>
          <Typography className={classes.caseName}>{testCase.case_name}</Typography>
        </Grid>
        {
          !expanded 
          && <Typography 
            className={classes.moreDetails}
            onClick={()=> setExpanded(true)}
          >
            More details
          </Typography>
        }
      </Grid>
      
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {
          expanded 
          && <ExpandedTestCase
            testCase={testCase}
            setExpanded={setExpanded}
          />
        }
        
      </Collapse>
    </Grid>
    { isLastTestCase && <Divider className={classes.divider}/> }
  </>
}

const StatusIcon = (testCase) => {
  const classes = useStyles()
  if(!testCase.testCase?.is_required) return <SvgIcon className={classes.star} component={Star} viewBox='0 0 24 24'  />
  return (
    <>
      {
        testCase.testCase?.is_completed
        ? <SvgIcon className={classes.completed} component={CheckCircle} viewBox='0 0 24 24'  />
        : <SvgIcon className={classes.incomplete} component={Attention} viewBox='0 0 24 24'  />
      }
    </>
  )
}
export default ApiContent