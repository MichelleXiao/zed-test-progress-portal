import React, { useState, useEffect } from "react"
import classnames from "classnames"

import {Typography, Accordion, AccordionDetails, AccordionSummary } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import ApiContent from "../components/ApiContent"
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const useStyles = makeStyles((theme) => ({
  svgIcon: {
    height: '24px',
    width: '24px',
    marginRight: '0.3rem'
  },
  accordion: {
    padding: '0.5rem 1rem',
    [theme.breakpoints.down("sm")]: {
      padding: '0.5rem',
    },
    '&.MuiAccordion-root': {
      borderTop: '1px solid #E6E6FC',
   },
    '&.MuiAccordion-root.Mui-expanded': {
      margin: 0,
   },
   '&.MuiAccordion-root.MuiPaper-elevation1': {
      boxShadow: 'none',
      '&:before':{
        backgroundColor: theme.palette.primaryLightGray2
      }
    }
  },
  firstAccordion: {
    '&.MuiAccordion-root': {
      borderTop: 'unset',
   },
  }
}))

const CustomizedAccordion = ({apiObj, index, expandedApi}) => {
  
  const isDefaultExpanded = expandedApi === apiObj.key
  const [defaultExpanded, setDefaultExpanded] = useState(isDefaultExpanded)
  const [expanded, setExpanded] = useState(false)

  const classes = useStyles()

  useEffect(()=>{
    setDefaultExpanded(isDefaultExpanded)
  },[isDefaultExpanded])

  return (
    <Accordion 
      expanded={defaultExpanded || expanded} 
      className={classnames(classes.accordion, index === 0 && classes.firstAccordion)}
    >     
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        className={classes.title}
        onClick={() => {
          defaultExpanded ? setDefaultExpanded(false) : setExpanded(!expanded)
        }}
      >
        <Typography variant="h6" gutterBottom>{apiObj.name}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <ApiContent
          api={apiObj}
        />
      </AccordionDetails>
    </Accordion>
  )
}

export default CustomizedAccordion
