import React, {useState} from 'react';
import { TextField, Box, Typography, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Visibility, VisibilityOff } from '@material-ui/icons'
import InputAdornment from '@material-ui/core/InputAdornment';
const useStyles = makeStyles((theme) => ({
  root: {
    border: '1px solid #3273AD ',
    borderRadius: '5px',
    padding: '7px 20px',
    height: 60,
    '& .MuiInputBase-input:-webkit-autofill': {
      '-webkit-box-shadow': '0 0 0 30px white inset !important'
    }
  },
  errMsg: {
    fontSize: 14,
    color: 'red',
    margin: '3px 0 20px 0'
  },
  actionWrapper: {
    marginTop: '1rem',
    display: 'flex',
    gap: 10,
    justifyContent: 'flex-end'
  },
  visibility: {
    padding: 0,
    marginTop: '-12px'
  },
}));

const TextInputField = ({ children, isError, errMsg, ...restProps }) => {
  const classes = useStyles()
  const [isPassword, setIsPassword] = useState(restProps.label === 'Password')
  const [showPassword, setShowPassword] = useState(false)
  return (
    <>
      <Box className={classes.root}>
          <TextField
            {...restProps} 
            type={isPassword && !showPassword ? 'password' : 'text'}
            InputProps={{ 
              disableUnderline: true,
              endAdornment: isPassword && ( 
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPassword(!showPassword)}
                    className={classes.visibility}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
              style:{
                fontSize: '15px', 
                fontWeight:'600',
                '& input': {
                  '-webkit-box-shadow': '0 0 0 30px white inset !important'
                }
              },
            }}
            InputLabelProps={{
              shrink: true,
              style: {
                fontSize: 14,
                color: '#333333',
              }
            }}
            
          />
      </Box>
      <Typography className={classes.errMsg}>
        {
          isError
          && errMsg
        }
      </Typography>
    </>
  );
}

export default TextInputField
