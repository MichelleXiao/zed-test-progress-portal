import React from "react"

import { Grid, Toolbar, Typography } from "@material-ui/core"
import { makeStyles } from '@material-ui/core/styles'

import CardContainer from "./CardContainer"
import LoadingButton from "./LoadingButton"

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: 63,
    display: 'flex',
    padding: '0 2rem',
    borderBottom: `1px solid ${theme.palette.button.primaryBlue}`,
    color: theme.palette.primaryWhite,
    backgroundColor: theme.palette.button.primaryBlue,
    borderRadius: '5px 5px 0 0'
  },
  mainContent: {
    backgroundColor: theme.palette.primaryWhite,
    borderRadius: `5px`
  }
}))

const CategoryLayout = ({
  headerContentText,
  children
}) => {
  const classes = useStyles()
  const Header = () => {
    return (
      <Toolbar className={classes.headerContainer} >
        <Typography variant='h6'>{headerContentText}</Typography>
      </Toolbar>
    )
  }
  return (
    <>
      <CardContainer
        header={<Header />}
        contentPadding="0"
      >
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="center"
          item
          sm={12}
          className={classes.mainContent}
        >
          { children }
        </Grid>
      </CardContainer>
    </>
  )
}

export default CategoryLayout