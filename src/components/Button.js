import React from "react"
/** Utils **/
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'

/** Components **/
import { Button, CircularProgress } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  loadingCircle: {
    color: props => props.color,
    height: '20px !important',
    width: '20px !important',
  },
  buttonWrapper: {
    borderRadius: 30,
    width: '100%',
    color: props => props.color,
    backgroundColor: props => props.backgroundColor,
    textTransform: props => props.textTransform || 'none',
    border: props => props.border,
    '&:hover': {
      backgroundColor: props => props.hover,
      color: theme.palette.primaryWhite,
      border: 'none'
    }
  }
}))

const CustomizedButton = ({ 
  children,
  classes: classesProps,
  isLoading,
  color,
  textTransform,
  endIcon,
  startIcon,
  stylingProp,
  ...rest 
}) => {
  
  const theme = useTheme();
  const determineStyling = () => {
    switch(stylingProp) {
      case 'lightBlue': 
        return {
          border: 'none',
          backgroundColor: '#15A0F5',
          color: theme.palette.primaryWhite,
          hover: '#45B8FF'
        }
      case 'transparent': 
        return {
          border: '2px solid #3273AD',
          backgroundColor: 'transparent',
          color: theme.palette.button.primaryBlue,
          hover: '#15A0F5'
        }
      case 'red':
        return {
          border: `2px solid ${theme.palette.primaryRed}`,
          backgroundColor: 'transparent',
          color: theme.palette.primaryRed,
          hover: theme.palette.primaryRed
        }
      default: 
        return {
          border: 'none',
          backgroundColor: theme.palette.button.primaryBlue,
          color: theme.palette.primaryWhite,
          hover: '#15A0F5'
        }
    }
  }
  const props = determineStyling()

  const classes = useStyles({
    textTransform: textTransform,
    border: props.border,
    color: props.color,
    backgroundColor: props.backgroundColor,
    hover: props.hover
  })
  
  return (
    <Button
      {...rest}
      classes={{ root: classes.buttonWrapper, ...classesProps }}
      startIcon={!isLoading && startIcon}
      endIcon={!isLoading && endIcon}
    >
      {
        isLoading 
        ? <CircularProgress className={classes.loadingCircle} /> 
        : <span> {children} </span>
      }

    </Button>
  )
}

CustomizedButton.propTypes  = {
  isLoading: PropTypes.bool
}

export default CustomizedButton