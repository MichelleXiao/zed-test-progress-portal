import React, { forwardRef } from "react"
import Box from "@material-ui/core/Box"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
/**
 * The CardContainer is Material UI container is mostly used Content wrapper with ZED webportal UI
 *
 * It has 3 specification
 * 1. The Container should have a flexibility to render any content
 * 2. The Container can able to have a title or None
 * 3. The Contaner has the flexibility to have a header (component as prop) with actions such as search textbox
 * @returns
 */
const CardContainer = ({ title, header: Header, className, children, elevation = 0, wrapperPadding = '0', contentPadding = '1rem' }, ref) => {
  return (
    <div style={{ padding: wrapperPadding, width: '100%' }} ref={ref}>
      <Paper elevation={elevation} className={className} >
        {Header ? Header : <TitleHeader title={title} />}
        <Box p={contentPadding}>{children}</Box>
      </Paper>
    </div>
  )
}

const TitleHeader = ({ title }) => {
  if (!title) {
    return null
  }
  return (
    <>
      <Box p="1rem 1rem 0">
        <Typography variant="h6"> {title}</Typography>
      </Box>
    </>
  )
}

export default forwardRef(CardContainer)
