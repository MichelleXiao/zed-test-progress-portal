import React, { useState } from "react"
import { makeStyles } from '@material-ui/core/styles'

import { Grid, LinearProgress, Typography, Collapse, List, ListItem, Divider, ListItemText } from '@material-ui/core'

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    padding: '3rem 3rem 2rem ',
    '& .MuiLinearProgress-bar': {
      borderRadius: 30,
    },
    '& .MuiLinearProgress-barColorPrimary': {
      backgroundColor: theme.palette.primaryBlue
    },
    '& .MuiLinearProgress-colorPrimary': {
      backgroundColor: theme.palette.primaryLightGray3
    }
  },
  progressBar: {
    width: '100%',
    height: 30,
    borderRadius: '30px'
  },
  completion: {
    textAlign: 'center',
    marginTop: '0.5rem',
    fontSize: 14,
    fontWeight: theme.typography.fontWeightLarge,
    cursor: 'pointer',
    '& svg': {
      paddingTop: props => !props.expanded && 15,
      transform: props => props.expanded && 'rotate(180deg)',
      paddingBottom: props => props.expanded && 15,
    }
  },
  listItemText: {
    '& .MuiTypography-body1': {
      fontSize: 15
    },
    '& .MuiTypography-body2': {
      fontSize: 12,
    },
  },
  testCaseName: {
    fontSize: '15px !important',
    textDecoration: 'underline',
    lineHeight: 1.9
  }
}))

const ProgressBar = ({progressData}) => {
  const [expanded, setExpanded] = useState(false)
  const classes = useStyles({expanded: expanded})
  const percentage = progressData.numOfCompletedTestCases/progressData.numOfRequiredTestCases * 100
  const isIncomplete = progressData?.incompleteApiList?.length > 0
  return (
    <Grid className={classes.root}>
      <LinearProgress className={classes.progressBar} variant="determinate" value={percentage} />
      <Typography className={classes.completion} onClick={()=> isIncomplete && setExpanded(!expanded)} >
        {`Completed: ${progressData.numOfCompletedTestCases}/${progressData.numOfRequiredTestCases}`} 
        { isIncomplete && <KeyboardArrowDownIcon/> }
      </Typography>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {
          expanded 
          && <ListOfIncompleteTestCases
            incompleteApiList={progressData.incompleteApiList}
            progressData={progressData}
          />
        }
      </Collapse>
    </Grid>
  )
}

const ListOfIncompleteTestCases = ({incompleteApiList}) => {
  return (
    <List>
      {
        incompleteApiList.map(api => <IncompleteTestCases api={api}/>)
      }
    </List>
  )
}

const IncompleteTestCases = ({api}) => {
  const classes = useStyles()
  return (
    <ListItem>
      <ListItemText
        className={classes.listItemText}
        primary={api.name}
        secondary={
          <>
            {
              api.test_cases.map(testCase => !testCase.is_completed && <>
                <Typography
                  sx={{ display: 'inline'}}
                  component="span"
                  variant="body2"
                  className={classes.testCaseName}
                  key={testCase.case_id}
                >
                  {testCase.case_name} 
                </Typography>
                {` -  ${testCase.instruction}`}
                <br/>
              </>)
            }
          </>
        }
      />
    </ListItem>
  )
}

export default ProgressBar
