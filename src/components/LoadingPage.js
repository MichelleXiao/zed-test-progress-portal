import React from "react"
import Grid from "@material-ui/core/Grid"
import { keyframes } from "styled-components"
import styled from "styled-components"
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  conatiner: {
    display: 'flex',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    minHeight: 500,
  },
}));

const moveFirst = keyframes`
  0%   {left: 35%;}
  100% {left: 58%;}
  }
`
const moveSecond = keyframes`
  0%   {left: 58%;}
  100% {left: 79%;}
  }
`
const moveThird = keyframes`
  0%   {left: 79%;transform: rotate(0deg);}
  100% {left: 35%;transform: rotate(360deg);}
  }
`
const Loader = styled.div`
position: absolute;
box-sizing: border-box;
margin: 0;
  padding: 0;
  overflow: hidden;
  margin-left: 2rem
  transform: translate(-50%, -50%);
  width: 4em;
  height: 4em;
  border: 0.2em solid #4F6382;
  border-radius: 50%;
  animation: 4s 0s ease infinite alternate;
  &:before {
    width: 4em;
    height: 4em;
    border: 0.2em solid transparentize(#4F6382, 0.25);
    border-radius: 50%;
    animation: 4s 0s ease infinite alternate;
    content: '';
    position: absolute;
    display: block;
    box-sizing: border-box;
  },
  &:after {
    width: 4em;
    height: 4em;
    border: 0.2em solid transparentize(#4F6382, 0.75);
    border-radius: 50%;
    animation: 4s 0s ease infinite alternate;
    content: '';
    position: absolute;
    display: block;
    box-sizing: border-box;
  }
`
const Dot = styled.div`
    position: absolute;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    top: 50%;
    width:0.6em;
    height: 0.6em;
    overflow: visible;
    transition: all 1s ease;
    transform: rotate(0deg);
    &:after {
      top: 0;
      left: calc(50% - 0.8em);
      width: 0.5em;
      height: 0.5em;
      background: #4F6382;
      border-radius: 50%;
      content: '';
      position: absolute;
      display: block;
      box-sizing: border-box;
    };
    &:before {
      content: '';
      position: absolute;
      display: block;
      box-sizing: border-box;
    };
    &:nth-child(1) {
      left: calc(25% - 0.5em);
      animation: ${moveFirst} 2s 0s ease infinite;
    };
    &:nth-child(2) {
      left: calc(50% - 0.5em);
      animation: ${moveSecond} 2s 0s ease infinite;
    };
    &:nth-child(3) {
      left: calc(67% - 0.5em);
      animation: ${moveThird} 2s 0s ease infinite;
    };
  }
`
const LoadingPage = () => {
  const classes = useStyles()
  return (
    <Grid 
      container
      item
      alignItems='center'
      xs={12}
      className={classes.conatiner}
    >
      <Loader>
        <Dot/>
        <Dot/>
        <Dot/>
      </Loader>
      <Typography 
        variant='h6'
        style={{
          marginTop: '6rem',
          color: '#4F6382'
        }}
      >
        Loading
      </Typography>
    </Grid>
  )
}

export default LoadingPage