import React, { useState, useEffect } from "react"

/** Utils **/
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'

/** Components **/
import { Grid, Button, Typography, SvgIcon, Card, CardHeader, CardMedia, CardContent, CardActions, Collapse } from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircleOutline';
import CodeContent from "./CodeContent";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: '0 3rem',
    [theme.breakpoints.down("sm")]: {
      padding: '0 1rem',
    },
  },
  instruction: {
    fontSize: 15,
    margin: '1rem 0'
  },
  lessDetails: {
    color: theme.palette.primaryBlue,
    marginTop: '1rem',
    fontSize: 15,
    cursor: 'pointer',
    textAlign: 'end',
    '&:hover': {
      color: theme.palette.text.darkBlue,
      textDecoration: 'underline'
    }
  }
}))

const ExpandedTestCase = ({testCase, setExpanded}) => {
  const theme = useTheme()
  const classes = useStyles()
  
  return (
    <Grid xs={12} className={classes.root}>
      <Typography className={classes.instruction}>
        {testCase?.instruction}
      </Typography>
      <CodeContent
        codeStr={testCase?.expected_result}
      />
      <Typography 
        className={classes.lessDetails}
        onClick={()=> setExpanded(false)}
      >
        Less details
      </Typography>
    </Grid>
    
  )
}


export default ExpandedTestCase