import React, { useEffect, useState } from "react"
import { navigate } from "gatsby"
import { useDispatch, useSelector } from "react-redux"
import { makeStyles } from '@material-ui/core/styles'


import { Grid,  Typography, SvgIcon, CircularProgress } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.button.primaryBlue,
    height: '60px',
    width: '100%',
  },
  copyRight: {
    color: theme.palette.primaryWhite,
    fontSize: 16
  }
}))

const Footer = () => {
  const classes = useStyles()
  return (
    <Grid 
      container
      direction='row'
      justifyContent='center'
      alignItems='center'
      className={classes.root}
    >
      <Typography className={classes.copyRight}>Copyright © Zed Network. All Right Reserved</Typography>
      
    </Grid>
  )
}

export default Footer
