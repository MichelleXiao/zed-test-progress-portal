import React, { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useLocation } from '@reach/router'

import { Paper, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, Collapse } from '@material-ui/core'

import { ClientService } from "../services/zed-api"
import toastHook from "../hooks/toastHook"
import LoadingPage from "../components/LoadingPage"
import CategoryLayout from "src/components/CategoryLayout"
import { THUNES, CORPAY, TEST_BANK_MOBILE_NUMBERS_DATA, TEST_CASH_PICKUP_NUMBERS_DATA } from '../constants/testCases'
import CustomizedAccordion from "../components/Accordion"
import {checkThunesTransfers, checkCorpayTransfers} from "src/services/CheckTransferStatus"
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  conatiner: {
    display: 'flex',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    minHeight: 500,
  },
  collapseSummary: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    marginBottom: '1.5rem',
  },
  title: {
    fontSize: 14
  },
  learnMore: {
    color: theme.palette.primaryBlue,
    fontSize: 12,
    cursor: 'pointer',
    textAlign: 'end',
    width: '100%',
    '&:hover': {
      color: theme.palette.text.darkBlue,
      textDecoration: 'underline'
    },
  },
  tableBackground: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent:'center',
    width: '100%',
    alignItems: 'center'
  },
  tableContainer: {
    display: 'flex',
    justifyContent: 'center',
    maxWidth: "70%",
    margin: "1rem 0",
    [theme.breakpoints.down("md")]: {
      maxWidth: '100%',
    }
  },
  tableHead: {
    '& th': {
      padding: '0.6rem !important',
      fontSize: 14,
      fontWeight: theme.typography.fontWeightLarge,
    }
  },
  tableRow: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.primaryLightGray4,
    },
    '& th': {
      padding: '0.6rem !important',
      fontSize: 14
    }
  }
}));

const TestNumberTable = ({testNumbers}) => {
  const classes = useStyles()
  const local = useLocation()
  const [expanded, setExpanded] = useState(false)
  return (
    <>
      <Grid
        direction="row"
        justifyContent="space-between"
        className={classes.collapseSummary}
      >
        <Typography className={classes.title}>{testNumbers.title}</Typography>
        <Typography 
          className={classes.learnMore} 
          onClick={() => {setExpanded(!expanded)}}
        >
          {expanded? "Show Less" : "Show More"}
        </Typography>
      </Grid>
      
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        {
          expanded 
          && <ExpandedTestNumberContent
            testNumbers={testNumbers}
            setExpanded={setExpanded}
          />
        }
      </Collapse>
      
    </>
    
  )
}

const ExpandedTestNumberContent = ({testNumbers}) => {
  const classes = useStyles()
  const testNumberTableHeadArr = Object.keys(testNumbers.test_numbers[0]) 
  return (
    <>
      
      <Grid className={classes.tableBackground}>
        <TableContainer className={classes.tableContainer} component={Paper}>
          <Table className={classes.table} sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
              <TableRow className={classes.tableHead}>
                <TableCell>Test Number</TableCell>
                <TableCell align="right">Expected Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {testNumbers.test_numbers.map((testNumber, index) => (
                <TableRow
                  key={index}
                  className={classes.tableRow}
                  sx={{ 
                    '&:last-child td, &:last-child th': { border: 0 }
                  }}
                >
                  <TableCell component="th" scope="row">
                    {testNumber.test_number}
                  </TableCell>
                  <TableCell component="th" scope="row" align="right">
                    {testNumber.expected_status}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
      
    </>
  )
  
}

export default TestNumberTable
