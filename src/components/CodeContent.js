import React, { useState, useEffect } from "react"

/** Utils **/
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@material-ui/core/styles'

/** Components **/
import { Grid, Button, Typography, SvgIcon, Card, CardHeader, CardMedia, CardContent, CardActions, Collapse } from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircleOutline';
import { mergeClasses } from "@material-ui/styles";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: '1rem',
    backgroundColor: theme.palette.primaryDarkGrey,
    '& p': {
      fontSize: 14,
      color: theme.palette.primaryWhite
    }
  },
}))

const CodeContent = ({codeStr}) => {
  const theme = useTheme()

  const classes = useStyles({
    // textTransform: textTransform,
    // border: props.border,
    // color: props.color,
    // backgroundColor: props.backgroundColor,
    // hover: props.hover
  })
  let whiteSpaceCounter = 0
  const codeArr = codeStr.split('\n')
  const newText = codeArr.map(str => {
    if(str === "}" || str === "},") {
      whiteSpaceCounter --
    }
    const formattedText = <Typography style={{marginLeft: `calc(30px * ${whiteSpaceCounter})`}}>{str}</Typography>
    if(str === "{" || str.includes(":{")) {
      whiteSpaceCounter ++
    }  
    return formattedText
  })
  return (
    <Grid xs={12} className={classes.root}>
      {newText}
    </Grid>
    
  )
}


export default CodeContent