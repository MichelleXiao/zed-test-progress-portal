import React, {useEffect, useState} from "react"
import { navigate } from "gatsby"
import jwt from "jsonwebtoken"
import { getCookie } from "../services/Cookies"
import toastHook from "../hooks/toastHook"

import { AuthLayout } from './layout';


const PrivateRoute = ({ component: Component, location, ...rest }) => {
  const authToken = getCookie("Authorization")
  const toastDispatcher = toastHook()
  if (!authToken || !isAuthClientBase(authToken)) {
    navigate(
      `/`,
      {
        state: { 
          conversionInfo: location?.state?.conversionInfo
        },
      }
    )
    toastDispatcher("You don't have an access to this page, Please login", "info")
    return null
  }
  return (
    <AuthLayout location={location} {...rest}>
      <Component />
    </AuthLayout>
  )
}

const isAuthClientBase = (authToken) => {
  authToken = authToken.substring(7, authToken.length);
  const decoded = jwt.decode(authToken);
  const permissions = decoded?.permissions
  return permissions?.includes("CAN_CREATE_CLIENT")
}

export const getUserId = (authToken) => {
  if (!authToken) return null;
  authToken = authToken.substring(7, authToken.length);
  const decoded = jwt.decode(authToken);
  return decoded?.id
}

export default PrivateRoute
