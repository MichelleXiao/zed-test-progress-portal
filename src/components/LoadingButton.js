import React from "react"
import PropTypes from 'prop-types';
import { makeStyles, useTheme, darken } from '@material-ui/core/styles'
import {
  Button,
  CircularProgress
} from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  loadingCircle: {
    color: theme.palette.primaryWhite,
    height: '20px !important',
    width: '20px !important',
  },
  buttonWrapper: {
    color: theme.palette.primaryWhite,
    backgroundColor: props => props.color,
    textTransform: props => props.textTransform,
    '&:hover': {
      backgroundColor: props => darken(props.color, 0.2),
    },
  }
}))

const LoadingButton = ({ 
  children,
  classes: classesProps,
  isLoading,
  color,
  textTransform,
  endIcon,
  startIcon,
  ...rest 
}) => {
  LoadingButton.propTypes  = {
    isLoading: PropTypes.bool
  }
  const theme = useTheme();

  let selectedColor = color || theme.palette.primaryBlue
  switch (color) {
    case 'green':
      selectedColor = theme.palette.primaryGreen
      break;
    case 'grey':
      selectedColor = theme.palette.primaryGrey
      break;
    case 'blue':
      selectedColor = theme.palette.primaryBlue
      break;
    case 'white':
      selectedColor = theme.palette.primaryWhite
      break;
    case 'red':
      selectedColor = theme.palette.icon.redIcon
      break;
    default:
      break;
  }

  const classes = useStyles({
    color: selectedColor,
    textTransform
  })
  return (
    <Button
      {...rest}
      classes={{ root: classes.buttonWrapper, ...classesProps }}
      startIcon={!isLoading && startIcon}
      endIcon={!isLoading && endIcon}
    >
      {
        isLoading 
        ? <CircularProgress className={classes.loadingCircle} /> 
        : <span> {children} </span>
      }

    </Button>
  )
}

export default LoadingButton