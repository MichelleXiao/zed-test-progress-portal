import React from "react"

// Utils
import { useSelector } from "react-redux"

// Components
import { Grid } from '@material-ui/core'
import LoadingPage from "../components/LoadingPage"
import CategoryLayout from "src/components/CategoryLayout"
import CustomizedAccordion from "../components/Accordion"
import ProgressBar from "../components/ProgressBar"

// Services
import { checkSenderRecipientStatus } from "src/services/CheckSenderRecipientStatus"
import { percentageOfCompleteness } from "src/services/helper"

// Assets
import { SENDER } from '../constants/testCases'

const Sender = ({location}) => {
  const senders = useSelector(state => state.clientBankAccounts.senders)
  const senderApiStatuses = senders?.senders && checkSenderRecipientStatus(senders.senders)
  const updateSenderApis = () => {
    const updatedSenderApis = SENDER.map(senderApi => {
      const updatedTestCases = senderApi.test_cases.map(testCase => {
        return ({
          ...testCase,
          is_completed: testCase.is_required ? senderApiStatuses[testCase.case_id] : false
        })
      })
      return ({
        ...senderApi,
        test_cases: updatedTestCases
      })
    })
    return updatedSenderApis
  }
  
  const updatedSenderApis = senderApiStatuses && updateSenderApis()

  return (
    <>
      {
        !senderApiStatuses
        ? <LoadingPage/>
        : <CategoryLayout
          headerContentText="Sender"
        >
          <Grid container direction='column'>
            <ProgressBar progressData={percentageOfCompleteness(updatedSenderApis)}/>
            {
              updatedSenderApis.map((apiObj, index) => <CustomizedAccordion 
                  key={apiObj.key}
                  apiObj={apiObj}
                  index={index}
                  expandedApi={location.state.expandedApi}
                />     
              )
            }
          </Grid>
        </CategoryLayout>
      }
    </>
  )
}

export default Sender
