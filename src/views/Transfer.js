import React from "react"

// Utils
import { useSelector } from "react-redux"
import { useLocation } from '@reach/router'

// Components
import { Grid } from '@material-ui/core'
import LoadingPage from "../components/LoadingPage"
import CategoryLayout from "src/components/CategoryLayout"
import ProgressBar from "../components/ProgressBar"
import CustomizedAccordion from "../components/Accordion"

// Services
import {checkThunesTransfers, checkCorpayTransfers} from "src/services/CheckTransferStatus"
import { percentageOfCompleteness } from "src/services/helper"

// Assets
import { THUNES, CORPAY } from '../constants/testCases'

const Transfer = ({location}) => {
  
  const local = useLocation()
  const transfers = useSelector(state => state.clientBankAccounts.transfers)
  
  const matchTransferList = () => {
    let apiArr = []
    let transferApiStatuses
    if(local.pathname.includes("thunes")){
      apiArr = THUNES
      transferApiStatuses = transfers?.transfers && checkThunesTransfers(transfers?.transfers)
    } 
    if(local.pathname.includes("corpay")){
      apiArr = CORPAY
      transferApiStatuses = transfers?.transfers && checkCorpayTransfers(transfers?.transfers)
    } 
    return updateTransferApis(apiArr, transferApiStatuses)
  }
  
  const updateTransferApis = (apiArr, transferApiStatuses) => {
    const updatedTransferApis = apiArr.map(transferApi => {
      const updatedTestCases = transferApi.test_cases.map(testCase => {
        return ({
          ...testCase,
          is_completed: transferApiStatuses[testCase.case_id] || false
        })
      })
      return ({
        ...transferApi,
        test_cases: updatedTestCases
      })
    })
    return updatedTransferApis
  }

  const completedTransferList = transfers?.transfers && matchTransferList()
 
  return (
    <>
      {
        !completedTransferList
        ? <LoadingPage/>
        : <CategoryLayout
          headerContentText="Transfer"
        >
          <ProgressBar progressData={percentageOfCompleteness(completedTransferList)}/>
          <Grid container direction='column'>
            {
              completedTransferList.map((apiObj, index) => <CustomizedAccordion 
                  key={apiObj.key}
                  apiObj={apiObj}
                  index={index}
                  expandedApi={location.state.expandedApi}
                />     
              )
            }
          </Grid>
        </CategoryLayout>
      }
    </>
  )
}

export default Transfer
