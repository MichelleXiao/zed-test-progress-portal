import React from "react"

// Utils
import { useSelector } from "react-redux"

// Components
import { Grid } from '@material-ui/core'
import LoadingPage from "../components/LoadingPage"
import CategoryLayout from "src/components/CategoryLayout"
import ProgressBar from "../components/ProgressBar"
import CustomizedAccordion from "../components/Accordion"

// Services
import { checkWalletStatus } from "../services/CheckSenderRecipientStatus"
import { percentageOfCompleteness } from "../services/helper"
// Assets
import { WALLET } from '../constants/testCases'

const Wallet = ({location}) => {
  const recipients = useSelector(state => state.clientBankAccounts.recipients)
  const walletApiStatuses = recipients?.recipients && checkWalletStatus(recipients.recipients)
  
  const updateWalletApis = () => {
    const updatedWalletApis = WALLET.map( walletApi => {
      if(walletApi.key !== "create_wallet") return walletApi
      const updatedTestCases = walletApi.test_cases.map(testCase => {
        return ({
          ...testCase,
          is_completed: walletApiStatuses[testCase.case_id]
        })
      })
      return ({
        ...walletApi,
        test_cases: updatedTestCases
      })
    })
    return updatedWalletApis
  }
  
  const updateddApis = walletApiStatuses && updateWalletApis()
  
  return (
    <>
      {
        !walletApiStatuses
        ? <LoadingPage/>
        : <CategoryLayout
          headerContentText="Wallet"
        >
          <Grid container direction='column'>
            <ProgressBar progressData={percentageOfCompleteness(updateddApis)}/>
            {
              updateddApis.map((apiObj, index) => <CustomizedAccordion 
                  key={apiObj.key}
                  apiObj={apiObj}
                  index={index}
                  expandedApi={location.state.expandedApi}
                />     
              )
            }
          </Grid>
        </CategoryLayout>
      }
    </>
  )
}

export default Wallet
