import React from "react"
import { navigate } from 'gatsby'
import classnames from "classnames"
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Button, Typography, SvgIcon } from "@material-ui/core"
import LeftArrowSVG from "../../images/icons/leftArrowSVG.inline.svg"
import LoadingButton from "../../components/LoadingButton"

const useStyles = makeStyles((theme) => ({
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '16px 25px 16px 8px',
    borderTop: '1px solid #DDDDE2',
    backgroundColor: theme.palette.primaryLightGray4,
    borderRadius: '0 0 0.7rem 0.7rem',
    flexWrap: 'wrap'
  },
  subTitle: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.lightGray2,
    marginLeft: '1rem',
    lineHeight: '1.4rem'
  },
  button: {
    width: '180px',
    height: 40,
    borderRadius: 8,
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightLarge,
    boxShadow: 'none',
    fontSize: 13,
    '&:focus': {
      outline: 0
    },
    '&:hover': {
      boxShadow: 'none',
    },
    [theme.breakpoints.down("lg")]: {
      width: '140px',
      fontSize: 11
    }
  },
  whiteButton: {
    border: "1px solid #E4E4E4",
    marginRight: '10px',
    [theme.breakpoints.down("sm")]: {
      marginRight: 5,
    }
  },
  svgIcon: {
    marginRight: '1rem',
    [theme.breakpoints.between("md", "lg")]: {
      marginRight: '0',
      display: 'none'
    }
  },
  warnMsg: {
    color: theme.palette.icon.redIcon,
    fontSize: '12px',
    marginTop: '5px',
    width: '100%',
    textAlign: 'end'
  }
}))

const Footer = ({activeStep, setActiveStep, isLoading, setIsLoading, setUpdateRate, setGetQuote, setIsConfirmed, isLocked, matches, t}) => {
  const classes = useStyles()
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleConfirm = () => {
    setIsLoading(true)
    setIsConfirmed(true)
  }

  const renderConfirmButton = () => <LoadingButton
    type="submit"
    variant="contained"
    color="green"
    isLoading={isLoading}
    disabled={isLoading || isLocked}
    className={classnames(classes.button)}
    onClick={()=>handleConfirm()}
  >
    {t("Confirm")}
  </LoadingButton>

  const renderUpdateRateButton = () => <LoadingButton
      type="submit"
      color="white"
      variant="contained"
      isLoading={isLoading}
      style={{color: '#333333'}}
      disabled={isLoading}
      className={classnames(classes.button, classes.whiteButton)}
      onClick={()=>setUpdateRate(true)}
    >
      {t("Update Rate")}
    </LoadingButton>

  const renderGetQuoteButton = () => <LoadingButton
      type="submit"
      variant="contained"
      color="green"
      disabled={isLoading || isLocked}
      isLoading={isLoading}
      className={classnames(classes.button)}
      onClick={()=>setGetQuote(true)}
    >
      {t("Get Quote")}
    </LoadingButton>
  return (
    <Grid className={classes.footer}>
      <Grid item md={3} xs={2}>
        <Button 
          style={{width: 'auto', padding: '10px', color: '#868686'}} 
          className={classnames(classes.button)}
          disabled={isLoading}
          onClick={()=>{
            if (activeStep === 0){
              navigate("/client/accounts")
            } else{
              handleBack()
            }
          }}
        >
          <SvgIcon component={LeftArrowSVG} viewBox='0 0 24 24' className={classes.svgIcon} />
          { 
            matches
            && t(activeStep === 0 ? "Back to Account" : "Back" )
          }
        </Button>
      </Grid>
      <Grid container direction='row' justifyContent='flex-end' item md={9} xs={10}>
        {renderUpdateRateButton()}
        {activeStep === 0 ? renderGetQuoteButton() : renderConfirmButton() }
      </Grid>
      { isLocked && <Typography className={classes.warnMsg}>{t("Spot rate")} <strong>{t("locked")}</strong>. {t("Update rate to proceed")}.</Typography>}
    </Grid>
  )
}

export default Footer