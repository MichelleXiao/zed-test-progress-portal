import React from "react"
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, Toolbar } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  toolBar:{
    display: 'flex',
    justifyContent: 'space-between',
    padding: '16px 25px',
  },
  title: {
    marginRight: '1rem',
  },
  subTitle: {
    fontSize: '12px',
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.text.lightGray2,
    lineHeight: '1.4rem',
  },
  countDown: {
    color: theme.palette.icon.redIcon,
    fontSize: '11px',
    '& strong': {
      fontWeight: theme.typography.fontWeighLarge
    }
  }
}))

const Header = ({activeStep, timer, matches, t}) => {
  const classes = useStyles()
  return (
    <Toolbar className={classes.toolBar}>
      <Grid container direction='row' item md={activeStep === 0 ? 12 : 8} xs={4}>
        <Typography variant='h6' className={classes.title}>{t(activeStep === 0 ? "Convert" : "Review")}</Typography>
        {
          matches 
          && <Typography variant='h6' className={classes.subTitle}>
            ({
              t(activeStep === 0 
                ? "Select currency and amount to convert" 
                : "Please review the quote before purchasing"
              )
            })
          </Typography>
        }

      </Grid>
      {activeStep === 1
        && <Grid container direction='row' justifyContent='flex-end' item md={4} xs={8}>
          { timer !== 0 
            ? <Typography className={classes.countDown}> {t("Time remaining")} <strong>{timer} {t("seconds")}</strong></Typography>
            : <Typography className={classes.countDown}> {t("Spot rate")} <strong> {t("locked")}.</strong> {t("Click")} <strong>{t("Update Rate")}</strong></Typography>
          }
        </Grid>
      }
    </Toolbar>
  )
}

export default Header