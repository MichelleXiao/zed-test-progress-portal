import React from "react"
import { navigate } from 'gatsby'
import PropTypes from "prop-types"

import { makeStyles } from '@material-ui/core/styles'
import { Grid, Box, Typography } from "@material-ui/core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons"
import { transformDate, transformDatePresentation, formatDisplayAmount } from "../../services/helper"
import LoadingButton from "../../components/LoadingButton"
const useStyles = makeStyles((theme) => ({
  root: {
    width: '30rem',
    padding: '2rem'
  },
  checkIcon: {
    color: theme.palette.primaryGreen,
    width: '85px !important',
    height: '85px',
    marginBottom: '20px'
  },
  boldText: {
    color: theme.palette.text.primary,
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 12,
  },
  transactionDetailContainer:{
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  },
  transactionAccountContainer: {
    backgroundColor: theme.palette.primaryLightGray,
    borderRadius: '0.7rem',
    margin: '1rem',
    padding:'1.5rem 3rem',
  },
  title: {
    marginTop: '10px',
    fontSize: '13px',
    '& strong': {
      color: '#868686',
      fontWeight: theme.typography.fontWeightMedium,
    }
  },
  transaction: {
    marginTop: '10px',
    fontSize: '13px',
    color: theme.palette.primaryGreen,
    '& strong': {
      color: '#868686',
      fontWeight: theme.typography.fontWeightMedium,
    }
  },
  button: {
    width: '94%',
    marginTop: '2rem',
    padding: '0.5rem',
    textTransform: 'none'
  }
}))
const Confirmation = ({
  confirmationInfo
}) => {
  const classes = useStyles()
  const estimateDeliveryDate = () => {
    const today = new Date()
    return transformDatePresentation(transformDate(today))
  }
  return (
    <Grid
      container
      direction='column'
      justifyContent='flex-start'
      alignItems='center'
      item
      xs={12}
      className={classes.root}
    >
      <FontAwesomeIcon icon={ faCheckCircle } className={classes.checkIcon}/>
      <Typography variant='h6' style={{fontSize: '18px'}}>Currency Conversion Completed!</Typography>
      <Box className={classes.transactionDetailContainer}>
        <Grid 
          container
          direction='column'
          justifyContent='flex-start'
          item
          xs={12}
          className={classes.transactionAccountContainer}
        >
          <Typography variant='h6' style={{marginBottom: '10px'}}>Transaction details: </Typography>
          <Typography variant='h6' className={classes.title}><strong>Transfer ID: </strong>{confirmationInfo.transfer_id}</Typography>
          <Typography variant='h6' className={classes.title}><strong>Partner Reference ID: </strong>{confirmationInfo.partner_reference_id}</Typography>
          <Typography variant='h6' className={classes.transaction}><strong>Sold: </strong>{formatDisplayAmount(confirmationInfo.sell_currency, confirmationInfo.settlement_amount)}</Typography>
          <Typography variant='h6' className={classes.transaction}><strong>Bought: </strong>{formatDisplayAmount(confirmationInfo.buy_currency, confirmationInfo.converted_amount)}</Typography>
          <Typography variant='h6' className={classes.transaction}><strong>At rate: </strong>{`1 ${confirmationInfo.sell_currency} : ${confirmationInfo.calculate_rate.toFixed(2)} ${confirmationInfo.buy_currency}`}</Typography>
          <Typography variant='h6' className={classes.title}><strong>Transaction Date: </strong>{estimateDeliveryDate()}</Typography>
        </Grid>
      </Box>
      <LoadingButton 
        type="button"
        variant="contained"
        onClick={()=>{navigate("/client/dashboard")}}
        color="green"
        className={classes.button}
      >
        Confirmed
      </LoadingButton>
    </Grid>
  )
}
Confirmation.propTypes = {
  confirmationInfo: PropTypes.object.isRequired
}

export default Confirmation
