import React, { useEffect } from "react"

/**  Utils  **/
import { useTranslation } from 'gatsby-plugin-react-i18next'
import { makeStyles } from '@material-ui/core/styles'

/**  Hooks  **/
import ToastHook from "../../hooks/toastHook"

/**  Components  **/
import { Grid, Typography, Box } from "@material-ui/core"
import CurrencyInputField from "src/components/FormField/CurrencyInput/CurrencyInputField"

/**  Services  **/
import { toCurrencyFormat, getHttpErrorMessage, handleTerms } from "../../services/helper"
import { TransferService } from "../../services/TransferService"

/**  Assets  **/
import currencies from "../../constants/currencies.json"

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2rem 4rem 1rem',
    backgroundColor: theme.palette.primaryLightGray4,
    borderTop: '1px solid #F1F1F6',
    [theme.breakpoints.down("lg")]: {
      padding: '1rem 1.5rem',
    },
    [theme.breakpoints.down("sm")]: {
      padding: '1rem 1rem',
    }
  },
  currencyInputContainer: {
    marginBottom: '32px',
    width: '48%',
    [theme.breakpoints.down("md")]: {
      width: '100%',
    }
  },
  title: {
    color: theme.palette.text.lightGray2,
    fontSize: '14px',
    marginBottom:'13px'
  },
  conversionRateArea: {
    height: 60,
    width: '100%',
    backgroundColor: theme.palette.primaryWhite,
    border: theme.overrides.greyBorder,
    borderRadius: '5px',
    textAlign: 'center',
    padding: '16px',
  },
  conversionRate: {
    fontSize: '13px',
    color: theme.palette.text.darkGray,
    '& strong': {
      fontSize: '15px',
    }
  },
  subTitle: {
    fontSize: '11px',
    marginBottom: '5px',
    color: theme.palette.text.darkGray,
  },
  dateContent: {
    fontSize: '12px',
    marginBottom: '25px',
    color: theme.palette.text.darkGray,
  }
}))

const Review = ({
  conversionInfo,
  setConversionInfo,
  isLoading,
  setIsLoading,
  setTimer,
  setIsLocked,
  updateRate,
  setUpdateRate,
  matches
}) => {
  const classes = useStyles()
  const toastDispatcher = ToastHook()
  const { t } = useTranslation()
  const today = new Date()
  const availableCurrencies = currencies.filter(currency => currency.symbol)
  useEffect(()=>{
    if(updateRate){
      fetchQuote()
    }
  },[updateRate])
  const fetchQuote = async () => {
    try {
      setIsLoading(true)
      const transferService = new TransferService()
      const response = await transferService.getQuote(conversionInfo.sell_currency, conversionInfo.buy_currency, conversionInfo.settlement_amount)
      if (response.status !== 200) {
        toastDispatcher(getHttpErrorMessage(response))
      }
      setTimer(30)
      const conversion = response.data
      setConversionInfo({
        ...conversion,
        calculate_rate: handleTerms(conversion)
      })
    } catch {
      toastDispatcher("Something went wrong while loading conversion", 'error')
    } finally {
      setUpdateRate(false)
      setIsLoading(false)
      setIsLocked(false)
    }
  }
  return (
    <Grid
      item
      xs={12}
      className={classes.root}
    >
      <Grid 
        container
        direction='row'
        justifyContent='space-between'
      >
        <Grid
          container
          direction='row'
          className={classes.currencyInputContainer}
        >
          <Typography variant='h6' className={classes.title} >{t("Sell")}</Typography>
          <CurrencyInputField
            currency={conversionInfo.sell_currency}
            amount={toCurrencyFormat(conversionInfo.settlement_amount)}
            isLoading={isLoading}
            customizeWidth='100%'
            isDisabled={true}
            isInvertedColoring={true}
            availableCurrencies={availableCurrencies}
            t={t}
          />
        </Grid>
        <Grid
          container
          direction='row'
          className={classes.currencyInputContainer}
        >
          <Typography  variant='h6' className={classes.title}>{t("Buy")}</Typography>
          <CurrencyInputField
            currency={conversionInfo.buy_currency}
            amount={toCurrencyFormat(conversionInfo.converted_amount)}
            isLoading={isLoading}
            customizeWidth='100%'
            isDisabled={true}
            isInvertedColoring={true}
            availableCurrencies={availableCurrencies}
            t={t}
          />
        </Grid>
      </Grid>
      <Grid
        container
        direction='column'
        className={classes.currencyInputContainer}
        style={{width: "100%"}}
      >
        <Typography variant='h6' className={classes.title}>{t("Exchange Rate")}</Typography>
        <Box className={classes.conversionRateArea}>
          <Typography variant='h6' className={classes.conversionRate}><strong>{conversionInfo.calculate_rate.toFixed(2)}</strong> ({t("inverse")} {(1/conversionInfo.calculate_rate).toFixed(2)}) </Typography>
        </Box>
      </Grid>
      <Grid
        container
        direction='column'
        className={classes.currencyInputContainer}
      >
        <Typography className={classes.subTitle}>{t("Settlement Date")}</Typography>
        <Typography variant='h6' className={classes.dateContent}>
          {t('intlDateTime',{
            val: today,
            formatParams: {
              val: {hour: "numeric", minute:"numeric", year: 'numeric', month: 'long', day: 'numeric' },
            },
          })}
        </Typography>
        <Typography className={classes.subTitle}>{t("Conversion Date")}</Typography>
        <Typography variant='h6' className={classes.dateContent}>
          {t('intlDateTime',{
            val: today,
            formatParams: {
              val: {year: 'numeric', month: 'long', day: 'numeric' },
            },
          })}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default Review