import React, { useState, useEffect } from "react"
import { useTranslation } from 'gatsby-plugin-react-i18next'
import { navigate } from 'gatsby'
import { useSelector } from 'react-redux'

import { getHttpErrorMessage } from "../../services/helper"

import { makeStyles } from '@material-ui/core/styles'
import Grid from "@material-ui/core/Grid"
import ToastHook from "../../hooks/toastHook"
import CardContainer from "../../components/CardContainer"
import LoadingPage from "../../components/LoadingPage"
import Modal from "../../components/Modal"
import SetAmount from "./SetAmount"
import Review from "./Review"
import Footer from "./Footer"
import Header from "./Header"
import Confirmation from "./Confirmation"
import { TransferService } from "../../services/TransferService"

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  },
}))
const CurrencyConversionSkeleton = ({location}) => {
  const defaultCurrency = location.state ? location.state.defaultCurrency : null
  const classes = useStyles()
  const { t } = useTranslation()
  const [activeStep, setActiveStep] = useState(0)
  const [timer, setTimer] = useState(30)
  const [conversionInfo, setConversionInfo] = useState(location?.state?.conversionInfo || null)
  const [confirmationInfo, setConfirmationInfo] = useState({})
  const [isLoading, setIsLoading] = useState(false)
  const [isLocked, setIsLocked] = useState(false)
  const [updateRate, setUpdateRate] = useState(false)
  const [getQuote, setGetQuote] = useState(false)
  const [isConfirmed, setIsConfirmed] = useState(false)
  const [openModal, setOpenModal] = useState(false)
  const matches = useSelector(state => state.app.matches)
  const clientBankAccountBalance = useSelector(state => state.clientBankAccounts.accountsBalance)
  const toastDispatcher = ToastHook()

  useEffect(() => {
    let interval = timer.remainingTime
    if (!isLocked && timer > 0) {
      interval = setInterval(() => {
        setTimer(timer -1)
      }, 1000)
    } else if (timer === 0) {
      setIsLocked(true)
      clearInterval(interval)
    }
    return () => clearInterval(interval);
  }, [timer, isLocked])
  
  useEffect(()=>{
    if (isConfirmed){
      sendOutrightForward()
    } else if (confirmationInfo.amount) {
      setOpenModal(true)
    }
  },[isConfirmed, confirmationInfo])

  const sendOutrightForward = async () => {
    try {
      setIsConfirmed(false)
      const data = {
        "buy_currency": conversionInfo.buy_currency,
        "sell_currency": conversionInfo.sell_currency,
        "amount": conversionInfo.settlement_amount,
        "quote_id": conversionInfo.quote_id,
        "is_amount_settlement": true
      }
      const transferService = new TransferService()
      const response = await transferService.createOutrightForward(data)
      if (response.status > 300) {
        toastDispatcher(getHttpErrorMessage(response))
      }
      setConfirmationInfo({
        ...response.data,
        calculate_rate: conversionInfo.calculate_rate,
        converted_amount: conversionInfo.converted_amount
      })
    } catch {
      toastDispatcher("Something went wrong while sending outright forward", 'error')
    } finally {
      setIsLoading(false)
      setIsConfirmed(false)
    }
  }

  const handleCompleteOutrightForward = () => {
    setOpenModal(false)
    navigate("/client/dashboard")
  }

  const renderStepContent = (activeStep) => {
    switch(activeStep) {
      case 0:
        return <SetAmount
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          accounts={clientBankAccountBalance}
          timer={timer}
          setTimer={setTimer}
          isLocked={isLocked}
          setIsLocked={setIsLocked}
          updateRate={updateRate}
          setUpdateRate={setUpdateRate}
          getQuote={getQuote}
          setGetQuote={setGetQuote}
          conversionInfo={conversionInfo}
          setConversionInfo={setConversionInfo}
          defaultCurrency={defaultCurrency}
          matches={matches}
          setActiveStep={setActiveStep}
        />
      case 1:
        return <Review
          conversionInfo={conversionInfo}
          setConversionInfo={setConversionInfo}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          setTimer={setTimer}
          setIsLocked={setIsLocked}
          updateRate={updateRate}
          setUpdateRate={setUpdateRate}
          matches={matches}
        />
      default:
        break
    }
  } 
  return (
    <>
      {
        !clientBankAccountBalance
        ? <LoadingPage/>
        : <Grid container justifyContent="center" alignItems='center'>
            <CardContainer
              header={ <Header 
                activeStep={activeStep}
                timer={timer}
                matches={matches}
                t={t}
              />}
              wrapperPadding={matches ? '0 10%' : '0'}
              contentPadding="0"
              className={classes.root}
            >
              {renderStepContent(activeStep)}
              <Footer
                activeStep={activeStep}
                setActiveStep={setActiveStep}
                isLoading={isLoading}
                setIsLoading={setIsLoading}
                setUpdateRate={setUpdateRate}
                setGetQuote={setGetQuote}
                setIsConfirmed={setIsConfirmed}
                isLocked={isLocked}
                matches={matches}
                t={t}
              />
            </CardContainer>
            <Modal
              openModal={openModal}
              setOpenModal={handleCompleteOutrightForward}
              title='Transaction Completed'
            >
              <Confirmation
                confirmationInfo={confirmationInfo}
              />
            </Modal>  
        </Grid>
      }
    </>
  )
}

export default CurrencyConversionSkeleton
