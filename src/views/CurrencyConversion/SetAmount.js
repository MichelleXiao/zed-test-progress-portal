import React, { useState, useEffect } from "react"
import { makeStyles } from '@material-ui/core/styles'
import { useSelector } from 'react-redux'
import { useTranslation } from 'gatsby-plugin-react-i18next'

import { Grid, Typography } from "@material-ui/core"

import ToastHook from "../../hooks/toastHook"
import { removeCurrencyStyle, toCurrencyFormatWhileInput, getHttpErrorMessage, handleTerms, toDecimalPlaces } from "../../services/helper"
import { TransferService } from "../../services/TransferService"

import CurrencyInputField from "src/components/FormField/CurrencyInput/CurrencyInputField"
import CurrentConversionRate from "../../components/CurrentConversionRate"

import currencies from "../../constants/currencies.json"

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2rem 4rem 1rem',
    backgroundColor: theme.palette.primaryLightGray4,
    borderTop: '1px solid #F1F1F6',
    [theme.breakpoints.down("lg")]: {
      padding: '1rem 1.5rem',
    },
    [theme.breakpoints.down("sm")]: {
      padding: '1rem 1rem',
    }
  },
  currencyInputContainer: {
    marginBottom: '32px',
    width: '48%',
    [theme.breakpoints.down("md")]: {
      width: '100%',
    }
  },
  title: {
    color: theme.palette.text.lightGray2,
    fontSize: '14px',
    marginBottom:'13px'
  },
  countDown: {
    marginTop: '13px',
    fontSize: '11px',
    color: theme.palette.text.lightGray2,
    '& strong' : {
      color: theme.palette.icon.redIcon
    }
  },
  warnMsg: {
    color: theme.palette.icon.redIcon,
    fontSize: '15px',
    marginTop: '5px',
  }
}))
const transferService = new TransferService()
const SetAmount = ({
  isLoading,
  setIsLoading,
  accounts,
  timer,
  setTimer,
  isLocked,
  setIsLocked,
  updateRate,
  setUpdateRate,
  getQuote,
  setGetQuote,
  conversionInfo,
  setConversionInfo,
  defaultCurrency,
  matches,
  setActiveStep
}) => {

  const classes = useStyles()
  const toastDispatcher = ToastHook()
  const validInput = (value) => value.match(/^[0-9.,]*$/)
  const userPreference = useSelector(state => state.user.userPreferences)
  const { t } = useTranslation()
  const availableSellCurrencies = currencies.filter(currency => {
    const matchedCurrency = accounts.find(account => account.currency === currency.name)
    return matchedCurrency
  })
  const availableBuyCurrencies = currencies.filter(currency => currency.symbol)
  const [sellCurrency, setSellCurrency] = useState(initiateSellCurrency(defaultCurrency, conversionInfo, accounts, availableSellCurrencies, userPreference))
  const [buyCurrency, setBuyCurrency] = useState(conversionInfo?.buy_currency || accounts[1].currency)
  const [sellAmount, setSellAmount] = useState(conversionInfo?.settlement_amount ||  null)
  const [buyAmount, setBuyAmount] = useState(conversionInfo?.converted_amount || null)
  const [cursor, setCursor] = useState(0)
  const [isSwapped, setIsSwapped] = useState(false)
  const [warnMsg, setWarnMsg] = useState(null) 
  const [latestChanged, setLatestChanged] = useState("sellCurrency")

  // handle if conversionInfo is passed in, and validate if conversionInfo.sell_currency exists.
  useEffect(() => {
    if(!conversionInfo) return 
    const validSellCurrency = availableSellCurrencies.find(currency => currency.name === conversionInfo?.sell_currency)
    setConversionInfo({
      ...conversionInfo,
      sell_currency: validSellCurrency || defaultCurrency
    })
    !validSellCurrency && toastDispatcher(`You're not holding ${conversionInfo?.sell_currency}`, 'error')
  }, [])

  useEffect(()=>{
    fetchRate()
  },[sellCurrency, buyCurrency, updateRate])

  useEffect(() => {
    if (getQuote && sellCurrency === buyCurrency) {
      setWarnMsg("Warning: Sell curreny and buy currency cannot be same")
    } else if (getQuote && (!sellAmount || sellAmount === 0)) {
      setWarnMsg("Warning: Please enter convert amount")
    } else if (getQuote && Number(sellAmount) < 1) {
      setWarnMsg('Sell amount must be greater than 1')
      return
    }else if(getQuote){
      fetchQuote(sellAmount)
    } 
  }, [getQuote])
  
  const handleIdenticalCurrency = () => {
    setConversionInfo({
      rate: 1,
      inverted_rate: 1,
    })
    setUpdateRate(false)
    setIsLocked(false)
    setTimer(30)
    if(latestChanged === "sellCurrency"){
      setBuyAmount(sellAmount)
    } else{
      setSellAmount(buyAmount)
    }
  }
  const handleAmountIfCurrencyChanges = (conversion) => {
    setWarnMsg()
    setGetQuote(false)
    const calculateRate = handleTerms(conversion)
    if(sellAmount && latestChanged === "sellCurrency"){
      setBuyAmount(
        sellAmount && removeCurrencyStyle(sellAmount) > 0
        ? toCurrencyFormatWhileInput(removeCurrencyStyle(sellAmount)*calculateRate)
        : null
      )
      return
    } 
    buyAmount && setSellAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(buyAmount)/calculateRate))
  }
  
  const handleAmountChange = (e, setErrMsg, action) => {
    setWarnMsg()
    setErrMsg('')
    getQuote && setGetQuote(false)
    if(!e.target.value) {
      setSellAmount(null)
      setBuyAmount(null)
      return
    }
    if(!validInput(e.target.value) || e.target.value[cursor] === ',' || e.target.value.replace(/[^.]/g, "").length > 1){
      setErrMsg('Invalid input')
      return
    } 
    if (e.target.value.length > 11){
      setErrMsg('Number length reached')
      return
    }
    const hasNewComma = e.target.value.length && e.target.value.length % 4 === 0;
    const cursorStart = hasNewComma ? e.target.selectionStart + 1 : e.target.selectionStart;
    setCursor(cursorStart)
    const value = toDecimalPlaces(e.target.value, 2)
    const parsedAmount = toCurrencyFormatWhileInput(value)
    const calculateRate = handleTerms(conversionInfo)
    if(action==='sell'){
      setLatestChanged("sellCurrency")
      setSellAmount(parsedAmount)
      setBuyAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(value)*calculateRate)) 
    } else{
      setLatestChanged("buyCurrency")
      setBuyAmount(parsedAmount)
      setSellAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(value)/calculateRate)) 
    }
  }

  const fetchQuote = async (amount) => {
    try {
      if(sellCurrency === buyCurrency){
        handleIdenticalCurrency()
        return
      } 
      setIsLoading(true)
      const response = await transferService.getQuote(sellCurrency, buyCurrency, amount && removeCurrencyStyle(amount) || '1')
      if (response.status !== 200) {
        toastDispatcher(getHttpErrorMessage(response))
      }
      setTimer(30)
      const conversion = response.data
      setConversionInfo({
        ...conversion,
        calculate_rate: conversionInfo.calculate_rate
      })
      setActiveStep(1)
    } catch {
      toastDispatcher("Something went wrong while getting quote", 'error')
    } finally {
      setUpdateRate(false)
      setIsLocked(false)
      setGetQuote(false)
      setIsLoading(false)
    }
  }

  const fetchRate = async () => {
    try {
      if(sellCurrency === buyCurrency){
        handleIdenticalCurrency()
        return
      } 
      setIsLoading(true)
      const response = await transferService.getRate(sellCurrency, buyCurrency)
      if (response.status !== 200) {
        toastDispatcher(getHttpErrorMessage(response))
      }
      setTimer(30)
      const conversion = response.data
      setConversionInfo({
        ...conversion,
        rate: conversion.rate.toFixed(5),
        inverted_rate: conversion.inverted_rate.toFixed(5),
        calculate_rate: handleTerms(conversion)
      })
      handleAmountIfCurrencyChanges(conversion)
      setIsLoading(false)
    } catch {
      toastDispatcher("Something went wrong while loading conversion rate", 'error')
      setIsLoading(false)
    } finally {
      setUpdateRate(false)
      setIsLocked(false)
      setGetQuote(false)
    }
  }
  
  return (
    <Grid
      item
      xs={12}
      className={classes.root}
    >
      <Grid 
        container
        direction='row'
        justifyContent='space-between'
      >
        <Grid
          container
          direction='row'
          className={classes.currencyInputContainer}
        >
          <Typography variant='h6' className={classes.title} >{t("Sell")}</Typography>
          <CurrencyInputField
            currency={sellCurrency}
            setCurrency={setSellCurrency}
            amount={sellAmount}
            cursor={cursor}
            handleAmountChange={handleAmountChange}
            action='sell'
            isLoading={isLoading}
            availableCurrencies={availableSellCurrencies}
            customizeWidth='100%'
            isInvertedColoring={true}
            matches={matches}
            t={t}
          />
        </Grid>
        <Grid
          container
          direction='row'
          className={classes.currencyInputContainer}
        >
          <Typography  variant='h6' className={classes.title} >{t("Buy")}</Typography>
          <CurrencyInputField
            currency={buyCurrency}
            setCurrency={setBuyCurrency}
            amount={buyAmount}
            cursor={cursor}
            handleAmountChange={handleAmountChange}
            action='buy'
            isLoading={isLoading}
            availableCurrencies={availableBuyCurrencies}
            customizeWidth='100%'
            isInvertedColoring={true}
            matches={matches}
            t={t}
          />
        </Grid>
      </Grid>
      <Grid
        container
        direction='column'
        className={classes.currencyInputContainer}
        style={{width: '100%'}}
      >
        <Typography variant='h6' className={classes.title}>{t("Rate")}</Typography>
        <CurrentConversionRate 
          conversionInfo={conversionInfo}
          isSwapped={isSwapped}
          setIsSwapped={setIsSwapped}
          isLoading={isLoading}
          sellCurrency={sellCurrency}
          buyCurrency={buyCurrency}
          customizedWidth={matches ? '100%' : null}
          isInvertedColoring={true}
          matches={matches}
        />
        { isLocked 
          ?  <Typography className={classes.countDown} style={{marginTop:'13px'}} >{t("Spot rate locked! Click")} <strong>{t("Update Rate")}</strong> {t("to get latest rate")}</Typography>
          : <Typography className={classes.countDown} style={{marginTop:'13px'}} >{t("Spot rate locked in for the next")} <strong>{timer} {t("seconds")}</strong></Typography>
        }
      </Grid>
      {warnMsg && <Typography variant='h6' className={classes.warnMsg}>{warnMsg}</Typography>}
    </Grid>
  )
}

const initiateSellCurrency = (defaultCurrency, conversionInfo, accounts, availableSellCurrencies, userPreference) => {
  if (conversionInfo?.sell_currency) {
    const validSellCurrency = availableSellCurrencies.find(currency => currency.name === conversionInfo?.sell_currency)
    // if no validSellCurrency, execute next lines
    if(validSellCurrency) return conversionInfo.sell_currency
  }
  return defaultCurrency || userPreference?.default_currency || accounts[0].currency
}

export default SetAmount