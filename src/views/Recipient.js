import React from "react"

// Utils
import { useSelector } from "react-redux"

// Components
import { Grid } from '@material-ui/core'
import LoadingPage from "../components/LoadingPage"
import CategoryLayout from "src/components/CategoryLayout"
import CustomizedAccordion from "../components/Accordion"
import ProgressBar from "../components/ProgressBar"

// Serivces
import { checkSenderRecipientStatus } from "src/services/CheckSenderRecipientStatus"
import { percentageOfCompleteness } from "src/services/helper"

// Assets
import { RECIPIENT } from '../constants/testCases'

const Recipient = ({location}) => {
  const recipients = useSelector(state => state.clientBankAccounts.recipients)
  const recipientApiStatuses = recipients?.recipients && checkSenderRecipientStatus(recipients.recipients)
  
  const updateRecipientApis = () => {
    const updatedRecipientApis = RECIPIENT.map(recipientApi => {
      const updatedTestCases = recipientApi.test_cases.map(testCase => {
        return ({
          ...testCase,
          is_completed: recipientApiStatuses[testCase.case_id] || false
        })
      })
      return ({
        ...recipientApi,
        test_cases: updatedTestCases
      })
    })
    return updatedRecipientApis
  }
  
  const updatedRecipientApis = recipientApiStatuses && updateRecipientApis()

  return (
    <>
      {
        !recipientApiStatuses
        ? <LoadingPage/>
        : <CategoryLayout
          headerContentText="Recipient"
        >
          <Grid container direction='column'>
            <ProgressBar progressData={percentageOfCompleteness(updatedRecipientApis)}/>
            {
              updatedRecipientApis.map((apiObj, index) => <CustomizedAccordion 
                  key={apiObj.key}
                  apiObj={apiObj}
                  index={index}
                  expandedApi={location.state.expandedApi}
                />     
              )
            }
          </Grid>
        </CategoryLayout>
      }
    </>
  )
}

export default Recipient
