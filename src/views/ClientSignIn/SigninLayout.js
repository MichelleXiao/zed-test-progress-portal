import React from "react"
import { makeStyles } from '@material-ui/core/styles'

import { Grid } from "@material-ui/core"
import LandingHeader from "src/components/LandingHeader"
import Footer from "src/components/Footer"

import BgImg from "src/images/imgs/background.svg"
import DeskImg from "src/images/imgs/desk.svg"
import PersonImg from "src/images/imgs/person.svg"
import CombinedImg from "src/images/imgs/combined.svg"
import MobileBg from "src/images/imgs/mobileBg.svg"
import TabletBg from "src/images/imgs/tabletImg.svg"

const useStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100% - 60px)',
    background: props => `transparent url(${props.pcBg}) 0% 0% no-repeat padding-box`,
    backgroundSize: '80% 80% !important',
    backgroundPosition: 'center !important',
    [theme.breakpoints.down("md")]: {
      background: props => `transparent url(${props.tabletBg}) 0% 0% no-repeat padding-box`,
      backgroundSize: '100% 80% !important',
    },
    [theme.breakpoints.down("sm")]: {
      background: props => `transparent url(${props.mobileBg}) 0% 0% no-repeat padding-box`,
      backgroundSize: '100% 80% !important',
    },
  },
  container: {
    height: 'calc(100vh - 60px)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 180px',
    [theme.breakpoints.down("sm")]: {
      height: 'unset'
    },
  },
  mobileImgContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    '& img': {
      display: 'none',
    },
    [theme.breakpoints.down("sm")]: {
      '& img': {
        display: 'unset',
        margin: 'auto',
        marginBottom: 0,
        alignSelf: 'end',
      },
    },
  },
  pcImg: {
    marginBottom: 0,
    alignSelf: 'end',
    width: '18%',
    [theme.breakpoints.down("lg")]: {
      width: '15%',
    },
    [theme.breakpoints.down("md")]: {
      width: '13%',
    },
    [theme.breakpoints.down("sm")]: {
      display: 'none'
    },
  }
}));


const SigninLayout = ({children}) => {
  const props = {
    pcBg: BgImg,
    tabletBg: TabletBg,
    mobileBg: MobileBg
  }
  const classes = useStyles(props)
  return (
    <>
      <LandingHeader/>
      <Grid
        container
        direction='row'
        justifyContent='center'
        className={classes.root}
      >
        <img
          src={DeskImg}
          alt={DeskImg}
          className={classes.pcImg}
        />
        {children}
        <img
          src={PersonImg}
          alt={PersonImg}
          className={classes.pcImg}
        />
        <Grid
          className={classes.mobileImgContainer}
          item
          xs={12}
        >
          <img
            src={CombinedImg}
            alt={CombinedImg}
            className={classes.mobileImg}
          />
        </Grid>
      </Grid>
      <Footer/>
    </>
  )
}

export default SigninLayout