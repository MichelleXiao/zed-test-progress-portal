import React, { useState } from 'react'

/** Components */
import { Formik } from 'formik';
import { Container, Grid, Typography } from '@material-ui/core';
import CardContainer from 'src/components/CardContainer';
import TextInputField from 'src/components/FormField/TextInputField'
import LoginButton from './LoginComponents/LoginButton';

/** Utils */
import { makeStyles } from '@material-ui/core/styles';
import { navigate } from 'gatsby';
import * as Yup from 'yup';

/** Services */
import { AuthenticationService } from 'src/services/zed-api'
import { yupDebounce } from 'src/services/helper'


const authService = new AuthenticationService()

const useStyles = makeStyles((theme) => ({
  container: {
    height: 'calc(100vh - 60px)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 9rem',
    width: '42%',
    margin: 0,
    [theme.breakpoints.down("lg")]: {
      width: '44%',
      padding: '0 5rem',
    },
    [theme.breakpoints.down("md")]: {
      width: '54%',
      padding: '0 2rem',
    },
    [theme.breakpoints.down("sm")]: {
      width: '94%',
      padding: '0 0.5rem',
      height: 'unset',
      margin: '80px 0 12px 0'
    }
  },
  paper: {
    padding: '0.5rem 2.5rem',
    '& h6': {
      color: theme.palette.text.darkGray,
      fontSize: 32
    },
    [theme.breakpoints.down("md")]: {
      padding: '0 2rem',
    },
    [theme.breakpoints.down("sm")]: {
      padding: '0 0.5rem',
    }
  },
  termsAndCondition: {
    fontSize: 14,
    color: theme.palette.text.primaryGray,
    textAlign: 'center',
    marginBottom: 20,
    '& a': {
      color: theme.palette.button.primaryBlue,
      textDecoration: 'underline',
      cursor: 'pointer',
    }
  },
  logo: {
    height: 40,
    width: 40,
    margin: '0.5rem',
    cursor: 'pointer',
  },
  otherLoginOptions: {
    padding: '1rem',
  },
  errMsg: {
    marginTop: '-15px',
    marginBottom: '10px',
    color: 'red',
    fontSize: 14
  }
}));

const FormFields = {
  EMAIL: 'email',
  PASSWORD: 'password',
  TERM_AND_CONDITION: 'terms_and_condition',
}

const TERMS_URL = "https://zed-client-documents.s3.amazonaws.com/web-portal-terms-conditions/ZED+Client+Terms+of+Service.pdf"
/**
 * Component Form containing an email and password field
 * currently used for Login and Signup functionality
 * Pass onNext function to act as SignupForm otherwise pass a prop of onLogin to act as Login
 *
 */
export default ({ onNext, onLogin, loading, swipeContainer, errMsg, setErrMsg }) => {
  const classes = useStyles()
  const formTitle = onNext ? 'Create Account' : 'Account Profile'
  const [openModal, setOpenModal] = useState(false)
  swipeContainer && swipeContainer.updateHeight()
  
  return (
    <Formik
      initialValues={{ email: '', password: '', terms_and_condition: true, account_entity_type: 'Corporate Client' }}
      validationSchema={validationSchema(!!onNext)}
      onSubmit={values => {
        onNext ? onNext(values, 'account') : onLogin(values)
      }}
    >
      {({ errors, touched, values, handleChange, handleSubmit, setFieldValue, setFieldTouched }) => {
        return (
          <Container className={classes.container}>
            <CardContainer
              title={formTitle}
              className={classes.paper}
            >
              <Grid container>
                <Grid item xs={12}>
                  <TextInputField
                    variant="standard"
                    fullWidth
                    name={FormFields.EMAIL}
                    value={values[FormFields.EMAIL]}
                    onChange={e => {
                      setFieldValue(FormFields.EMAIL, e.target.value)
                      errMsg && setErrMsg()
                    }}
                    isError={Boolean(errors[FormFields.EMAIL])}
                    errMsg={errors[FormFields.EMAIL]}
                    label='Email'
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextInputField
                    variant="standard"
                    fullWidth
                    name={FormFields.PASSWORD}
                    value={values[FormFields.PASSWORD]}
                    onChange={e => {
                      setFieldTouched(FormFields.PASSWORD, true)
                      setFieldValue(FormFields.PASSWORD, e.target.value)
                      errMsg && setErrMsg()
                    }}
                    label='Password'
                    isError={!onNext && Boolean(errors[FormFields.PASSWORD])}
                    errMsg={!onNext && errors[FormFields.PASSWORD]}
                  />
                </Grid>
                <Typography className={classes.errMsg}>{errMsg}</Typography>
                <Grid item xs={12}>
                  <LoginButton onLogin={() => handleSubmit(values)} loading={loading} />
                </Grid>
              </Grid>
            </CardContainer>
          </Container>

        )
      }}
    </Formik>
  )
}
const validationSchema = (isSignUp) => Yup.object().shape({
  email: Yup.string()
    .email("Invalid Email Format")
    .required("Email is required")
    .test(
      'is-email-taken',
      'Email is already taken',
      yupDebounce(
        async (value) => {
          try {
            if (!isSignUp) return true
            const response = await authService.checkEmailAvailability(value)
            return response.data.message === 'Email is still available'
          } catch (error) {
            return true
          }
        },
        500
      )
    ),
  password: Yup.string()
    .required("Password is required")
    .min(8, "Password must be from 8 to 32 characters")
    .max(32, "Password must be from 8 to 32 characters")
    .matches(
      /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W))/,
      "Contains 1 lowercase, 1 uppercase and 1 special character"
    ),
  terms_and_condition: Yup.bool().oneOf(
      [true],
      "Terms and Conditions must be accepted"
    ),
})
