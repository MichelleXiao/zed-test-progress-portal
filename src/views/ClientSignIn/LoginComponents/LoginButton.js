import React from 'react'

import Button from 'src/components/Button';

const LoginButton = ({ onLogin, loading }) => 
  <Button 
    type="button" 
    textTransform="capitalize" 
    isLoading={loading} 
    onClick={onLogin} 
    style={{
      marginBottom: '20px',
      height: '60px'
    }}
  >
    Login
  </Button>

export default LoginButton