import React from "react"
import PropTypes from "prop-types"
import Skeleton from "react-loading-skeleton"
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  table: {
    marginBottom: '0 !important'
  },
}))

const RecipientListSkeleton = ({
  children,
  coloumnNum,
  rowNum
}) => 
{
  RecipientListSkeleton.propTypes = {
    children: PropTypes.node.isRequired,
    coloumnNum: PropTypes.number.isRequired,
    rowNum: PropTypes.number.isRequired,
  }
  
  const classes = useStyles()
  const SkeletonList = () => {
    return [...Array(rowNum).keys()].map(i => <Skeleton key={i} height="25" />)
  }
  const SkeletonColumn = () => {
    return [...Array(coloumnNum).keys()].map(i => <td key={i}> <SkeletonList /> </td>)
  }

  return (
    <table className={classes.table}>
        {children}
      <tbody>
        <tr>
          <SkeletonColumn />
        </tr>
      </tbody>
    </table>
  )
}

export default RecipientListSkeleton