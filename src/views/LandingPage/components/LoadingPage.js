import React from "react"

import { Typography, CircularProgress, Box } from "@material-ui/core"

const LoadingPage = () => <Box
  >
    <CircularProgress
      style={{
        display: 'flex',
        alignItems: 'center',
        marginLeft: '45%',
        color: '#3273AD',
        width: 26,
      }}
    />  
    <Typography
      style={{
        marginTop: '1rem',
        textAlign: 'center',
        fontSize: '16px',
        color: '#3273AD',
        fontWeight: 600
      }}
    >
      Loading...
    </Typography>
  </Box>

export default LoadingPage