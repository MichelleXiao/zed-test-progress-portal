import React from "react"
import classnames from "classnames"
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

//  Components
import { MenuItem, Select } from "@material-ui/core"

// Assets
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import EUSVG from 'src/images/imgs/eu.svg'

const useStyles = makeStyles((theme) => ({
  currencyField: {
    height: '100%',
    fontWeight: 600,
    fontSize: '14px',
    width: 102,
    '& .MuiSelect-select.MuiSelect-select': {
      color: theme.palette.text.darkGray,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '& img':{
        margin: 0,
        marginRight: 10
      }
    },
    '& .MuiSelect-select:focus': {
      backgroundColor: 'unset',
    },
    '& .MuiSelect-selectMenu': {
      fontSize: '14px',
    },
    '& svg': {
      fontSize: '1rem',
      marginTop: '-2px',
      color: '#BBBBBB',
      transform: 'rotate(-90deg)'
    },
    '& .MuiSvgIcon-root.MuiSelect-icon.MuiSelect-iconOpen': {
      transform: 'rotate(90deg)',
      marginTop: '6px',
    }
  },
  dropdownStyle: {
    borderRadius: "5px",
    maxHeight: '200px',
    width: '122px',
    marginTop: "15px",
    marginLeft: '5px',
    '& li': {
      fontSize: '14px',
      color: theme.palette.text.darkGray,
      fontWeight: theme.typography.fontWeightLarge
    },
  },
  menuItem: {
    display: 'flex',
    paddingRight: '40px',
    '& img': {
      marginBottom: 0,
      marginRight: 10
    }
  }
}))
const SelectCurrency = ({
  currency, 
  setCurrency,
  currencies
}) => {
  const classes = useStyles()
  const currencyFlag = (code) => 
    code === 'EU' 
    ? EUSVG
    : `http://purecatamphetamine.github.io/country-flag-icons/1x1/${code}.svg`
  
  return (
    <Select
      value={currency}
      onChange={(e)=>{setCurrency(e.target.value)}}
      disableUnderline
      className={classnames(classes.currencyField)}
      IconComponent = {ArrowBackIosIcon}
      MenuProps={{ 
        classes: { paper: classes.dropdownStyle },
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "center"
        }, 
        transformOrigin: {
          vertical: "top",
          horizontal: "center"
        },
        getContentAnchorEl: null
      }}
    >
      {currencies.map((option) => {
        return (
          <MenuItem key={option.name} value={option.name} className={classes.menuItem}>
            <img
              alt={option.code}
              src={currencyFlag(option.code)}
              style={{
                borderRadius: '50%',
                height: 28,
                width: 28
              }}
            />
            {option.name}
          </MenuItem>
        )
      })}
    </Select>
  )
}

SelectCurrency.propTypes = {
  currency: PropTypes.string,
  setCurrency: PropTypes.func,
  currencies: PropTypes.array,
}

export default SelectCurrency
