import React, {useState, useEffect} from "react"
import { navigate } from 'gatsby'
import { useDispatch, useSelector } from "react-redux"
import { makeStyles } from '@material-ui/core/styles'

import { Grid,  Typography } from "@material-ui/core"
import LoadingPage from "./LoadingPage"
import CurrencyInputField from "./CurrencyInputField"
import Swap from "./Swap"
import Button from "src/components/Button"

import { removeCurrencyStyle, toCurrencyFormatWhileInput, getHttpErrorMessage, handleTerms, transformDatePresentation, transformDate } from "src/services/helper"
import { TransferService } from "src/services/TransferService"
import ToastHook from "src/hooks/toastHook"
const transferService = new TransferService()

const useStyles = makeStyles((theme) => ({
  subLine: {
    width: '100%',
    color: theme.palette.text.darkGray,
    fontSize: 16,
    margin: '15px 0',
    [theme.breakpoints.down("md")]: {
      textAlign: 'start',
      fontSize: 13,
    },
  },
  warnMsg: {
    color: theme.palette.primaryRed,
    fontWeight: theme.typography.fontWeightLarge,
    textAlign: 'end',
    fontSize: 14,
    marginTop: 3,
    [theme.breakpoints.down("md")]: {
      fontSize: 12,
      marginBottom: 0
    }
  }
}))

const CurrencyConversion = ({initToken}) => {
  const classes = useStyles()
  const toastDispatcher = ToastHook()
  const validInput = (value) => value.match(/^[0-9.,]*$/)

  const [conversionInfo, setConversionInfo] = useState()
  const [sellCurrency, setSellCurrency] = useState("USD")
  const [buyCurrency, setBuyCurrency] = useState("EUR")
  const [sellAmount, setSellAmount] = useState(null)
  const [buyAmount, setBuyAmount] = useState(null)
  const [cursor, setCursor] = useState(0)
  const [isSwapped, setIsSwapped] = useState(false)
  const [latestChanged, setLatestChanged] = useState("sellCurrency")
  const [isLoading, setIsLoading] = useState(false)
  const [warnMsg, setWarnMsg] = useState()

  useEffect(()=>{
    initToken && getRate()
  },[sellCurrency, buyCurrency, initToken])

  const getRate = async () => {
    try {
      if(sellCurrency === buyCurrency){
        handleIdenticalCurrency()
        return
      } 

      setIsLoading(true)

      const response = await transferService.getRate(sellCurrency, buyCurrency, initToken)

      if (response.status !== 200) {
        toastDispatcher(getHttpErrorMessage(response))
        setConversionInfo({
          rate: null,
          inverted_rate: null,
          calculate_rate: null
        })
      }

      const conversion = response.data
      
      setConversionInfo({
        ...conversion,
        rate: conversion.rate.toFixed(5),
        inverted_rate: conversion.inverted_rate.toFixed(5),
        calculate_rate: handleTerms(conversion)
      })

      handleAmountIfCurrencyChanges(conversion)

    } catch {
      toastDispatcher("Something went wrong while loading conversion rate", 'error')

      setConversionInfo({
        rate: null,
        inverted_rate: null,
        calculate_rate: null
      })

    } finally {
      setIsLoading(false)
    }
  }

  const handleIdenticalCurrency = () => {
    setConversionInfo({
      rate: 1,
      inverted_rate: 1,
      calculate_rate: 1
    })

    latestChanged === "sellCurrency" ? setBuyAmount(sellAmount) : setSellAmount(buyAmount)
  }

  const handleAmountIfCurrencyChanges = (conversion) => {
    const calculateRate = handleTerms(conversion)

    if(latestChanged === "sellCurrency"){
      setBuyAmount(
        sellAmount && removeCurrencyStyle(sellAmount) > 0
        ? toCurrencyFormatWhileInput(removeCurrencyStyle(sellAmount)*calculateRate)
        : null
      )
      return
    } 

    setSellAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(buyAmount)/calculateRate))
  }

  const handleAmountChange = (e, setErrMsg, action) => {
    setErrMsg()
    setWarnMsg()

    if(!validateInputAmount(e, setErrMsg)) return

    setErrMsg('')
    const hasNewComma = e.target.value.length && e.target.value.length % 4 === 0;
    const cursorStart = hasNewComma ? e.target.selectionStart + 1 : e.target.selectionStart;
    setCursor(cursorStart)
    const parsedAmount = toCurrencyFormatWhileInput(e.target.value)
    const calculateRate = handleTerms(conversionInfo)
    if(action==='sell'){
      setLatestChanged("sellCurrency")
      setSellAmount(parsedAmount)
      setBuyAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(e.target.value)*calculateRate)) 
    } else{
      setLatestChanged("buyCurrency")
      setBuyAmount(parsedAmount)
      setSellAmount(toCurrencyFormatWhileInput(removeCurrencyStyle(e.target.value)/calculateRate)) 
    }
  }

  const estimateDeliveryDate = () => {
    const deliveryDate = new Date()
    return transformDatePresentation(transformDate(deliveryDate))
  }

  const handleConvertNowBtn = () => {
    if(!sellAmount || !buyAmount) {
      setWarnMsg("Please input conversion amount")
      return 
    }
    navigate(
      `/client/currency-conversion`,
      {
        state: { 
          conversionInfo: {
            ...conversionInfo,
            settlement_amount: sellAmount,
            sell_currency: sellCurrency,
            buy_currency: buyCurrency,
            converted_amount: buyAmount
          }
        },
      }
    )
  }

  const validateInputAmount = (e, setErrMsg) => {
    if(!e.target.value) {
      setSellAmount(null)
      setBuyAmount(null)
      return false
    }
    if(!validInput(e.target.value) || e.target.value[cursor] === ',' || e.target.value.replace(/[^.]/g, "").length > 1 || e.target.value.charAt(0) === "."){
      setErrMsg('Invalid input')
      return false
    } 
    if (e.target.value.length > 11){
      setErrMsg('Number length reached')
      return false
    }
    if (removeCurrencyStyle(e.target.value) < 1) {
      setErrMsg('Sell amount must be greater than 1')
      return false
    }
    return true
  }

  if (!conversionInfo || isLoading || !initToken) return <LoadingPage/>

  return (
    <Grid 
      container
      direction='row'
      justifyContent='space-between'
      alignItems='flex-start'
      className={classes.root}
    >
      <Typography className={classes.subLine}>
        Current Market Rate:      
        <strong>
        {
          ` 1
          ${isSwapped ? buyCurrency : sellCurrency} 
          = 
          ${conversionInfo?.calculate_rate ? (isSwapped ? (1/conversionInfo?.calculate_rate)?.toFixed(5) : conversionInfo?.calculate_rate): "---"}
          ${isSwapped ? sellCurrency : buyCurrency}`
        }
        </strong>
      </Typography>
      <CurrencyInputField
        currency={isSwapped ? buyCurrency : sellCurrency}
        setCurrency={isSwapped ? setBuyCurrency : setSellCurrency}
        amount={isSwapped ? buyAmount : sellAmount}
        cursor={cursor}
        handleAmountChange={handleAmountChange}
        action={isSwapped ? 'buy' : 'sell'}
        isLoading={isLoading}
        isClearable={true}
        subTitle="You send"
      />
      <Swap
        isSwapped={isSwapped}
        setIsSwapped={setIsSwapped}
      />
      <CurrencyInputField
        currency={isSwapped ? sellCurrency : buyCurrency}
        setCurrency={isSwapped ? setSellCurrency : setBuyCurrency}
        amount={isSwapped ? sellAmount : buyAmount}
        cursor={cursor}
        handleAmountChange={handleAmountChange}
        action={isSwapped ? 'sell' : 'buy'}
        isLoading={isLoading}
        isClearable={true}
        subTitle="Recipient gets"
      />
      <Typography className={classes.subLine}>Should arrive by: <strong>{estimateDeliveryDate()}</strong></Typography>
      <Button 
        style={{width: '100%'}}
        onClick={() => handleConvertNowBtn()}
      >
        Convert Now
      </Button>
      { warnMsg && <Typography className={classes.warnMsg}> {warnMsg} </Typography> }
    </Grid>
  )
}

export default CurrencyConversion