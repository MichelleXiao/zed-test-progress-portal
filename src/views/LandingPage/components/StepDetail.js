import React from "react"
import { makeStyles } from '@material-ui/core/styles'

import { Grid,  Typography } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    background: props => `transparent url(${props?.stepInfo?.step}) 0% 0% no-repeat padding-box`,
    paddingLeft: 80,
    paddingRight: 50,
    marginTop: 40,
    height:236,
  },
  title: {
    textAlign: 'center',
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 20,
    marginBottom: 8,
  },
  desc: {
    textAlign: 'center',
    fontSize: 16
  },
  img: {
    width: 49, 
    height: 49, 
    marginBottom: 15
  }
}))

const StepDetail = ({stepInfo}) => {
  const classes = useStyles({ stepInfo: stepInfo })
  return (
    <Grid 
      container
      direction='row'
      justifyContent='space-between'
      alignItems='center'
      className={classes.root}
      item
      lg={3}
      sm={6}
      xs={12}
      style={{
        backgroundSize: '280px 236px',
      }}
    >
      <Grid
        container
        direction='column'
        justifyContent='space-between'
        alignItems='center'
      >
        <img src={stepInfo.image} className={classes.img}/>
        <Typography className={classes.title}> {stepInfo.title} </Typography>
        <Typography className={classes.desc}> {stepInfo.desc} </Typography>

      </Grid>
    </Grid>
  )
}

export default StepDetail