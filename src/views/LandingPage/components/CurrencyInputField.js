import React, { useState, useEffect, useRef } from "react"
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'
import { Grid, TextField, Typography, SvgIcon, Box } from "@material-ui/core"
import SelectCurrency from "./SelectCurrency"

import ClearIcon from '@material-ui/icons/Clear';
import { removeCurrencyStyle } from "src/services/helper"
import currencies from "src/constants/currencies.json"
const useStyles = makeStyles((theme) => ({
  inputField: {
    height: 60,
    border: theme.overrides.greyBorder,
    borderRadius: '5px',
    width: '100%',
    padding: '7px 20px',
    justifyContent:"space-between",
    backgroundColor:theme.palette.primaryWhite,
  },
  currencyField: {
    padding: '0 10px',
    border:'1px solid #E9E9FF',
    borderTop: 'none',
    borderBottom: 'none',
    borderRight: 'none',
    borderRadius: '0 8px 8px 0',
    height: '100%',
    fontWeight: 600,
    fontSize: '14px',
    '& .MuiSelect-select.MuiSelect-select': {
      paddingRight: '10px',
      color: theme.palette.text.darkGray,
    },
    '& .MuiSelect-select:focus': {
      backgroundColor: 'unset',
    },
    '& .MuiSelect-selectMenu': {
      fontSize: '14px'
    },
    '& svg': {
      fontSize: '1rem',
      marginTop: '3px',
      color: theme.palette.text.primary,
    },
    '& .Mui-disabled.MuiSelect-icon.MuiSvgIcon-root': {
      display: 'none'
    },
  },
  amountField: {
    height: 16,
    width: '90%',
    display:'flex',
    justifyContent: 'center',
    fontSize: 16,
    color: theme.palette.text.darkGray,
    '& input':{
      padding: 0,
      color: theme.palette.text.darkGray
    },
    '& .Mui-disabled': {
      color: theme.palette.text.primary,
    },
  },
  errMsg: {
    color: theme.palette.primaryRed,
    fontWeight: theme.typography.fontWeightLarge,
    textAlign: 'end',
    fontSize: 14,
    marginTop: 3,
    [theme.breakpoints.down("md")]: {
      fontSize: 12,
      marginBottom: 0
    }
  },
  clearIcon: {
    width: '10%',
    display:'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    marginRight: 15,
    '& svg': {
      marginTop: '3px',
      width: '1.2rem',
      cursor: 'pointer',
      '&:hover': {
        color: theme.palette.icon.redIcon
      }
    }
  },
  inputSub: {
    fontSize: 12,
    color: theme.palette.text.darkGray,
    fontWeight: theme.typography.fontWeightLarge
  }
}))
const CurrencyInputField = ({
  currency, 
  setCurrency,
  amount, 
  cursor, 
  handleAmountChange, 
  action, 
  isLoading, 
  subTitle,
}) => {
  const [errMsg, setErrMsg] = useState("")
  const inputRef = useRef()
  const classes = useStyles()

  const availableCurrencies = currencies.filter( currency => currency.code )
  
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.selectionStart = cursor
      inputRef.current.selectionEnd = cursor
    }
  }, [amount])

  const handleClear = (e) => {
    e.target.value = null
    handleAmountChange(e, setErrMsg, action)
  }
  return (
    <Grid
      container
      direction="row"
      item
      xs={12}
    >
      <Grid
        container
        alignItems='center'
        className={classes.inputField}
      >
        <Grid
          container
          item
          xs={5}
          md={7}
          style={{height: '100%'}}
        >
          <Typography className={classes.inputSub}>{subTitle}</Typography>
          <TextField
            inputRef={inputRef}
            placeholder="Enter amount"
            InputProps={{ 
              disableUnderline: true,
              style:{fontSize: '15px', fontWeight:'600'},
            }}
            className={classes.amountField}
            value={amount || ""}
            onChange={(e) => handleAmountChange(e, setErrMsg, action)}
            step="0.01"
            disabled={isLoading}
          />

        </Grid>


        <Grid 
          container
          direction='row'
          justifyContent='flex-end'
          item 
          xs={7} 
          md={5}
          style={{height: '100%'}}
        >
          <Box className={classes.clearIcon}>
            { amount && removeCurrencyStyle(amount) > 0
              && <SvgIcon 
                component={ClearIcon} 
                viewBox='0 0 28 28' 
                onClick={(e) => handleClear(e)} 
              />
            }
          </Box>
          <SelectCurrency
            currency={currency}
            setCurrency={setCurrency}
            currencies={availableCurrencies}
          />
        </Grid>

      </Grid>
      { errMsg && <Typography variant='h6' className={classes.errMsg}>{errMsg}</Typography>}
    </Grid>
  )
}

CurrencyInputField.propTypes = {
  currency: PropTypes.string,
  setCurrency: PropTypes.func,
  amount: PropTypes.string,
  cursor: PropTypes.number,
  handleAmountChange: PropTypes.func,
  action: PropTypes.string,
  isLoading: PropTypes.bool,
}

export default CurrencyInputField
