import React from "react"
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'

import { Grid,  Typography } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 40,
    paddingRight: 49,
    [theme.breakpoints.down("lg")]: {
      paddingRight: '40px',
      alignItems:'center'
    },
    [theme.breakpoints.down("md")]: {
      paddingRight: '20px',
    },
    [theme.breakpoints.down("sm")]: {
      paddingRight: 0,
    },
  },
  title: {
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 20,
    marginBottom: 8,
    color: theme.palette.primaryWhite
  },
  desc: {
    fontSize: 16,
    color: theme.palette.primaryWhite
  },
  img: {
    height: 55
  }
  
}))

const StrengthDetail = ({strengthInfo}) => {
  const classes = useStyles()
  return (
    <Grid 
      container
      direction='row'
      justifyContent='space-between'
      alignItems='flex-start'
      className={classes.root}
      item
      md={6}
      xs={12}

    >
      <img
        src={strengthInfo.image}
        className={classes.img}
      />
      <Grid
        item
        lg={8}
        sm={10}
        xs={9}
      >
        <Typography className={classes.title}>{strengthInfo.title}</Typography>
        <Typography className={classes.desc}>{strengthInfo.desc}</Typography>
      </Grid>
    </Grid>
  )
}

StrengthDetail.propTypes  = {
  strengthInfo: PropTypes.object
}

export default StrengthDetail