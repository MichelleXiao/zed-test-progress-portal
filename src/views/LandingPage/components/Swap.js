import React from "react"
import PropTypes from 'prop-types'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import { Grid, Typography } from "@material-ui/core"
import SwapIcon from "src/images/imgs/swap.svg"
const useStyles = makeStyles((theme) => createStyles({
  transferFeeContainer: {
    width: 'calc(100% -32px)',
    borderLeft: '2px dashed #BBBBBB',
    marginLeft: '-16px',
    padding:'20px 35px',
  },
  transferFeeMsg: {
    fontSize: 14,
    color: theme.palette.text.darkGray,
    [theme.breakpoints.down("md")]: {
      fontSize: 12,
    },
  },
  swapIcon: {
    margin: 0,
    cursor: 'pointer',
    '&:active': {
      animation: `${'$rotation'} 150ms`
    }
  },
  '@keyframes rotation': {
    from: { 'transform': 'rotate(0deg)' },
    to: { 'transform': 'rotate(180deg)' }
  }
}))
const Swap = ({
  transferFee,
  isSwapped,
  setIsSwapped
}) => {
  const classes = useStyles()

  return (
    <Grid
      container
      direction="row"
      item
      xs={12}
    >
      <Grid
        container
        alignItems='center'
        item
        style={{width: 32, zIndex: 9}}
      >
        <img
          src={SwapIcon}
          className={classes.swapIcon}
          onClick={()=>setIsSwapped(!isSwapped)}
        />
      </Grid>
      <Grid
        className={classes.transferFeeContainer}
        item
      >
        <Typography className={classes.transferFeeMsg}>
          <strong>1.60 USD </strong>
          Wire transfer fee
        </Typography>
        <Typography className={classes.transferFeeMsg}>
          <strong>4.79 USD </strong>
          Our fee
        </Typography>
      </Grid>
    </Grid>
  )
}

export default Swap
