import React from "react"
import { navigate } from "gatsby"

import { makeStyles } from '@material-ui/core/styles'

import { Grid,  Typography } from "@material-ui/core"
import StepDetail from "./components/StepDetail"
import Button from "src/components/Button"

import AccountSVG from "src/images/imgs/account.svg"
import RegisterSVG from "src/images/imgs/register.svg"
import ShareSVG from "src/images/imgs/share.svg"
import VerifySVG from "src/images/imgs/verify.svg"
import Step1 from "src/images/imgs/steps-1.gif"
import Step2 from "src/images/imgs/steps-2.gif"
import Step3 from "src/images/imgs/steps-3.gif"
import Step4 from "src/images/imgs/steps-4.gif"

const STEPS = [
  {
    image: RegisterSVG,
    title: 'Register in Minutes',
    desc: 'Sign up online, or in our app with an email address, or a Google, Facebook or Apple account.',
    step: Step1
  },
  {
    image: VerifySVG,
    title: 'Verify your identity',
    desc: 'All you need to receive money or order a card is your government ID',
    step: Step2
  },
  {
    image: AccountSVG,
    title: 'Top up your account',
    desc: 'You can add money using your bank account, Apple Pay, credit/debit card.',
    step: Step3
  },
  {
    image: ShareSVG,
    title: 'Share your account',
    desc: 'Share your account details to get paid, or spend around the world with your debit card.',
    step: Step4
  },
]
const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: 563,
    backgroundColor: theme.palette.primaryWhite,
    padding: '80px 20%',
    [theme.breakpoints.down("lg")]: {
      padding: '60px 0 0 0',
    },
  },
  title: {
    color: theme.palette.text.darkGray,
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 32,
    marginBottom: 20,
    [theme.breakpoints.down("lg")]: {
      padding: '0 40px',
    },
    [theme.breakpoints.down("sm")]: {
      padding: '0 20px',
      fontSize: 28,
    }
  },
  blueTitle: {
    color: theme.palette.button.primaryBlue,
    fontSize: 16,
  },
}))

const GetStarted = () => {
  const classes = useStyles()
  return (
    <Grid 
      container
      direction='column'
      justifyContent='center'
      alignItems='center'
      className={classes.root}
    >
      <Typography className={classes.blueTitle}>
        Getting Started
      </Typography>
      <Typography className={classes.title}>
        Get set up and start spending with your card in minutes
      </Typography>
      <Button
        style={{width: 300}}
        onClick={() => navigate('/signup')}
      >
        Get Started Now
      </Button>
      <Grid
        container
        direction='row'
      >
        {
          STEPS.map(stepInfo => 
            <StepDetail
              stepInfo={stepInfo}
              key={stepInfo.title}
            />
            )
        }
      </Grid>
    </Grid>
  )
}


export default GetStarted