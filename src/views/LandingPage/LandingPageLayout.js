import React from "react"

import Intro from "./Intro"
import GetStarted from "./GetStarted"
import WhyZed from "./WhyZed"
import LandingHeader from "src/components/LandingHeader"
import Footer from "src/components/Footer"

const LandingPageLayout = () => {
  return  (
    <>
      <LandingHeader/>
      <Intro/>
      <GetStarted/>
      <WhyZed/>
      <Footer/>
    </>
  )
}

export default LandingPageLayout