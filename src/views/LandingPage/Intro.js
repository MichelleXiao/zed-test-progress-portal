import React, { useEffect, useState } from "react"
import { useTranslation, useI18next } from "gatsby-plugin-react-i18next"

import { makeStyles } from '@material-ui/core/styles'
import { AuthenticationService } from "src/services/zed-api"
import { getCookie } from "src/services/Cookies"

import { Grid,  Typography } from "@material-ui/core"
import CurrencyConversion from "./components/CurrencyConversion"

import IntroGIF from "../../images/imgs/intro.gif"

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: 796,
    background: 'transparent radial-gradient(closest-side at 50% 0%, #F7F9FE 0%, #F1F4FC 100%) 0% 0% no-repeat padding-box',
    padding: '66px 20% 0 20%',
    [theme.breakpoints.down("lg")]: {
      padding: '66px 40px 0 40px',
      minHeight: 656,
    },
    [theme.breakpoints.down("sm")]: {
      padding: '66px 20px 0 20px',
    }
  },
  leftPanel: {
    flexDirection: 'column',
    [theme.breakpoints.down("lg")]: {
      flexDirection: 'row',
      paddingTop: 100,
      marginBottom: 30,
      '& div':{
        width: '50%'
      },
      '& img':{
        width: '50%',
      },
    },
    [theme.breakpoints.down("sm")]: {
      flexDirection: 'column',
      paddingTop: 40,
      marginBottom: 20,
      '& div':{
        width: '100%'
      },
      '& img':{
        display: 'none'
      },
    }
  },
  rightPanel: {
    height: 730,
    alignItems: 'center',
    '&>img':{
      display: 'none'
    },
    [theme.breakpoints.down("lg")]: {
      height: 450,
      marginBottom: 80,
    },
    [theme.breakpoints.down("sm")]: {
      height: 'unset',
      marginBottom: 0,
      '&>img':{
        display: 'unset'
      },
    }
  },
  title: {
    color: theme.palette.text.darkGray,
    fontWeight: theme.typography.fontWeightLarge,
    fontSize: 32,
    marginBottom: 20,
    [theme.breakpoints.down("sm")]: {
      fontSize: 28,
    }
  },
  desc: {
    color: theme.palette.text.darkGray,
    fontSize: 16,
  },
  conversionContainer: {
    padding: 60,
    backgroundColor: theme.palette.primaryWhite,
    boxShadow: '0px 0px 17px #0000001A',
    borderRadius: 10,
    height: 470,
    [theme.breakpoints.down("sm")]: {
      padding: 20,
      minHeight: 357,
      marginBottom: 20,
      height: 'unset'
    }
  },
  introGif: {
    margin: '40px 0 0 0',
    [theme.breakpoints.down("sm")]: {
      margin: '20px 0 0 0'
    }
  }
}))

const Intro = () => {
  const classes = useStyles()

  const [initToken, setInitToken] = useState()
  const { t } = useTranslation()
  const {language, changeLanguage} = useI18next()
  
  useEffect(()=>{
    !initToken && getInitToken()
  },[])

  const getInitToken = async () => {
    const authService = new AuthenticationService()
    const response = await authService.getPermissionBaseAuthentication()
    setInitToken(response?.data?.token)
  }
  return (
    <Grid 
      container
      direction='row'
      justifyContent='space-between'
      alignItems='flex-end'
      className={classes.root}
    >
      <LeftPanel t={t}/>
      <RightPanel
        initToken={initToken}
      />
    </Grid>
  )
}

const LeftPanel = ({t}) => {
  const classes = useStyles()
  return (
    <Grid
      container
      justifyContent='flex-end'
      item
      lg={6}
      xs={12}
      className={classes.leftPanel}
    >
      <div>
        <Typography className={classes.title}>
          Cross Border Payments Made Simple.
        </Typography>
        <Typography>
          Zed is a payment orchestration platform focused on cross border payments and foreign exchange (FX) in 200+ countries. 
        </Typography>
        <br/>
        <Typography>
          Unleash your international payments and scale up rapidly with a single API integration and save up to 75% in fees!
        </Typography>
      </div>
      <img
        src={IntroGIF}
        alt='intro.gif'
        className={classes.introGif}
      />
      
    </Grid>
  )
}

const RightPanel = ({initToken}) => {
  const classes = useStyles()
  return (
    <Grid
      container
      direction='row'
      justifyContent='center'
      item
      lg={5}
      xs={12}
      className={classes.rightPanel}
    >
      <Grid
        container
        direction='column'
        justifyContent='center'
        className={classes.conversionContainer}
      >
        <CurrencyConversion initToken={initToken}/>
      </Grid>
      <img
        src={IntroGIF}
        alt='intro.gif'
        className={classes.introGif}
      />
    </Grid>
  )
}
export default Intro