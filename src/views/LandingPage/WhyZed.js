import React from "react"
import { makeStyles } from '@material-ui/core/styles'

import { Grid,  Typography } from "@material-ui/core"

import StrengthDetail from "./components/StrengthDetail"
import MoneyGif from "../../images/imgs/money.gif"
import AccessAll from "src/images/imgs/access_all.png"
import OneApi from "src/images/imgs/one_api.png"
import OpenPlatform from "src/images/imgs/open_platform.png"
import RealTimeData from "src/images/imgs/real_time_data.png"

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '80px 20%',
    backgroundColor: '#11528B',
    '&>img':{
      display: 'none'
    },
    [theme.breakpoints.down("lg")]: {
      padding: '60px 40px',
      alignItems: 'center'
    },
    [theme.breakpoints.down("sm")]: {
      padding: '60px 20px',
      '&>img':{
        display: 'unset'
      },
    },
  },
  title: {
    fontSize: 38,
    color: theme.palette.primaryWhite,
    fontWeight: theme.typography.fontWeightLarge
  },
  img: {
    width: 417, 
    height: 391,
    [theme.breakpoints.down("sm")]: {
      width: 230, 
      height: 215,
    },
  },
  imageContainer: {
    [theme.breakpoints.down("sm")]: {
      '&>img':{
        display: 'none'
      },
    },
  }
}))

const STRENGTHS = [
  {
    image: OpenPlatform,
    title: 'Open Platform',
    desc: 'You can keep your existing payment processors and gateways or let us introduce you to some of our per-vetted domestic and global payment partners. The choice is yours.',
  },
  {
    image: AccessAll,
    title: 'Accessible to all',
    desc: 'We orchestrate your entire payment solution regardless of your company\'s size, stage, geography, industry or transaction volume.',
  },
  {
    image: OneApi,
    title: 'One API',
    desc: 'Zed helps you avoid directly integrating with multiple payment processors, managing different platforms and ingesting non-standardized reporting files.',
  },
  {
    image: RealTimeData,
    title: 'Real time data',
    desc: 'We provide a state of the art, real-time data monitoring and reporting service that will make sure that you are getting consistently good data coming in from all of your current payment processors so that you can close your books in one day!',
  },
]

const WhyZed = () => {
  const classes = useStyles()
  return (
    <Grid 
      container
      direction='column'
      alignItems='flex-start'
      className={classes.root}
    >
      <img
        src={MoneyGif}
        alt={MoneyGif}
        className={classes.img}
      />
      <Typography className={classes.title}>Why Zed?</Typography>
      <Grid
        container
        direction='row'
        justifyContent='flex-start'
      >
        <Grid
          item
          lg={8}
          xs={12}
          container
          direction='row'
        >
          {
            STRENGTHS.map(strengthInfo => 
              <StrengthDetail 
                strengthInfo={strengthInfo}
                key={strengthInfo.title}
              />)
          }
          
        </Grid>
        <Grid
          container
          justifyContent='center'
          item
          lg={4}
          xs={12}
          className={classes.imageContainer}
        >
          <img
            src={MoneyGif}
            alt={MoneyGif}
            className={classes.img}
          />
        </Grid>

      </Grid>
    </Grid>
  )
}

export default WhyZed