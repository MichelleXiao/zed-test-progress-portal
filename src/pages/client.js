import React, {useState, useEffect} from "react"

import { Router } from "@reach/router"
import { graphql } from 'gatsby'

import { getCookie } from 'src/services/Cookies'
import { useI18next } from "gatsby-plugin-react-i18next"

import Recipient from "../views/Recipient"
import Wallet from "../views/Wallet"
import Transfer from "../views/Transfer"
import PrivateRoutes, {getUserId} from "../components/PrivateRoutes"
import NotFoundPage from "./404";
import Sender from "../views/Sender"

const App = () => {
  const [authToken, setAuthToken] = useState(null);

  useEffect(() => {
    const token = getCookie('Authorization')
    setAuthToken(token);
  }, [])
  const userId = authToken && getUserId(authToken)
  const {language} = useI18next()
  if (!authToken) return null 
  return authToken && (
    <>
      <Router basepath={language === "en" ? "" : '/:lang'}>
        {userId ? <PrivateRoutes path='**' component={NotFoundPage}/> : <NotFoundPage path='**'/>}
        <PrivateRoutes path='/client/recipient' component={Recipient} />
        <PrivateRoutes path='/client/sender' component={Sender} />
        <PrivateRoutes path='/client/wallet' component={Wallet} />
        <PrivateRoutes path='/client/thunes' component={Transfer} />
        <PrivateRoutes path='/client/corpay' component={Transfer} />
      </Router>
    </>
  )
}
export default App

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node { 
          ns
          data
          language
        }
      }
    }
  }
`
