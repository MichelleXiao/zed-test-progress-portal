import React, { useEffect, useState } from "react"

/** Utils **/
import { navigate, graphql } from "gatsby"
import { useDispatch, useSelector } from "react-redux"
import toastHook from "../hooks/toastHook"
import { setCookie, getCookie  } from "../services/Cookies"

/** Services **/
import { AuthenticationService } from "../services/zed-api"
import { getHttpErrorMessage } from "src/services/helper"

/** Components **/
import AccountForm from "../views/ClientSignIn/AccountForm"
import SigninLayout from "../views/ClientSignIn/SigninLayout"

export default () => {
  const toastDispatcher = toastHook()
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false);
  const [authToken, setAuthToken] = useState(null);
  const [errMsg, setErrMsg] = useState()

  useEffect(() => {
    const token = getCookie('Authorization')
    setAuthToken(token);
  }, [])

  const isAppInitialize = useSelector(state => state?.app?.isInitialize)

  useEffect(() => {
    try {
      const token = getCookie('Authorization')
      initialize(token)
    } catch (e) {
      dispatch({ type: "INITIALIZE", payload: { isInitialize: true } })
      setLoading(false)
      console.error(e)
    }
  }, [isAppInitialize])

  const initialize = async (token) => {
    if (authToken || token) {
      dispatch({ type: "INITIALIZE", payload: { isInitialize: true }})
      setLoading(false)
      return
    }
    const authService = new AuthenticationService()
    const response = await authService.getPermissionBaseAuthentication()
    setCookie("Authorization", response?.data?.token, 86399)
    dispatch({ type: "INITIALIZE", payload: { isInitialize: true }})
    setLoading(false)
  }

  
  const onLogin = async emailAndPassword => {
    try {
      setLoading(true)
      const authService = new AuthenticationService()
      const response = await authService.login(emailAndPassword)
      if (response.status !== 200) {
        const errorMessage = response.data?.detail
        setErrMsg(errorMessage)
        return
      }
      setCookie("Authorization", response?.data?.token, 86399)
      navigate("/client/recipient")
    } catch (e) {
      toastDispatcher("Something went wrong while trying to login", 'error')
    } finally {
      setLoading(false)
    }
  }

  if(loading || !isAppInitialize) return null
  return (
    <SigninLayout>
      <AccountForm 
        onLogin={emailAndPassword => onLogin(emailAndPassword)} 
        loading={loading} 
        errMsg={errMsg} 
        setErrMsg={setErrMsg}
      />
    </SigninLayout>
  )
}

export const query = graphql`
  query ($language: String!) {
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`