import React from "react"
import { navigate } from "gatsby"
import LoadingButton from 'src/components/LoadingButton';

// import logo from "../images/giphy.gif"

export default () => {
  const style = {
    margin: "50px auto",
    padding: "40px 60px",
    boxSizing: "border-box",
    borderRadius: "4px",
    textAlign: "center"
  }

  return (
    <div style={style}>
      {/*<img src={logo} />*/}
      <h4>Congratulations!</h4>
      <h5>
        You can now login and start integrating with ZED services
      </h5>
      <LoadingButton onClick={() => navigate("/")}> GO TO LOGIN </LoadingButton>
    </div>
  )
}