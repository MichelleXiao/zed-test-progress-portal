import React from "react"

import { makeStyles, createStyles } from '@material-ui/core/styles'
import { Grid, Typography, SvgIcon } from "@material-ui/core"

import SettingsIcon from '@material-ui/icons/Settings'

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      height: '100%',
    },
    rotateIcon: {
      width: '3rem',
      height: '3rem',
      color: theme.palette.text.darkBlue,
      animation: `spin 4s linear infinite`
    },
    desc: {
      color: theme.palette.text.darkBlue,
      fontSize: 18,
      marginLeft: '1rem'
    }
  })
)

const UnderConstruction = () => {
  const classes = useStyles()
  return (
    <Grid 
      container
      justifyContent='center'
      alignItems='center'
      className={classes.root}
    >
      <SvgIcon component={SettingsIcon} viewBox='0 0 24 24' className={classes.rotateIcon} />
      <style>
        {`
            @keyframes spin {
                 0% { transform: rotate(0deg); }
                 100% { transform: rotate(360deg); }
            }
        `}
      </style>
      <Typography variant='h6' className={classes.desc}>Coming soon...</Typography>
    </Grid>
  )
}

export default UnderConstruction