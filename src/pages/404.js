import React, { useState, useEffect } from "react"
import { navigate } from "gatsby"

import { makeStyles } from '@material-ui/core/styles'

import { getCookie } from 'src/services/Cookies'
import { getUserId } from 'src/components/PrivateRoutes'

import { Grid,  Typography, Button } from "@material-ui/core"
import SEO from "../components/seo"
import Header from "src/components/LandingHeader"
import BgImg from "src/images/imgs/background.svg"

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& p': {
      color: theme.palette.text.darkGray,
      textAlign: 'center',
    }
  },
  button: {
    width: 300,
    backgroundColor: '#3273AD',
    borderRadius: 30,
    color: theme.palette.primaryWhite,
    marginTop: '4rem',
    '&:hover': {
      backgroundColor: '#15A0F5'
    }
  }
}))

const NotFoundPage = () => { 
  const classes = useStyles()
  const [authToken, setAuthToken] = useState(null);

  useEffect(() => {
    const token = getCookie('Authorization')
    setAuthToken(token);
  }, [])
  
  const userId = authToken && getUserId(authToken)
  
  return (
    <Grid style={{height: '100vh'}}>
      <SEO title="404: Not found" />
      {!userId && <Header/>}

      <Grid
        direction='column'
        justifyContent='center'
        alignItems='center'
        className={classes.root}
        style={{
          height: userId ? 'calc(100% - 120px)' : 'calc(100% - 60px)',
          marginTop: userId && '60px',
          background: `transparent url(${BgImg}) 0% 0% no-repeat padding-box`,
          backgroundSize: '60% 60%',
          backgroundPosition: 'center',
        }}
      >

        <Typography 
          style={{
            fontSize: '2rem',
            fontWeight: 600,
            marginBottom: '0.5rem'
          }}
        >
          Page Not Found: 404
        </Typography>
        <Typography 
          style={{
            fontSize: '0.9rem',
            fontWeight: 500,
          }}
        >
          We are sorry - this page doesn't exist
        </Typography>
        <Button className={classes.button} onClick={() => navigate(userId ? '/client/dashboard' : '/')}>
            {userId ? 'Go to Dashboard' : 'Go to Home Page'}
        </Button>
      </Grid>
    </Grid>
  )
}

export default NotFoundPage
