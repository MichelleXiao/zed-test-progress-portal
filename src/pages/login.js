import React, { useEffect, useState } from "react"
import { navigate } from "gatsby"

import AccountForm from "../views/ClientSignIn/AccountForm"
import SigninLayout from "../views/ClientSignIn/SigninLayout"

import toastHook from "../hooks/toastHook"
import { AuthenticationService } from "../services/zed-api"
import { setCookie, getCookie  } from "../services/Cookies"

export default ({location}) => {
  const toastDispatcher = toastHook()
  const [loading, setLoading] = useState(false);
  const [errMsg, setErrMsg] = useState()
  const onLogin = async emailAndPassword => {
    try {
      setLoading(true)
      const authService = new AuthenticationService()
      const response = await authService.login(emailAndPassword)
      if (response.status !== 200) {
        const errorMessage = response.data?.detail
        setErrMsg(errorMessage)
        return
      }
      setCookie("Authorization", response?.data?.token, 86399)
      if(location?.state?.conversionInfo) {
        navigate(
          `/client/currency-conversion`,
          {
            state: { 
              conversionInfo: location?.state?.conversionInfo
            },
          }
        )
        return
      } 
      navigate("/client/dashboard")
    } catch (e) {
      toastDispatcher("Something went wrong while trying to login", 'error')
    } finally {
      setLoading(false)
    }
  }
  return (
    <SigninLayout>
      <AccountForm 
        onLogin={emailAndPassword => onLogin(emailAndPassword)} 
        loading={loading} 
        errMsg={errMsg} 
        setErrMsg={setErrMsg}
      />
    </SigninLayout>
  )
    
}