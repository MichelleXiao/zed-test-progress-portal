const BUSINESS_TYPE = "Business"
const INDIVIDUAL_TYPE = "Individual"

const isAccountTypeCreated = (individualList, individualAccountType) => !!individualList.find(individual => individual.account_type === individualAccountType)
const isIndividualIDAdded = (individualList) => !!individualList.find(individual => individual?.identification_cards.length > 0)

export const checkSenderRecipientStatus = (individualList) => {
  const create_business_recipient = isAccountTypeCreated(individualList, BUSINESS_TYPE)
  const create_individual_recipient = isAccountTypeCreated(individualList, INDIVIDUAL_TYPE)
  const successful_add_of_recipient_id = isIndividualIDAdded(individualList)
  return ({
    successful_add_of_recipient_id: successful_add_of_recipient_id,
    create_individual_recipient: create_individual_recipient,
    create_business_recipient: create_business_recipient
  })
}

export const checkWalletStatus = (individualList) => {
  let create_cash_pickup_wallet = false
  let create_mobile_wallet = false
  let create_bank_wallet = false
  const isAllCompleted = create_cash_pickup_wallet && create_mobile_wallet && create_bank_wallet
  individualList.forEach(individual => {
    if(isAllCompleted) return
    const walletArr = Object.values(individual.payout_wallet)
    walletArr.forEach(wallet => {
      if(!create_bank_wallet && wallet.type === "Bank") {
        create_bank_wallet = true
      }
      if(!create_mobile_wallet && wallet.type === "Mobile Wallet") {
        create_mobile_wallet = true
      }
      if(!create_cash_pickup_wallet && wallet.type === "Cash Pickup") {
        create_cash_pickup_wallet = true   
      }
    })
  })
  return ({
    create_cash_pickup_wallet: create_cash_pickup_wallet,
    create_mobile_wallet: create_mobile_wallet,
    create_bank_wallet: create_bank_wallet
  })
}

