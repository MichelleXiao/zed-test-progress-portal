import axios from "axios"
import { getAuthToken } from "./Cookies"

axios.defaults.baseURL = process.env.GATSBY_API_BASE_URL

const validateStatus = status => status <= 500

export class BulkTransactionsService {
  downloadTemplate = async template => {
    const headers = { Authorization: getAuthToken() }
    return await axios.get(
      `bulk-transfer/file/download-recipients?template=${template}`,
      {
        headers,
        responseType: "blob",
        validateStatus,
      }
    )
  }

  importBulkRecipients = async file => {
    const headers = { Authorization: getAuthToken() }

    const formData = new FormData()
    formData.append("file", file)

    return await axios.post(`/bulk-transfer/file/recipient`, formData, {
      headers,
      validateStatus,
    })
  }
}
