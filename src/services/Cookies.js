import authTokenObserver from "./authTokenObserver"
import { navigate } from 'gatsby'

const notifyNoAuthToken = () => {
  authTokenObserver.notifyAuthExpired()
}

export const getAuthToken = () => {
  const authToken = getCookie("Authorization")
  if (!authToken) {
    notifyNoAuthToken()
    return
  }

  return authToken
}

export const getCookie = cookieName => {
  //  AVOID gatsby build SSR failure
  if (!typeof document === undefined) return

  let cookieData = document.cookie
    .split("; ")
    .find(row => row.startsWith(cookieName))
    ?.split("=")[1]
  return cookieData
}

export const setCookie = (key, value, maxAge) => {
  if (!document || !value) {
    return
  }
  document.cookie = maxAge
    ? `${key}=${value};path=/;max-age=${maxAge}`
    : `${key}=${value};path=/`
}

export const deleteCookie = () => {
  const now = new Date()
  if( getCookie( "Authorization" ) ) {
    document.cookie = "Authorization" + "=" +
      ";path=/"+
      `;expires=${now}`;
    navigate("/")
  }
  
}