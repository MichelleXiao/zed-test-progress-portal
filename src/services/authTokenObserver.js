class AuthTokenObserver {

  constructor() {
    this.observers = []
  }

  attachObserver = (observer) => {
    this.observers.push(observer)
  }

  detachObserver = (_observer) => {
    this.observers = this.observers.filter(observer => observer !== _observer)
  }

  notifyAuthExpired = () => {

    if (this.observers.length === 0) return;

    for (const observer of this.observers) {
      observer()
    }

  }

}
const authTokenObserver = new AuthTokenObserver()
export default authTokenObserver
