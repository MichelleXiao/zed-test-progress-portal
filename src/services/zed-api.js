import axios from "axios"
import { getAuthToken } from "./Cookies"

axios.defaults.baseURL = process.env.GATSBY_API_BASE_URL

const validateStatus = status => status < 500

export class Reference {
  getCompanyTypes = async (token) => {
    const headers = { Authorization: token || getAuthToken() }
    return await axios.get(`/payout-partners/company-types`, {
      headers,
    })
  }
  getNaics = async (token) => {
    const headers = { Authorization: token || getAuthToken() }
    return await axios.get(`/payout-partners/naics`, {
      headers,
    })
  }
}

export class AuthenticationService {
  getPermissionBaseAuthentication = async () => {
    return await axios.post("/auth/permission-token/", {})
  }

  signup = async data => {
    return await axios.post(`/auth/signup`, data)
  }

  login = async emailAndPassword => {
    return await axios.post("/auth/authenticate/", emailAndPassword, {
      validateStatus,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
  }

  checkEmailAvailability = async (email) => {
    return await axios.get("/auth/is-email-available", {
      params: {
        email
      },
      validateStatus,
    })
  }
}

export class ClientService {
  createPartnerAccount = async data => {
    const headers = { Authorization: getAuthToken() }
    return await axios.post(`/client/create-client`, data, {
      headers,
      validateStatus,
    })
  }

  getDetails = async () => {
    const headers = { Authorization: getAuthToken() }
    return await axios.get(`/client/fetch-client`, { headers, validateStatus })
  }

  /**
   *
   * @param {object} serviceName the service name
   */
  integrateService = async serviceName => {
    const headers = { Authorization: getAuthToken() }
    return await axios.patch(
      `/client/integrate-service?service=${serviceName}`,
      {},
      {
        headers,
        validateStatus,
      }
    )
  }

  getAccountsBalance = async () => {
    const headers = { Authorization: getAuthToken() }
    return await axios.get(
      `/client/balance`,
      {
        headers,
        validateStatus,
      }
    )
  }

  getAccountBalanceHistory = async (currency, startDate, endDate) => {
    const headers = { Authorization: getAuthToken() }
    return await axios.get(
      `/client/balance-history`,
      {
        headers,
        params: {
          currency: currency,
          start_date: startDate,
          end_date: endDate
        },
        validateStatus,
      }
    )
  }

  getClientDetails = async accessToken => {
    // TODO: THE API is not yet created
    const headers = { Authorization: getAuthToken() }
    return await axios.get(`/client?accessToken=${accessToken}`, { headers })
  }

  upsertCredential = async data => {
    const headers = { Authorization: getAuthToken() }
    return await axios.post(`/client/upsert-credential?`, data, { headers })
  }

  getCreditLimit = async () => {
    const headers = { Authorization: getAuthToken() }
    const response = await axios.get(`/client/account-credit-limits`, {
      headers,
    })
    return response
  }

  getActivity = async ({ page = 1, search }) => {
    const headers = { Authorization: getAuthToken() }
    const response = await axios.get(`/logs/fetch-activity`, {
      headers,
      params: {
        page,
        search
      }
    })

    return response
  }
  
  changePassword = async data => {
    const headers = { Authorization: getAuthToken() }
    return await axios.put(`/client/change-password`, data, {
      headers,
      validateStatus,
    })
  }


  updateClient = async (data) => {
    const headers = { Authorization: getAuthToken() }
    return await axios.put(`/client/update-client`, data, {
      headers,
      validateStatus,
    })
  }

  getClientPreferences = async () => {
    const headers = { Authorization: getAuthToken() }
    return await axios.get(
      `/client/setting`,
      {
        headers,
        validateStatus,
      }
    )
  }

  updateClientPreferences = async (data) => {
    const headers = { Authorization: getAuthToken() }
    return await axios.put(`/client/setting`, data, {
      headers,
      validateStatus,
    })
  }
  
}
