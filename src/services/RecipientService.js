import axios from "axios"
import { getAuthToken } from "./Cookies"

axios.defaults.baseURL = process.env.GATSBY_API_BASE_URL

export class RecipientService {

  getRecipients = async (page, searchTerm) => {
    const headers = { Authorization: getAuthToken() }
    const queryParams = searchTerm
      ? `search=${searchTerm}`
      : `page=${page}`
    return await axios.get(`/profile/recipient?${queryParams}`, {
      headers,
    })
  }

  getSenders = async (page, searchTerm) => {
    const headers = { Authorization: getAuthToken() }
    const queryParams = searchTerm
      ? `search=${searchTerm}`
      : `page=${page}`
    return await axios.get(`/profile/sender?${queryParams}`, {
      headers,
    })
  }

  
}
