const CORPAY = "CORPAY"
const CORPAY_TRANSFER_ID_LENGTH = 8
const THUNES = "THUNES"
const THUNES_TRANSFER_ID_LENGTH = 7

const CORPAY_STATUS = [
  "CONFIRMED", 
  "COMPLETED", 
  "DECLINED", 
  "CANCELED"
]

const THUNES_NEGATIVE_STATUSES = [
  "REJECTED", 
  "REJECTED_SLS_SENDER", 
  "REJECTED_SLS_BENEFICIARY", 
  "REJECTED_INVALID_BENEFICIARY", 
  "REJECTED_BARRED_BENEFICIARY", 
  "REJECTED_BARRED_SENDER", 
  "REJECTED_INVALID_BENEFICIARY_DETAILS", 
  "REJECTED_LIMITATIONS_ON_TRANSACTION_VALUE", 
  "REJECTED_LIMITATIONS_ON_SENDER_VALUE", 
  "REJECTED_LIMITATIONS_ON_BENEFICIARY_VALUE", 
  "REJECTED_LIMITATIONS_ON_ACCOUNT_VALUE", 
  "REJECTED_LIMITATIONS_ON_SENDER_QUANTITY", 
  "REJECTED_LIMITATIONS_ON_BENEFICIARY_QUANTITY", 
  "REJECTED_LIMITATIONS_ON_ACCOUNT_QUANTITY", 
  "REJECTED_COMPLIANCE_REASON", 
  "REJECTED_PAYER_CURRENTLY_UNAVAILABLE", 
  "REJECTED_INSUFFICIENT_BALANCE", 
  "DECLINED", 
  "DECLINED_SLS_SENDER", 
  "DECLINED_SLS_BENEFICIARY", 
  "DECLINED_INVALID_BENEFICIARY", 
  "DECLINED_BARRED_BENEFICIARY", 
  "DECLINED_UNSUPPORTED_BENEFICIARY", 
  "DECLINED_INVALID_BENEFICIARY_DETAILS", 
  "DECLINED_INVALID_SENDER_DETAILS", 
  "DECLINED_LIMITATIONS_ON_TRANSACTION_VALUE", 
  "DECLINED_LIMITATIONS_ON_SENDER_VALUE", 
  "DECLINED_LIMITATIONS_ON_BENEFICIARY_VALUE", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_VALUE", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_VALUE_DAILY", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_VALUE_WEEKLY", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_VALUE_MONTHLY", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_VALUE_YEARLY", 
  "DECLINED_LIMITATIONS_ON_SENDER_QUANTITY",
  "DECLINED_LIMITATIONS_ON_BENEFICIARY_QUANTITY", 
  "DECLINED_LIMITATIONS_ON_ACCOUNT_QUANTITY", 
  "DECLINED_DUPLICATED_TRANSACTION", 
  "DECLINED_CANCELLED", 
  "DECLINED_REFUSED", 
  "DECLINED_COMPLIANCE_REASON",
  "DECLINED_INVALID_PURPOSE_OF_REMITTANCE", 
  "DECLINED_PAYER_CURRENTLY_UNAVAILABLE"
]

const ONLINE_BANKING = "ONLINE_BANKING"
const MOBILE_TRANSFER = "MOBILE_TRANSFER"
const CASH_PICKUP = "CASH_PICKUP"

const getTransfersByPayoutPartner = (payoutPartner, transferList) => {
  if(payoutPartner === CORPAY) {
    const corpayTransferList = transferList.filter(transfer => transfer?.partner_reference_id?.length === CORPAY_TRANSFER_ID_LENGTH)
    return corpayTransferList
  }
  if(payoutPartner === THUNES) {
    const thunesTransferList = transferList.filter(transfer => transfer?.partner_reference_id?.length === THUNES_TRANSFER_ID_LENGTH)
    return thunesTransferList
  }
}

const categorizeTransfersByOperationType = (thunesTransferList) => {
  const mobileTransfers = thunesTransferList.filter(transfer => transfer.transaction_operation_type === MOBILE_TRANSFER)
  const cashPickupTransfers = thunesTransferList.filter(transfer => transfer.transaction_operation_type === CASH_PICKUP)
  const bankTransfers = thunesTransferList.filter(transfer => transfer.transaction_operation_type === ONLINE_BANKING)
  return ({
    mobileTransfers: mobileTransfers,
    cashPickupTransfers: cashPickupTransfers,
    bankTransfers: bankTransfers
  })
}

const filterUniqueTransfer = (transfers) => {
  let existingStatuses = []
  return transfers.filter(transfer => {
    if(existingStatuses.includes(transfer.status)) return
    existingStatuses.push(transfer.status)
    return transfer
  })
}

const countNegativeStatus = (uniqueBankTransfers) => {
  let statusCounter = 0
  uniqueBankTransfers.forEach(transfer => {
    THUNES_NEGATIVE_STATUSES.includes(transfer.status) && statusCounter++
  })
  return statusCounter
}

export const checkCorpayTransfers = (transferList) => {
  const corpayTransferList = getTransfersByPayoutPartner(CORPAY, transferList)
  const create_batch_transfer = !!corpayTransferList.find(transfer => transfer?.bulk_transfer_reference)
  const completed_single_transfer = !!corpayTransferList.find(transfer => transfer?.status === "COMPLETED")
  const confirmed_single_transfer = !!corpayTransferList.find(transfer => transfer?.status === "CONFIRMED")
  const insufficient_balance = !!transferList.find(transfer => transfer?.status === "DECLINED_INSUFFICIENT_BALANCE") 
  return ({
    create_batch_transfer: create_batch_transfer,
    completed_single_transfer: completed_single_transfer,
    confirmed_single_transfer: confirmed_single_transfer,
    insufficient_balance: insufficient_balance
  })
}

export const checkThunesTransfers = (transferList) => {
  const thunesTransferList = getTransfersByPayoutPartner(THUNES, transferList)
  const {mobileTransfers, cashPickupTransfers, bankTransfers} = categorizeTransfersByOperationType(thunesTransferList)
  const uniqueBankTransfers = filterUniqueTransfer(bankTransfers)
  const completed_mobile_transfer = !!mobileTransfers.find(mobileTransfer => mobileTransfer.status === "COMPLETED")
  const declined_mobile_transfer = !!mobileTransfers.find(mobileTransfer => THUNES_NEGATIVE_STATUSES.includes(mobileTransfer.status))
  const completed_cash_pickup_transfer = !!cashPickupTransfers.find(cashPickupTransfer => cashPickupTransfer.status === "COMPLETED")
  const confirmed_cash_pickup_transfer = !!cashPickupTransfers.find(cashPickupTransfer => cashPickupTransfer.status === "CONFIRMED_WAITING_FOR_PICKUP")
  const declined_cash_transfer = !!cashPickupTransfers.find(cashPickupTransfer => THUNES_NEGATIVE_STATUSES.includes(cashPickupTransfer.status))
  const successfully_cancel_cash_pickup_transfer = !!cashPickupTransfers.find(cashPickupTransfer => cashPickupTransfer.status === "CANCELLED")
  const completed_bank_transfer = !!bankTransfers.find(bankTransfer => bankTransfer.status === "COMPLETED")
  const declined_bank_transfer = countNegativeStatus(uniqueBankTransfers) > 3
  const reversed_transfer = !!bankTransfers.find(bankTransfer => bankTransfer.status === "REVERSED")
  const insufficient_balance = !!transferList.find(transfer => transfer?.status === "DECLINED_INSUFFICIENT_BALANCE") 
  const add_transfer_attachment = !!thunesTransferList.find(transfer => transfer?.documents_attachment_id)
  return ({
    completed_mobile_transfer: completed_mobile_transfer,
    declined_mobile_transfer: declined_mobile_transfer,
    completed_cash_pickup_transfer: completed_cash_pickup_transfer,
    confirmed_cash_pickup_transfer: confirmed_cash_pickup_transfer,
    declined_cash_transfer: declined_cash_transfer,
    successfully_cancel_cash_pickup_transfer: successfully_cancel_cash_pickup_transfer,
    completed_bank_transfer: completed_bank_transfer,
    declined_bank_transfer: declined_bank_transfer,
    reversed_transfer: reversed_transfer,
    insufficient_balance: insufficient_balance,
    add_transfer_attachment: add_transfer_attachment
  })
}