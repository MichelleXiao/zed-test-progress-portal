import moment from "moment"
// https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore#_isempty
export const isEmpty = obj =>
  [Object, Array].includes((obj || {}).constructor) &&
  !Object.entries(obj || {}).length

/**
 * Format string date base on API standard format
 *
 * @param {string} date
 * @returns {string} date formatted
 */
export const transformDate = date => moment(date).format("YYYY/MM/DD")

/**
 * Format string date base on API standard format
 *
 * @param {string} date
 * @returns {string} date formatted
 */
export const transformDatePresentation = date =>
  moment(date, "YYYY/MM/DD").format("MMM DD, YYYY")

/**
 * Format phone number API standard format
 *
 * @param {string} phoneNumber
 * @returns {string} The the phone number with + in the prefix
 */
export const transformPhoneNumber = phoneNumber =>
  phoneNumber.includes("+") ? phoneNumber : `${phoneNumber}`

/**
 * Helper function for react-select as it only accepts array of objects {label: value}
 *
 * @param {string} phoneNumber
 * @returns {string} The the phone number with + in the prefix
 */
export const toLabelValue = item => {
  return item ? { label: item, value: item } : null
}

/**
 * Extract an error from an HTTP Response Object
 * @param {*} httpResponse 
 * @returns 
 */
export const getHttpErrorMessage = httpResponse => {
  if (!httpResponse?.data) return

  const { data } = httpResponse
  if (data?.detail) return data.detail

  // if (Array.isArray(data)) {
  //   return data.filter(item => item?.message)
  // }
}


/**
 * Concat multiple arrays into one array
 *
 * @param {Array} arrays
 * @returns {Array} Concated array
 */
export const concatArrays = (...arrays) => [].concat(...arrays.filter(Array.isArray)) 

/**
 * Format a string into sentence style, with first letter capitalized.
 *
 * @param {string} String
 * @returns {String} First letter capitalized
 */
 export const capitalizeFirstLetter = (str) => {
   const lowerCasedStr = str.toLowerCase()
   return lowerCasedStr.charAt(0).toUpperCase() + lowerCasedStr.slice(1)
 }




 /**
 * Compare strings 
 *
 * @param {string} String
 * @returns {Number} Compare strings, and sort them alphbetically  
 * Designed for sorting table cell which contains multiples lines. Eg. Recipient name + created date. But we only want to sort by name. 
 */
  export const sortStr = (a, b) => a.toLowerCase() < b.toLowerCase() ? -1 : 1

 /**
 * Compare strings 
 *
 * @param {string} Date string
 * @returns {Number} 
 * Sort dates.  
 */
  export const sortDate = (a, b) => {
    const aDate = new Date(a)
    const bDate = new Date(b)
    return aDate < bDate ? -1 : 1
  }

/**
 * Compare strings 
 *
 * @param {string} Date string
 * @returns {string}  String
 * Convert afex date string into zed date string
 */
  export const transformDateStr = (dateStr) => moment(dateStr, "MM/DD/YYYY").format("YYYY/MM/DD")

/**
 * Compare strings 
 *
 * @param {string} String
 * @returns {string}  String
 * remove minus sign 
 */
  export const removeNegative = (str) => str.replace("-", '')

/**
 * Compare strings 
 *
 * @param {string} String
 * @returns {string}  String
 * Remove whitespace at beginning of a string, and remove invalid whitespaces (eg. "  1  street      rd" => "1 street rd")
 */
   export const removeInvalidSpaces = (str) => str.trimStart().replace(/\s\s+/g, ' ')
/**
 * Compare strings 
 *
 * @param {string} String
 * @returns {string}  String
 * Remove whitespace at beginning of a string, and remove invalid whitespaces (eg. "  1  street      rd" => "1 street rd")
 */
 export const removeWhiteSpaces = (str) => str.trimStart().replace(/\s/g, "")

  /**
 *
 * @param {object} Object expected recipient object
 * @returns {string}  String formated recipient name or business name
 * 
 */
  export const handleEmptyName = (originalRow) => {
    if (!originalRow.first_name && !originalRow.last_name && originalRow.business_name) return originalRow.business_name
    return `${originalRow.first_name} ${originalRow.last_name}`
  }


/**
 * Debounce validation using yup
 *
 * @param {object} Function, Number, Function
 * @returns {string} any
 * 
 */
export const yupDebounce = (fn, wait, callFirst) => {
  let timeout;
  return function() {
    return new Promise(async (resolve) => {
      if (!wait) {
        const result = await fn.apply(this, arguments);
        resolve(result);
      }

      const context = this;
      const args = arguments;
      const callNow = callFirst && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(async function() {
        timeout = null;
        if (!callNow) {
          const result = await fn.apply(context, args);
          resolve(result);
        }
      }, wait);

      if (callNow) {
        const result = await fn.apply(this, arguments);
        resolve(result);
      }
    });
  };
};

/**
 * determine correct conversion rate to do calculation
 *
 * @param {object} Object response of getRate
 * @returns {number}  Correct conversion rate
 * 
 */
 export const handleTerms = (conversion) => conversion.terms === "A" ? conversion.rate : conversion.inverted_rate

 /**
 * Compare strings 
 *
 * @param {string} String
 * @returns {boolean} Compare strings, and return boolean
 */
export const textContain = (text, keyword) => `${text}`.toLowerCase().includes(keyword.toLowerCase())

 /**
 * Transform each word to have first letter capitalized and rest in lowercase. 
 * Eg. nEw comPany => New Company
 *
 * @param {string} String
 * @returns {string} String
 */
export const toTitleCase = (str) => {
  return str
    .toLowerCase()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
};

 /**
 * Check and add valid http header to incoming string 
 * Eg. www.zed.network => https://www.zed.network
 *
 * @param {string} String
 * @returns {string} String
 */
export const transformWebsite = (str) => {
  const urlHead = 'https://'
  const isUrlFormat = str.substr(0, 8) === urlHead
  return isUrlFormat ? str : urlHead.concat(str)
}

 /**
 * Check and formate incoming string to US phone formate
 * Eg. 905123467 => +1 (905) 123-4657
 *
 * @param {string} String
 * @returns {string} String
 */
export const formatPhoneNumber = (phoneNumberString) => {
  const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
  const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    const intlCode = (match[1] ? '+1 ' : '');
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
  }
  return null;
}

 /**
 * Remove extra decimal places
 * Eg. 1.0000002 => 1.00
 *
 * @param {string} String sting of numbers in currency style
 * @param {number} Integer Decimal places 
 * @returns {string} String
 */
  export const toDecimalPlaces = (str, decimalPlaces = 2) => {
    return str
      .split('.')
      .map((num, index) => {
        if (index !== 1) return num
        return num.slice(0, decimalPlaces)
      })
      .join('.');
  };

/**
 * determine correct conversion rate to do calculation
 *
 * @param {object} Object response of getRate
 * @returns {number}  Correct conversion rate
 * 
 */
 export const handleCalculateRate = (conversion) => {
   const rate = conversion.converted_amount/conversion.settlement_amount
   return rate.toFixed(5)
 }
 
 export const percentageOfCompleteness = (completedApiList) => {
  let numOfRequiredTestCases = 0
  let numOfCompletedTestCases = 0
  completedApiList.forEach(api => {
    numOfRequiredTestCases = numOfRequiredTestCases + api.test_cases.filter(testCase => testCase.is_required).length
  })
  completedApiList.forEach(api => {
    numOfCompletedTestCases = numOfCompletedTestCases + api.test_cases.filter(testCase => testCase.is_completed).length
  })
  const incompleteApiList = completedApiList.filter(api => {
    const includesIncomplteTestCase = api.test_cases.find(testCase => {
      return testCase.is_required && !testCase.is_completed})
    return includesIncomplteTestCase && api
  })
  return {
    numOfRequiredTestCases: numOfRequiredTestCases,
    numOfCompletedTestCases: numOfCompletedTestCases,
    incompleteApiList: incompleteApiList
  }
}