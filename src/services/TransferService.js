import axios from "axios"
import { getAuthToken } from "./Cookies"

axios.defaults.baseURL = process.env.GATSBY_API_BASE_URL

export class TransferService {
  
  getTransfers = async (search) => {
    const queryParams = `search=${search}`
    const headers = { Authorization: getAuthToken()}
    const requestUrl = search ? `/transfer/payout/fetch-transfers-list?${queryParams}` : `/transfer/payout/fetch-transfers-list`
    return await axios.get(requestUrl, {
      headers,
    })
  }
}
