import axios from "axios"
import { getAuthToken } from "./Cookies"

axios.defaults.baseURL = process.env.GATSBY_API_BASE_URL

const validateStatus = status => status < 500

export class PayoutPartnerService {
  getPayoutPartner = async (page, searchTerm) => {
    const headers = { Authorization: getAuthToken() }

    return await axios.get(`/client/key-management`, {
      headers,
    })
  }
  upsertPayoutPartner = async data => {
    const headers = { Authorization: getAuthToken() }
    return await axios.post(`/client/key-management`, data, { headers, validateStatus })
  }
}
