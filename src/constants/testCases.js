export const TEST_BANK_MOBILE_NUMBERS_DATA = {
  title: "Test Bank and Mobile Transfers - with the following test numbers, you will be able to test different transfer statuses in the staging environment. The last 3 digits of account number or IBAN number(if applicable) determine the response given and the \"X\" stands for any number. ",
  instruction: "",
  test_numbers: [
    {
      test_number: 'XXX XXX 100',
      expected_status: 'COMPLETED'
    },
    {
      test_number: 'XXX XXX 101',
      expected_status: 'DECLINED-INVALID-BENEFICIARY'
    },
    {
      test_number: 'XXX XXX 104',
      expected_status: 'DECLINED-BARRED-BENEFICIARY'
    },
    {
      test_number: 'XXX XXX 105',
      expected_status: 'DECLINED-SLS-BENEFICIARY'  
    },
    {
      test_number: 'XXX XXX 106',
      expected_status: 'DECLINED-SLS-SENDER'
    },
    {
      test_number: 'XXX XXX 110',
      expected_status: 'DECLINED-LIMITATIONS-ON-BENEFICIARY-QUANTITY'
    },
    {
      test_number: 'XXX XXX 111',
      expected_status: 'DECLINED-LIMITATIONS-ON-TRANSACTION-VALUE'
    },
    {
      test_number: 'XXX XXX 112',
      expected_status: 'DECLINED-LIMITATIONS-ON-ACCOUNT-VALUE'
    },
    {
      test_number: 'XXX XXX 113',
      expected_status: 'DECLINED-LIMITATIONS-ON-ACCOUNT-QUANTITY'
    },
    {
      test_number: 'XXX XXX 114',
      expected_status: 'DECLINED-LIMITATIONS-ON-SENDER-QUANTITY'
    },
    {
      test_number: 'XXX XXX 115',
      expected_status: 'DECLINED-LIMITATIONS-ON-SENDER-VALUE'
    },
    {
      test_number: 'XXX XXX 116',
      expected_status: 'DECLINED-LIMITATIONS-ON-BENEFICIARY-VALUE'
    },
    {
      test_number: 'XXX XXX 117',
      expected_status: 'DECLINED-CURRENTLY-UNAVAILABLE'
    },
    {
      test_number: 'XXX XXX 200',
      expected_status: 'COMPLETED (with delay)'
    },
    {
      test_number: 'XXX XXX 201',
      expected_status: 'DECLINED-INVALID-BENEFICIARY (with delay)'
    },
    {
      test_number: 'XXX XXX 203',
      expected_status: 'DECLINED-INVALID-BENEFICIARY-DETAILS (with delay)'
    },
    {
      test_number: 'XXX XXX 205',
      expected_status: 'DECLINED-SLS-BENEFICIARY (with delay)'
    },
    {
      test_number: 'XXX XXX 206',
      expected_status: 'DECLINED-SLS-SENDER (with delay)'
    },
    {
      test_number: 'XXX XXX 210',
      expected_status: 'DECLINED-LIMITATIONS-ON-BENEFICIARY-QUANTITY (with delay)'
    },
    {
      test_number: 'XXX XXX 211',
      expected_status: 'DECLINED-LIMITATIONS-ON-TRANSACTION-VALUE (with delay)'
    },
    {
      test_number: 'XXX XXX 216',
      expected_status: ' DECLINED-LIMITATIONS-ON-BENEFICIARY-VALUE (with delay)'
    },
    {
      test_number: 'XXX XXX 217',
      expected_status: 'DECLINED-CURRENTLY-UNAVAILABLE (with delay)'
    }
  ]
}

export const TEST_CASH_PICKUP_NUMBERS_DATA = {
  title: "Test Cash Pickup Transfers - With the following test numbers, you will be able to test different transfer statuses in the staging environment. The last 3 digits of mobile number determine the response given and the \"X\" stands for any number. Any other 3 digit ending numbers returns random statuses.",
  instruction: "",
  test_numbers: [
    {
      test_number: 'XXX XXX 320',
      expected_status: 'COMPLETED (with delay)'
    }
  ]
}

export const THUNES = [
  {
    key: "create_mobile_transfer",
    name: "Create Mobile Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Mobile%20Transfer",
    test_cases: [
      {
        case_id: "completed_mobile_transfer",
        case_name: "Completed Mobile Transfer",
        instruction: "Successfully create a mobile transfer with mobile_number ends with 320.",
        is_required: true,
        expected_result: "{\n\"sender_remote_id\": \"8faf28b1e6ac43f08c5fc6f4f4668f77\",\n\"amount\": \"10000\",\n\"quote_id\": \"18688638\",\n\"remote_id\": \"ec8e7eec70b24388a842d5d3f0a35316\",\n\"purpose_of_payment\": \"bill_payment\"\n}"
      },
      {
        case_id: "declined_mobile_transfer",
        case_name: "Declined Mobile Transfer",
        instruction: "Create a mobile tranfer and receive DECLINED status",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2494422\",\n\"transfer_id\": \"6bbf1706-0b22-4523-9467-9c3677d74db0\",\n\"ref_id\": null,\n\"amount\": 10000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"IDR\",\n\"rate\": 14257.46,\n\"memo\": null,\n\"delivery_date\": null,\n\"remote_id\": \"FFIBI-CUS76\",\n\"sender_remote_id\": \"f30709a0e9e44ca7908a173fa96ff609\",\n\"sender\": null,\n\"is_direct_debit\": false,\n\"status\": \"DECLINED\",\n\"fee_amount\": 1.2,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 0.7,\n\"transaction_operation_type\": \"MOBILE_TRANSFER\",\n\"cash_pickup_code\": null,\n\"created_date\": \"2022-04-21T20:33:22\",\n\"wallet_id\": \"8f20f70fb7d9451289468c458e9f2575\"\n}"
      }
    ]
  },
  {
    key: "create_cash_pickup_transfer",
    name: "Create Cash Pickup Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Cash%20Pickup%20Transfer",
    test_cases: [
      {
        case_id: "completed_cash_pickup_transfer",
        case_name: "Completed Cash Pickup Transfer",
        instruction: "Successfully create a cash pickup transfer with mobile_number ends with 320.",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2491602\",\n\"transfer_id\": \"6636376d-1f4e-42f3-bc2d-45e5c977297b\",\n\"ref_id\": null,\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"NPR\",\n\"rate\": 121.21,\n\"memo\": null,\n\"delivery_date\": null,\n\"remote_id\": \"BEN81\",\n\"sender_remote_id\": null,\n\"sender\": {\n\"first_name\": \"Firstname\",\n\"last_name\": \"Lastname\",\n\"middle_name\": \"\",\n\"gender\": \"FEMALE\",\n\"job_title\": \"OWNER\",\n\"phone_number\": \"+16663334444\",\n\"birthday\": \"1963-01-10\",\n\"relationship\": \"DAUGHTER\",\n\"source_of_funds\": \"CASH\",\n\"city\": \"\",\n\"province\": \"\",\n\"country\": \"US\",\n\"postal_code\": \"\",\n\"citizenship\": \"US\",\n\"id_number\": \"123\",\n\"id_type\": \"PASSPORT\",\n\"id_country_iso_code\": null,\n\"address\": \"111 Random Strrt, Florida\"\n},\n\"is_direct_debit\": false,\n\"status\": \"COMPLETED\",\n\"fee_amount\": 1.5,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 8.25,\n\"transaction_operation_type\": \"CASH_PICKUP\",\n\"cash_pickup_code\": \"THUNES-KHM-QX-XF4-GRR-EAD\",\n\"created_date\": \"2022-04-20T21:34:27\",\n\"wallet_id\": \"9b29c486426a48cf8f675636450e32ce\"\n}"
      },
      {
        case_id: "confirmed_cash_pickup_transfer",
        case_name: "Create Confirmed Cash Pickup Transfer",
        instruction: "Successfully create a cash pickup transfer with CONFIRMED_WAITING_FOR_PICKUP status",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2491602\",\n\"transfer_id\": \"6636376d-1f4e-42f3-bc2d-45e5c977297b\",\n\"ref_id\": null,\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"NPR\",\n\"rate\": 121.21,\n\"memo\": null,\n\"delivery_date\": null,\n\"remote_id\": \"BEN81\",\n\"sender_remote_id\": null,\n\"sender\": {\n\"first_name\": \"Firstname\",\n\"last_name\": \"Lastname\",\n\"middle_name\": \"\",\n\"gender\": \"FEMALE\",\n\"job_title\": \"OWNER\",\n\"phone_number\": \"+16663334444\",\n\"birthday\": \"1963-01-10\",\n\"relationship\": \"DAUGHTER\",\n\"source_of_funds\": \"CASH\",\n\"city\": \"\",\n\"province\": \"\",\n\"country\": \"US\",\n\"postal_code\": \"\",\n\"citizenship\": \"US\",\n\"id_number\": \"123\",\n\"id_type\": \"PASSPORT\",\n\"id_country_iso_code\": null,\n\"address\": \"111 Random Strrt, Florida\"\n},\n\"is_direct_debit\": false,\n\"status\": \"CONFIRMED_WAITING_FOR_PICKUP\",\n\"fee_amount\": 1.5,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 8.25,\n\"transaction_operation_type\": \"CASH_PICKUP\",\n\"cash_pickup_code\": \"THUNES-KHM-QX-XF4-GRR-EAD\",\n\"created_date\": \"2022-04-20T21:34:27\",\n\"wallet_id\": \"9b29c486426a48cf8f675636450e32ce\"\n}"
      },
      {
        case_id: "declined_cash_transfer",
        case_name: "Declined Cash Pickup Transfer",
        instruction: "Create a cash pickup tranfer and receive DECLINED status",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2491602\",\n\"transfer_id\": \"6636376d-1f4e-42f3-bc2d-45e5c977297b\",\n\"ref_id\": null,\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"NPR\",\n\"rate\": 121.21,\n\"memo\": null,\n\"delivery_date\": null,\n\"remote_id\": \"BEN81\",\n\"sender_remote_id\": null,\n\"sender\": {\n\"first_name\": \"Firstname\",\n\"last_name\": \"Lastname\",\n\"middle_name\": \"\",\n\"gender\": \"FEMALE\",\n\"job_title\": \"OWNER\",\n\"phone_number\": \"+16663334444\",\n\"birthday\": \"1963-01-10\",\n\"relationship\": \"DAUGHTER\",\n\"source_of_funds\": \"CASH\",\n\"city\": \"\",\n\"province\": \"\",\n\"country\": \"US\",\n\"postal_code\": \"\",\n\"citizenship\": \"US\",\n\"id_number\": \"123\",\n\"id_type\": \"PASSPORT\",\n\"id_country_iso_code\": null,\n\"address\": \"111 Random Strrt, Florida\"\n},\n\"is_direct_debit\": false,\n\"status\": \"DECLINED\",\n\"fee_amount\": 1.5,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 8.25,\n\"transaction_operation_type\": \"CASH_PICKUP\",\n\"cash_pickup_code\": null,\n\"created_date\": \"2022-04-20T21:34:27\",\n\"wallet_id\": \"9b29c486426a48cf8f675636450e32ce\"\n}"
      }
    ]
  },
  {
    key: "cancel_cash_pickup_transfer",
    name: "Cancel Cash Pickup Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Cancel%20Cash%20Pickup%20Transfer",
    test_cases: [
      {
        case_id: "successfully_cancel_cash_pickup_transfer",
        case_name: "Successfully Cancelled a Transfer",
        instruction: "Find a created cash pickup transfer with status CONFIRMED_WAITING_FOR_PICKUP, and cancel it.",
        is_required: true,
        expected_result: "{\n\"additional_information_1\": null,\n\"additional_information_2\": null,\n\"additional_information_3\": null,\n\"beneficiary\": {\n\"address\": null,\n\"bank_account_holder_name\": null,\n\"city\": null,\n\"code\": null,\n\"country_iso_code\": \"KHM\",\n\"country_of_birth_iso_code\": null,\n\"date_of_birth\": null,\n\"email\": null,\n\"firstname\": \"Cambodia\",\n\"gender\": null,\n\"id_country_iso_code\": null,\n\"id_delivery_date\": null,\n\"id_expiration_date\": null,\n\"id_number\": null,\n\"id_type\": null,\n\"lastname\": \"Individual\",\n\"lastname2\": null,\n\"middlename\": null,\n\"msisdn\": \"164623123544\",\n\"nationality_country_iso_code\": \"KHM\",\n\"nativename\": null,\n\"occupation\": null,\n\"postal_code\": null,\n\"province_state\": null\n},\n\"callback_url\": \"https://canyon-securities-reflect-mad.trycloudflare.com/webhook/transfer/THUNES?webhook_hash=edce8fd2b731465dacde0ff7afb254fc\",\n\"creation_date\": \"2022-04-11T14:45:56Z\",\n\"credit_party_identifier\":{\n\"msisdn\": \"16462346544\"\n},\n\"destination\": {\n\"amount\": 10,\n\"currency\": \"USD\"},\n\"document_reference_number\": null,\n\"expiration_date\": \"2022-04-11T15:32:58Z\",\n\"external_code\": null,\n\"external_id\": \"75a040c3bc0042ad93c833d77624b43e\",\n\"fee\": {\n\"amount\": 2.5,\n\"currency\": \"USD\"},\n\"id\": 2475266,\n\"payer\": {\n\"country_iso_code\": \"KHM\",\n\"currency\": \"USD\",\n\"id\": 2550,\n\"name\": \"Cambodia Universal CashPick-up\",\n\"service\": {\n\"id\": 3,\n\"name\": \"CashPickup\"\n}},\n\"payer_transaction_code\": \"THUNES-KHM-QX-XF4-GRR-EAD\",\n\"payer_transaction_reference\": null,\n\"purpose_of_remittance\": \"OTHER\",\n\"retail_fee\": null,\n\"retail_fee_currency\": null,\n\"retail_rate\": null,\n\"sender\": {\n\"address\": \"111 Random AVE MIAMI 33133Florida\",\n\"bank_account_number\": null,\n\"beneficiary_relationship\": \"DAUGHTER\",\n\"city\": null,\n\"code\": \"null\",\n\"country_iso_code\": \"USA\",\n\"country_of_birth_iso_code\": \"USA\",\n\"date_of_birth\": \"1963-04-20\",\n\"email\": null,\n\"firstname\": \"Michelle\",\n\"gender\": null,\n\"id_country_iso_code\": null,\n\"id_delivery_date\": null,\n\"id_expiration_date\": null,\n\"id_number\": \"123123\",\n\"id_type\": \"PASSPORT\",\n\"lastname\": \"Poignant\",\n\"lastname2\": null,\n\"middlename\": null,\n\"msisdn\": \"16663334444\",\n\"nationality_country_iso_code\": \"USA\",\n\"nativename\": null,\n\"occupation\": null,\n\"postal_code\": null,\n\"province_state\": null,\n\"source_of_funds\": null},\n\"sent_amount\": {\n\"amount\": 10,\n\"currency\": \"USD\"},\n\"source\": {\n\"amount\": 10,\n\"country_iso_code\": \"CAN\",\n\"currency\": \"USD\"},\n\"status\": \"40000\",\n\"status_class\": \"4\",\n\"status_class_message\": \"CANCELLED\",\n\"status_message\": \"CANCELLED\",\n\"transaction_type\": \"C2C\",\n\"wholesale_fx_rate\": 1\n}"
      }
    ]
  },
  {
    key: "create_bank_transfer",
    name: "Create Bank Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Transfer",
    test_cases: [
      {
        case_id: "completed_bank_transfer",
        case_name: "Completed Bank Transfer",
        instruction: "Successfully create a bank transfer with COMPLETED status.",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2503894\",\n\"transfer_id\": \"0f5816f7-4ebb-4c5e-b386-c739686fc265\",\n\"ref_id\": \"\",\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"EUR\",\n\"rate\": 0.94,\n\"memo\": \"payment\",\n\"delivery_date\": null,\n\"remote_id\": \"greece123\",\n\"sender_remote_id\": null,\n\"sender\": null,\n\"is_direct_debit\": false,\n\"status\": \"COMPLETED\",\n\"fee_amount\": 2.0,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 1067.52,\n\"transaction_operation_type\": \"ONLINE_BANKING\",\n\"cash_pickup_code\": null,\n\"created_date\": \"2022-04-27T18:55:43\",\n\"wallet_id\": \"f43e7592d4fd44e1a8abdd120a61de0d\"\n}"
      },
      {
        case_id: "declined_bank_transfer",
        case_name: "Declined Bank Transfer",
        instruction: "Create a bank tranfers with different account numbers and receive different DECLINED statuses",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2503894\",\n\"transfer_id\": \"0f5816f7-4ebb-4c5e-b386-c739686fc265\",\n\"ref_id\": \"\",\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"EUR\",\n\"rate\": 0.94,\n\"memo\": \"payment\",\n\"delivery_date\": null,\n\"remote_id\": \"greece123\",\n\"sender_remote_id\": null,\n\"sender\": null,\n\"is_direct_debit\": false,\n\"status\": \"DECLINED_LIMITATIONS_ON_SENDER_VALUE\",\n\"fee_amount\": 2.0,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 1067.52,\n\"transaction_operation_type\": \"ONLINE_BANKING\",\n\"cash_pickup_code\": null,\n\"created_date\": \"2022-04-27T18:55:43\",\n\"wallet_id\": \"f43e7592d4fd44e1a8abdd120a61de0d\"\n}"
      },
      {
        case_id: "insufficient_balance",
        case_name: "Insufficient Balance",
        instruction: "Clear account balance and create a transfer to receive an insufficient balance error.",
        is_required: true,
        expected_result: "{\n\"detail\": {\n\"errors\": [\n {\n \"code\": \"1007005\",\n \"message\": \"Transaction can not be confirmed, insufficient balance\"\n }\n]\n },\n \"title\": \"Bad Request\",\n \"status\": 400\n }\n"
      },
      {
        case_id: "reversed_transfer",
        case_name: "Reversed Transfer",
        instruction: "Send transfer_id and partner_reference_id of a transfer to ZED team. ZED will reverse the transfer, then update the transfer status. ",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2503894\",\n\"transfer_id\": \"0f5816f7-4ebb-4c5e-b386-c739686fc265\",\n\"ref_id\": \"\",\n\"amount\": 1000.0,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"EUR\",\n\"rate\": 0.94,\n\"memo\": \"payment\",\n\"delivery_date\": null,\n\"remote_id\": \"greece123\",\n\"sender_remote_id\": null,\n\"sender\": null,\n\"is_direct_debit\": false,\n\"status\": \"REVERSED\",\n\"fee_amount\": 2.0,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 1067.52,\n\"transaction_operation_type\": \"ONLINE_BANKING\",\n\"cash_pickup_code\": null,\n\"created_date\": \"2022-04-27T18:55:43\",\n\"wallet_id\": \"f43e7592d4fd44e1a8abdd120a61de0d\"\n}"
      },
    ]
  },
  {
    key: "add_transfer_attachment",
    name: "Add Transfer Attachment", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Add%20transfer%20Attachment",
    test_cases: [
      {
        case_id: "add_transfer_attachment",
        case_name: "Success",
        instruction: "Create a B2B transfer to a destination bank that requires additional file (e.g. invoice, packing slip, etc.), then send additional file to proceed the transfer.",
        is_required: true,
        expected_result: "{\n\"message\": \"Successfully add transaction attachment\"\n}"
      }
    ]
  },
  
]

export const CORPAY = [
  {
    key: "create_bulk_transfer",
    name: "Create Bulk Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Batch%20Transfer",
    test_cases: [
      {
        case_id: "create_batch_transfer",
        case_name: "Create Batch Transfers",
        instruction: "Successfully create batch transfers.",
        is_required: true,
        expected_result: "[\n{\n\"partner_reference_id\": \"430219\",\n\"transfer_id\": \"609\",\n\"ref_id\": \"\",\n\"amount\": 100,\n\"sell_currency\": \"CAD\",\n\"buy_currency\": \"CAD\",\n\"account_no\": \"EUR123432234\",\n\"rate\": 1,\n\"memo\": \"payment\",\n\"delivery_date\": \"2021-09-07T00:00:00\",\n\"remote_id\": \"473c801a11fb45a18774998dccafd979\",\n\"is_direct_debit\": true,\n\"status\": \"COMPLETED\",\n\"fee_amount\": 0,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 0,\n\"type\": \"ONLINE_BANKING\"\n},\n{\n\"partner_reference_id\": \"430220\",\n\"transfer_id\": \"610\",\n\"ref_id\": \"\",\n\"amount\": 200,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"CAD\",\n\"account_no\": \"854312432\",\n\"rate\": 1.2453,\n\"memo\": \"payment\",\n\"delivery_date\": \"2021-09-07T00:00:00\",\n\"remote_id\": \"a0ceab47c5d2422aad58a6275ca8dcf5\",\n\"is_direct_debit\": false,\n\"fee_amount\": 0,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 0,\n\"type\": \"ONLINE_BANKING\"\n}\n]"
      },
    ]
  },
  {
    key: "create_single_transfer",
    name: "Create Single Transfer", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Transfer",
    test_cases: [
      {
        case_id: "completed_single_transfer",
        case_name: "Completed Single Transfer",
        instruction: "Successfully create single bank transfer",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2263896\",\n\"transfer_id\": \"89ea598f-93a0-40d4-bc1e-f6603c560c5e\",\n\"ref_id\": \"\",\n\"amount\": 100,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"CLP\",\n\"account_no\": \"123123\",\n\"rate\": 837.67,\n\"memo\": \"payment\",\n\"delivery_date\": \"2021-12-07T06:43:16Z\",\n\"remote_id\": \"a5251dbb1731468a89693536ee743fdb\",\n\"is_direct_debit\": false,\n\"status\": \"CONFIRMED\",\n\"fee_amount\": 2,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 0.12,\n\"type\": \"ONLINE_BANKING\"\n}"
      },
      {
        case_id: "confirmed_single_transfer",
        case_name: "Confirmed Single Transfer",
        instruction: "Successfully create single bank transfer with status CONFIRMED",
        is_required: true,
        expected_result: "{\n\"partner_reference_id\": \"2263896\",\n\"transfer_id\": \"89ea598f-93a0-40d4-bc1e-f6603c560c5e\",\n\"ref_id\": \"\",\n\"amount\": 100,\n\"sell_currency\": \"USD\",\n\"buy_currency\": \"CLP\",\n\"account_no\": \"123123\",\n\"rate\": 837.67,\n\"memo\": \"payment\",\n\"delivery_date\": \"2021-12-07T06:43:16Z\",\n\"remote_id\": \"a5251dbb1731468a89693536ee743fdb\",\n\"is_direct_debit\": false,\n\"fee_amount\": 2,\n\"fee_currency\": \"USD\",\n\"settlement_amount\": 0.12,\n\"type\": \"ONLINE_BANKING\"\n}"
      },
    ]
  }
]

export const RECIPIENT = [
  {
    key: "get_recipient_list",
    name: "Get Recipient List", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Fetch%20Recipients%20List",
    test_cases: [
      {
        case_id: "success_without_a_param",
        case_name: "Success without a Parameter",
        instruction: "Successfully fetch first page of recipients without any parameters",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "success_with_search_param",
        case_name: "Success with Search Parameter",
        instruction: "Use search parameter to find retrieve one/multiple recipients by recipients' email/remote_id/first_name/last_name.",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "success_with_page_param",
        case_name: "Success with Page Parameter",
        instruction: "Use page parameter to find a specific page of recipient list",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "recipient_not_found",
        case_name: "Recipient Not Found",
        instruction: "Search for a not existing recipient.",
        is_required: false,
        expected_result: "{\n\"recipient\": [],\n\"page\": 1,\n\"total_pages\": 1\n}"
      },
      {
        case_id: "page_out_of_range",
        case_name: "Page Number Out of Range",
        instruction: "Use a invalid number in page parameter",
        is_required: false,
        expected_result: "{\"detail\": \"Page -1 not found\",\n\"title\": \"Not Found\",\n\"status\": 404\n}"
      }
    ]
  },
  {
    key: "create_recipient",
    name: "Create Recipient", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Recipient",
    test_cases: [
      {
        case_id: "create_business_recipient",
        case_name: "Create a Business Recipient",
        instruction: "Successful creation of a business recipient.",
        is_required: true,
        expected_result: "{\n\"address\":{\n\"id\":3011,\n\"country\":\"GR\",\n\"address_line_one\":\"111ImaginationRd\",\n\"address_line_two\":null,\n\"city\":\"Toronto\",\n\"province\":\"Voiotia\",\n\"postal_code\":\"M1A0A0\"},\n\"payout_wallet\":{\n\"e7a93a99e2da4bb1bc4c66cbde1208c8\":{\n\"bank_country_code\":\"UY\",\n\"bank_currency\":\"UYU\",\n\"bank_name\":\"BancoHipotecariodelUruguay\",\n\"bank_swift_bic\":\"BHUMUYM1XXX\",\n\"type\":\"Bank\",\n\"bank_account_type\":\"OTHERS\",\n\"default_payment\":true,\n\"account_number\":\"123123115\",\n\"provider_id\":\"4862\",\n\"wallet_id\":\"e7a93a99e2da4bb1bc4c66cbde1208c8\"\n}\n},\n\"identification_cards\":[],\n\"transfer_purpose\":null,\n\"citizenship\":\"US\",\n\"email\":\"dasdasd@email.com\",\n\"remote_id\":\"4692d06106c04702ac180bdd52a215c6\",\n\"account_type\":\"Business\",\n\"first_name\":null,\n\"middle_name\":null,\n\"tax_id\":null,\n\"last_name\":null,\n\"business_name\":\"Greece\",\n\"business_registration_number\":\"123456789\",\n\"phone_number\":\"+16462346544\",\n\"payment_type\":\"LocalBankTransfer\",\n\"birthday\":null,\n\"date_of_formation\":null,\n\"gender\":null,\n\"recipient_role\":true,\n\"sender_role\":false,\n\"create_time\":\"2022-05-17T16:17:40\",\n\"job_title\":null,\n\"remittance_line_2\":\"reward_payment\",\n\"remittance_line_3\":\"Other\",\n\"remittance_line_4\":\"Other\"\n}"
      },
      {
        case_id: "create_individual_recipient",
        case_name: "Create a Individual Recipient",
        instruction: "Successful creation of a individual recipient.",
        is_required: true,
        expected_result: "{\n\"address\":{\n \"id\": 3012,\n \"country\": \"GR\",\n \"address_line_one\": \"111 Imagination Rd\",\n \"address_line_two\": null,\n \"city\": \"Toronto\",\n \"province\": \"Voiotia\",\n \"postal_code\": \"M1A 0A0\"\n},\n \"payout_wallet\": {},\n \"identification_cards\": [],\n \"transfer_purpose\": null,\n \"citizenship\": \"US\",\n \"email\": \"ge1234@email.com\",\n \"remote_id\": \"2bef86c5e9b34646b0c0a2d2828bac74\",\n \"account_type\": \"Individual\",\n \"first_name\": \"Smith\",\n \"middle_name\": null,\n \"tax_id\": null,\n \"last_name\": \"Blank\",\n \"business_name\": null,\n \"business_registration_number\": null,\n \"phone_number\": \"+16462346544\",\n \"payment_type\": \"Local Bank Transfer\",\n \"birthday\": null,\n \"date_of_formation\": null,\n \"gender\": null,\n \"recipient_role\": true,\n \"sender_role\": false,\n \"create_time\": \"2022-05-17T16:42:38\",\n \"job_title\": null,\n \"remittance_line_2\": \"reward_payment\",\n \"remittance_line_3\": \"Other\",\n \"remittance_line_4\": \"Other\"\n}"
      }
    ]
  },
  {
    key: "update_recipient",
    name: "Update Recipient", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Update%20Recipient",
    test_cases: [
      {
        case_id: "update_business_recipient",
        case_name: "Update a Business Recipient",
        instruction: "Successful update of a business recipient.",
        is_required: false,
        expected_result: "{\n\"address\":{\n\"id\":3011,\n\"country\":\"GR\",\n\"address_line_one\":\"111ImaginationRd\",\n\"address_line_two\":null,\n\"city\":\"Toronto\",\n\"province\":\"Voiotia\",\n\"postal_code\":\"M1A0A0\"},\n\"payout_wallet\":{\n\"e7a93a99e2da4bb1bc4c66cbde1208c8\":{\n\"bank_country_code\":\"UY\",\n\"bank_currency\":\"UYU\",\n\"bank_name\":\"BancoHipotecariodelUruguay\",\n\"bank_swift_bic\":\"BHUMUYM1XXX\",\n\"type\":\"Bank\",\n\"bank_account_type\":\"OTHERS\",\n\"default_payment\":true,\n\"account_number\":\"123123115\",\n\"provider_id\":\"4862\",\n\"wallet_id\":\"e7a93a99e2da4bb1bc4c66cbde1208c8\"\n}\n},\n\"identification_cards\":[],\n\"transfer_purpose\":null,\n\"citizenship\":\"US\",\n\"email\":\"dasdasd@email.com\",\n\"remote_id\":\"4692d06106c04702ac180bdd52a215c6\",\n\"account_type\":\"Business\",\n\"first_name\":null,\n\"middle_name\":null,\n\"tax_id\":null,\n\"last_name\":null,\n\"business_name\":\"Greece\",\n\"business_registration_number\":\"123456789\",\n\"phone_number\":\"+16462346544\",\n\"payment_type\":\"LocalBankTransfer\",\n\"birthday\":null,\n\"date_of_formation\":null,\n\"gender\":null,\n\"recipient_role\":true,\n\"sender_role\":false,\n\"create_time\":\"2022-05-17T16:17:40\",\n\"job_title\":null,\n\"remittance_line_2\":\"reward_payment\",\n\"remittance_line_3\":\"Other\",\n\"remittance_line_4\":\"Other\"\n}"
      },
      {
        case_id: "update_individual_recipient",
        case_name: "Update a Individual Recipient",
        instruction: "Successful update of a individual recipient.",
        is_required: false,
        expected_result: "{\n\"address\":{\n \"id\": 3012,\n \"country\": \"GR\",\n \"address_line_one\": \"111 Imagination Rd\",\n \"address_line_two\": null,\n \"city\": \"Toronto\",\n \"province\": \"Voiotia\",\n \"postal_code\": \"M1A 0A0\"\n},\n \"payout_wallet\": {},\n \"identification_cards\": [],\n \"transfer_purpose\": null,\n \"citizenship\": \"US\",\n \"email\": \"ge1234@email.com\",\n \"remote_id\": \"2bef86c5e9b34646b0c0a2d2828bac74\",\n \"account_type\": \"Individual\",\n \"first_name\": \"Smith\",\n \"middle_name\": null,\n \"tax_id\": null,\n \"last_name\": \"Blank\",\n \"business_name\": null,\n \"business_registration_number\": null,\n \"phone_number\": \"+16462346544\",\n \"payment_type\": \"Local Bank Transfer\",\n \"birthday\": null,\n \"date_of_formation\": null,\n \"gender\": null,\n \"recipient_role\": true,\n \"sender_role\": false,\n \"create_time\": \"2022-05-17T16:42:38\",\n \"job_title\": null,\n \"remittance_line_2\": \"reward_payment\",\n \"remittance_line_3\": \"Other\",\n \"remittance_line_4\": \"Other\"\n}"
      }
    ]
  },
  {
    key: "add_or_update_recipient_id",
    name: "Add or Update Recipient ID", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Add%20Recipient%20ID",
    test_cases: [
      {
        case_id: "successful_add_of_recipient_id",
        case_name: "Successful Add of Recipient ID",
        instruction: "Add ZED approved type of recipient ID",
        is_required: true,
        expected_result: "[\n {\n \"id_number\": \"12341\",\n \"id_type\": \"NATIONAL_ID\",\n \"expiration\": \"2022-11-01\"\n }\n]"
      },  
    ]
  },
]

export const SENDER = [
  {
    key: "get_sender_list",
    name: "Get Sender List", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Fetch%20Sender%20List",
    test_cases: [
      {
        case_id: "success_without_a_param",
        case_name: "Success without a Parameter",
        instruction: "Successfully fetch first page of sender without any parameters",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "success_with_search_param",
        case_name: "Success with Search Parameter",
        instruction: "Use search parameter to find retrieve one/multiple recipients by recipients' email/remote_id/first_name/last_name.",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "success_with_page_param",
        case_name: "Success with Page Parameter",
        instruction: "Use page parameter to find a specific page of recipient list",
        is_required: false,
        expected_result: "{\n\"recipient\": [\n{\n\"address\": {},\n\"payout_wallet\": {},\n\"email\": \"zed_network@gmail.com\",\n\"remote_id\": \"a7c066f913a941498c26fe22d659c60e\",\n\"account_type\": \"Individual\",\n\"first_name\": \"Dennis\",\n\"last_name\": \"Brown\",\n\"phone_number\": \"+639912348\",\n\"birthday\": \"1995-01-31\",\n\"payment_type\": \"Local Bank Transfer\",\n\"remittance_line_2\": \"Other\",\n\"remittance_line_3\": \"Other\",\n\"remittance_line_4\": \"Other\",\n\"status\": \"Approved\"\n}\n]\n}"
      },
      {
        case_id: "sender_not_found",
        case_name: "Sender Not Found",
        instruction: "Search for a not existing sender.",
        is_required: false,
        expected_result: "{\n\"recipient\": [],\n\"page\": 1,\n\"total_pages\": 1\n}"
      },
      {
        case_id: "page_out_of_range",
        case_name: "Page Number Out of Range",
        instruction: "Use a invalid number in page parameter",
        is_required: false,
        expected_result: "{\"detail\": \"Page -1 not found\",\n\"title\": \"Not Found\",\n\"status\": 404\n}"
      }
    ]
  },
  {
    key: "create_sender",
    name: "Create Sender", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Sender",
    test_cases: [
      {
        case_id: "create_business_sender",
        case_name: "Create a Business sender",
        instruction: "Successful creation of a business sender.",
        is_required: true,
        expected_result: "{\n\"address\":{\n\"id\":3011,\n\"country\":\"GR\",\n\"address_line_one\":\"111ImaginationRd\",\n\"address_line_two\":null,\n\"city\":\"Toronto\",\n\"province\":\"Voiotia\",\n\"postal_code\":\"M1A0A0\"},\n\"payout_wallet\":{\n\"e7a93a99e2da4bb1bc4c66cbde1208c8\":{\n\"bank_country_code\":\"UY\",\n\"bank_currency\":\"UYU\",\n\"bank_name\":\"BancoHipotecariodelUruguay\",\n\"bank_swift_bic\":\"BHUMUYM1XXX\",\n\"type\":\"Bank\",\n\"bank_account_type\":\"OTHERS\",\n\"default_payment\":true,\n\"account_number\":\"123123115\",\n\"provider_id\":\"4862\",\n\"wallet_id\":\"e7a93a99e2da4bb1bc4c66cbde1208c8\"\n}\n},\n\"identification_cards\":[],\n\"transfer_purpose\":null,\n\"citizenship\":\"US\",\n\"email\":\"dasdasd@email.com\",\n\"remote_id\":\"4692d06106c04702ac180bdd52a215c6\",\n\"account_type\":\"Business\",\n\"first_name\":null,\n\"middle_name\":null,\n\"tax_id\":null,\n\"last_name\":null,\n\"business_name\":\"Greece\",\n\"business_registration_number\":\"123456789\",\n\"phone_number\":\"+16462346544\",\n\"payment_type\":\"LocalBankTransfer\",\n\"birthday\":null,\n\"date_of_formation\":null,\n\"gender\":null,\n\"recipient_role\":true,\n\"sender_role\":true,\n\"create_time\":\"2022-05-17T16:17:40\",\n\"job_title\":null,\n\"remittance_line_2\":\"reward_payment\",\n\"remittance_line_3\":\"Other\",\n\"remittance_line_4\":\"Other\"\n}"
      },
      {
        case_id: "create_individual_recipient",
        case_name: "Create a Individual sender",
        instruction: "Successful creation of a individual sender.",
        is_required: true,
        expected_result: "{\n\"address\":{\n \"id\": 3012,\n \"country\": \"GR\",\n \"address_line_one\": \"111 Imagination Rd\",\n \"address_line_two\": null,\n \"city\": \"Toronto\",\n \"province\": \"Voiotia\",\n \"postal_code\": \"M1A 0A0\"\n},\n \"payout_wallet\": {},\n \"identification_cards\": [],\n \"transfer_purpose\": null,\n \"citizenship\": \"US\",\n \"email\": \"ge1234@email.com\",\n \"remote_id\": \"2bef86c5e9b34646b0c0a2d2828bac74\",\n \"account_type\": \"Individual\",\n \"first_name\": \"Smith\",\n \"middle_name\": null,\n \"tax_id\": null,\n \"last_name\": \"Blank\",\n \"business_name\": null,\n \"business_registration_number\": null,\n \"phone_number\": \"+16462346544\",\n \"payment_type\": \"Local Bank Transfer\",\n \"birthday\": null,\n \"date_of_formation\": null,\n \"gender\": null,\n \"recipient_role\": true,\n \"sender_role\": false,\n \"create_time\": \"2022-05-17T16:42:38\",\n \"job_title\": null,\n \"remittance_line_2\": \"reward_payment\",\n \"remittance_line_3\": \"Other\",\n \"remittance_line_4\": \"Other\"\n}"
      }
    ]
  },
  {
    key: "update_recipient",
    name: "Update sender", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Update%20Recipient",
    test_cases: [
      {
        case_id: "update_business_recipient",
        case_name: "Update a Business sender",
        instruction: "Successful update of a business sender.",
        is_required: false,
        expected_result: "{\n\"address\":{\n\"id\":3011,\n\"country\":\"GR\",\n\"address_line_one\":\"111ImaginationRd\",\n\"address_line_two\":null,\n\"city\":\"Toronto\",\n\"province\":\"Voiotia\",\n\"postal_code\":\"M1A0A0\"},\n\"payout_wallet\":{\n\"e7a93a99e2da4bb1bc4c66cbde1208c8\":{\n\"bank_country_code\":\"UY\",\n\"bank_currency\":\"UYU\",\n\"bank_name\":\"BancoHipotecariodelUruguay\",\n\"bank_swift_bic\":\"BHUMUYM1XXX\",\n\"type\":\"Bank\",\n\"bank_account_type\":\"OTHERS\",\n\"default_payment\":true,\n\"account_number\":\"123123115\",\n\"provider_id\":\"4862\",\n\"wallet_id\":\"e7a93a99e2da4bb1bc4c66cbde1208c8\"\n}\n},\n\"identification_cards\":[],\n\"transfer_purpose\":null,\n\"citizenship\":\"US\",\n\"email\":\"dasdasd@email.com\",\n\"remote_id\":\"4692d06106c04702ac180bdd52a215c6\",\n\"account_type\":\"Business\",\n\"first_name\":null,\n\"middle_name\":null,\n\"tax_id\":null,\n\"last_name\":null,\n\"business_name\":\"Greece\",\n\"business_registration_number\":\"123456789\",\n\"phone_number\":\"+16462346544\",\n\"payment_type\":\"LocalBankTransfer\",\n\"birthday\":null,\n\"date_of_formation\":null,\n\"gender\":null,\n\"recipient_role\":true,\n\"sender_role\":true,\n\"create_time\":\"2022-05-17T16:17:40\",\n\"job_title\":null,\n\"remittance_line_2\":\"reward_payment\",\n\"remittance_line_3\":\"Other\",\n\"remittance_line_4\":\"Other\"\n}"
      },
      {
        case_id: "update_individual_recipient",
        case_name: "Update a Individual sender",
        instruction: "Successful update of a individual sender.",
        is_required: false,
        expected_result: "{\n\"address\":{\n \"id\": 3012,\n \"country\": \"GR\",\n \"address_line_one\": \"111 Imagination Rd\",\n \"address_line_two\": null,\n \"city\": \"Toronto\",\n \"province\": \"Voiotia\",\n \"postal_code\": \"M1A 0A0\"\n},\n \"payout_wallet\": {},\n \"identification_cards\": [],\n \"transfer_purpose\": null,\n \"citizenship\": \"US\",\n \"email\": \"ge1234@email.com\",\n \"remote_id\": \"2bef86c5e9b34646b0c0a2d2828bac74\",\n \"account_type\": \"Individual\",\n \"first_name\": \"Smith\",\n \"middle_name\": null,\n \"tax_id\": null,\n \"last_name\": \"Blank\",\n \"business_name\": null,\n \"business_registration_number\": null,\n \"phone_number\": \"+16462346544\",\n \"payment_type\": \"Local Bank Transfer\",\n \"birthday\": null,\n \"date_of_formation\": null,\n \"gender\": null,\n \"recipient_role\": true,\n \"sender_role\": true,\n \"create_time\": \"2022-05-17T16:42:38\",\n \"job_title\": null,\n \"remittance_line_2\": \"reward_payment\",\n \"remittance_line_3\": \"Other\",\n \"remittance_line_4\": \"Other\"\n}"
      }
    ]
  },
  {
    key: "add_or_update_sender_id",
    name: "Add or Update sender ID", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Add%20Recipient%20ID",
    test_cases: [
      {
        case_id: "sucessful_add_of_recipient_id",
        case_name: "Successful Add of sender ID",
        instruction: "Add ZED approved type of sender ID",
        is_required: true,
        expected_result: "[\n {\n \"id_number\": \"12341\",\n \"id_type\": \"NATIONAL_ID\",\n \"expiration\": \"2022-11-01\"\n }\n]"
      },  
    ]
  },
]

export const WALLET = [
  {
    key: "get_wallet",
    name: "Get Wallet", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Fetch%20Wallet%20Details",
    test_cases: [
      {
        case_id: "success",
        case_name: "Success",
        instruction: "Successfully retrieve wallet details.",
        is_required: false,
        expected_result: "{\n\"e2a19602e3354a33b1309a27208149b1\": {\n\"bank_country_code\": \"EC\",\n\"bank_currency\": \"USD\",\n\"bank_name\": \"BANCO PICHINCHA C.A.\",\n\"bank_swift_bic\": \"PICHECEQ350\",\n\"type\": \"Bank\",\n\"bank_account_type\": \"checking\",\n\"default_payment\": false,\n\"account_number\": \"123456789100\",\n\"mobile_number\": \"+19998887777\",\n\"provider_id\": \"3557\",\n\"wallet_id\": \"e2a19602e3354a33b1309a27208149b1\"\n},\n\"9b3457aef8b74cbe9bd578a83bc881d9\": {\n\"bank_country_code\": \"EC\",\n\"bank_currency\": \"USD\",\n\"bank_name\": \"BANCO PICHINCHA C.A.\",\n\"bank_swift_bic\": \"PICHECEQ350\",\n\"type\": \"Bank\",\n\"bank_account_type\": \"checking\",\n\"default_payment\": false,\n\"account_number\": \"123456789100\",\n\"mobile_number\": \"+19998887777\",\n\"provider_id\": \"3557\",\n\"wallet_id\": \"9b3457aef8b74cbe9bd578a83bc881d9\",\n\"citizenship\": \"USA\"\n}\n}"
      }
    ]
  },
  {
    key: "create_wallet",
    name: "Create Wallet", 
    api_doc: "https://zed-api-dev.zed.network/redoc/#operation/Create%20Wallet",
    test_cases: [
      {
        case_id: "create_cash_pickup_wallet",
        case_name: "Create Cash Pickup Wallet",
        instruction: "Successfully create a cash pickup wallet",
        is_required: true,
        expected_result: "{\n\"mobile_number\": \"+19511596323\",\n\"type\": \"Cash Pickup\",\n\"bank_name\": \"Royal Trust Corporation of Canada\",\n\"default_payment\": true,\n\"bank_currency\": \"CAD\",\n\"bank_country_code\": \"CA\",\n\"wallet_id\": \"804f1ad4d932467baf9f0f9a523fecd1\"\n}\n"
      },
      {
        case_id: "create_mobile_wallet",
        case_name: "Create Wallet Wallet",
        instruction: "Successfully create a mobile wallet",
        is_required: true,
        expected_result: "{\n\"mobile_number\": \"+19511596323\",\n\"type\": \"Mobile Wallet\",\n\"bank_name\": \"Royal Trust Corporation of Canada\",\n\"default_payment\": true,\n\"bank_currency\": \"CAD\",\n\"bank_country_code\": \"CA\",\n\"wallet_id\": \"804f1ad4d932467baf9f0f9a523fecd1\"\n}\n"
      },
      {
        case_id: "create_bank_wallet",
        case_name: "Create Bank Wallet",
        instruction: "Successfully create a bank wallet",
        is_required: true,
        expected_result: "{\n\"f38c7dbe32814d5bbc4def8e4dd9254d\": {\n \"bank_country_code\": \"KE\",\n \"bank_currency\": \"KES\",\n \"bank_name\": \"PARAMOUNT UNIVERSAL BANK LIMITED\",\n \"bank_swift_bic\": \"PAUTKENAXXX\",\n \"type\": \"Bank\",\n \"default_payment\": false,\n \"account_number\": \"123123106\",\n \"provider_id\": \"1512\",\n \"wallet_id\": \"f38c7dbe32814d5bbc4def8e4dd9254d\"\n }\n}"
      }
    ]
  }
]