
import { createStore } from "redux"
import reducer from "./index.reducer"

const isProduction = process.env.GATSBY_ENV === "PRODUCTION"
const isWindowAvailable = typeof window !== "undefined"

export default preloadedState => {
  const store =
    !isProduction && isWindowAvailable
      ? createStore(
          reducer,
          window.__REDUX_DEVTOOLS_EXTENSION__ &&
            window.__REDUX_DEVTOOLS_EXTENSION__()
        )
      : createStore(reducer)

  return store
}
