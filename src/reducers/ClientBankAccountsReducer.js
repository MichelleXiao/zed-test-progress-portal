export default function clientBankAccountsReducer(state = {}, action) {
  switch (action.type) {
    case 'ACCOUNTS':
      const accountsBalance = action.payload
      return { ...state, accountsBalance }
    case 'INTEGRATIONS':
      const integrations = action.payload
      return { ...state, integrations }
    case 'RECIPIENTS':
      const recipients = action.payload
      return { ...state, recipients }
    case 'SENDERS':
      const senders = action.payload
      return { ...state, senders }
    case 'TRANSFERS':
        const transfers = action.payload
        return { ...state, transfers }
    default:
      return state
  }
}
