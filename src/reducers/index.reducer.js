import {combineReducers} from "redux"
import userReducer from "./UserReducer"
import clientBankAccountsReducer from "./ClientBankAccountsReducer"
import sidebarReducer from "./SidebarReducer"
const initialState = {
  isInitialize: true,
  matches: true,
  open: true,
  /**
   * The application toast message state
   */
  toast: [],
  notifications: []
}

// Application level reducer
const initialReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'INITIALIZE':
      const { isInitialize } = action.payload
      return { ...state, isInitialize }
    case 'MEDIA_QUERY':
      const { matches } = action.payload
      return { ...state, matches }
    case 'SIDE_BAR_OPEN':
      const { open } = action.payload
      return { ...state, open }
    case 'TOAST':
      return {
        ...state,
        toast: [
          ...state.toast,
          {
            key: new Date().getTime() + Math.random(),
            ...action.payload,
          }
        ]
      }
    case 'DELETE_TOAST':
      return {
        ...state,
        toast: state.toast.filter(toastItem => toastItem.key !== action.payload.key)
      }
    case 'ADD_NOTIFICATION':
      return {
        ...state,
        notifications: [...state.notifications, ...action.payload]
      }
    case 'RESET_NOTIFICATION':
      return {
        ...state,
        notifications: action.payload || []
      }
      
    default:
      return state
  }
}

export default combineReducers({user: userReducer, app: initialReducer, clientBankAccounts: clientBankAccountsReducer, sidebar: sidebarReducer})