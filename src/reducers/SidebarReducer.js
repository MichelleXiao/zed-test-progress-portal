
export default function sidebarReducer(state = {}, action) {
  switch (action.type) {
    case 'SIDEBAR':
      const showApis = action.payload
      return { ...state, showApis }
    case 'SHOW_SIDEBAR':
      const showSideBar = action.payload
      return { ...state, showSideBar }
    default:
      return state
  }
}
