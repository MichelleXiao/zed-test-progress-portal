export default function userReducer(state = {}, action) {
  switch (action.type) {
    case 'USER':
      const userInfo = action.payload
      return { ...state, userInfo }
    case 'PREFERENCES':
      const userPreferences = action.payload
      return { ...state, userPreferences }
    default:
      return state
  }
}
