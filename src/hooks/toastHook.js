import { useDispatch } from 'react-redux'

export default () => {
  const dispatch = useDispatch()

  return (message, type = 'error') => {
    if (Array.isArray(message)) {
      message.forEach(item =>
        dispatch({
          type: "TOAST",
          payload: {
            message: item.message,
            type: type,
          },
        })
      )
      return
    }

    // For Single Item

    dispatch({
      type: "TOAST",
      payload: {
        message: message,
        type: type,
      },
    })
  }
}
