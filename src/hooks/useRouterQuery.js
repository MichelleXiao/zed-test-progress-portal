import { useMemo, useCallback } from 'react';
import { useLocation, useHistory } from '@reach/router'; 
import { navigate } from 'gatsby';

export const useRouterQuery = () => {
  const location = useLocation();

  const queries = useMemo(() => {
    const urlParams = new URLSearchParams(location.search);

    let params = {};

    const entries = urlParams.entries();
    let item = entries.next();

    while (!item.done) {
      params[item.value[0]] = item.value[1];
      item = entries.next();
    }

    return params;
  }, [location])

  const updateQueries = useCallback((newQueries) => {
    const params = new URLSearchParams(newQueries);

    navigate(`${location.pathname}?${params.toString()}`)
  }, [location])

  return [queries, updateQueries];
}
