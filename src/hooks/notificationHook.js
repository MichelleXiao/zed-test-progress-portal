import { useDispatch } from 'react-redux'

export default () => {
  const dispatch = useDispatch()

  return {
    add: (notification) => {
      dispatch({
        type: 'ADD_NOTIFICATION',
        payload: Array.isArray(notification) ? notification : [notification],
      })
    },
    reset: (notification = []) => {
      dispatch({
        type: 'RESET_NOTIFICATION',
        payload: notification
      })
    }
  }
}
