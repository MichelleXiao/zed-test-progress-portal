function langPrefix(page) {
  return page.context.language === page.context.i18n.defaultLanguage &&
    !page.context.i18n.generateDefaultLanguagePage
    ? ''
    : `/${page.context.language}`
}

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

 exports.onCreatePage = async ({ page, actions }) => {
    const { createPage } = actions
    if (page.path.match(/\/client/)) {
      page.matchPath = `${langPrefix(page)}/client/*`
      createPage(page)
    }  
    if (page.path.match(/^\/signup/)) {
      page.matchPath = `/signup`
  
      createPage(page)
    }
  }

exports.onCreateWebpackConfig = ({ stage, loaders, actions, plugins }) => {
    let additionalConfig = {}
    if (stage === "build-html" || stage === "develop-html") {
      additionalConfig = {
        ...additionalConfig,
        module: {
          rules: [
            {
              test: /react-pdf/,
              use: loaders.null(),
            },
          ],
        }
      }
    }
    if (stage === 'build-javascript' || stage === 'develop') {
      additionalConfig = {
        ...additionalConfig,
        plugins: [
          plugins.provide({ process: 'process/browser' })
        ],
      }
    }
    actions.setWebpackConfig({
      resolve: {
        fallback: {
          stream: require.resolve('stream-browserify'),
          crypto: require.resolve('crypto-browserify'),
          util: require.resolve('util/'),
        },
      },
      ...additionalConfig
    })
  }